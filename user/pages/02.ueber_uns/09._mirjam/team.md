---
title: Mirjam
published: true
image_align: left
---


### Mirjam
**Aktiv in:** Eislingen (BW)   
**Foodsaver\*in seit:** Mai 2021  
**Im Team bin ich** da wo ich gebraucht werde  
**Über eine Rettung dieses Lebensmittels freue ich mich am meisten:** über Dinge, die ich nicht gekauft hätte und durch das Retten kennenlerne   
**Die foodsharing Stadt bedeutet für mich:** die Chance Menschen von jung bis alt zu bilden, informieren, wachzurütteln, zu begeistern und die Möglichkeit im Kleinen, Großes zu bewirken.  