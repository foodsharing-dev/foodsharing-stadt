---
title: Rebecca
published: false
image_align: left
---


### Rebecca
**Aktiv in:** Mainz  
**Foodsaver\*in seit:** 2019  
**In der Bewegung arbeite ich am liebsten an:** Events zur Vernetzung und zum Austausch zwischen den beteiligten Städten  
**Über eine Rettung dieses Lebensmittels freue ich mich am meisten:** Austernpilze und Oliven <3  
**Die foodsharing Stadt bedeutet für mich:** Eine foodsharing Stadt ist für mich eine Stadt, die nicht-kommerzielle Begegnungsräume zur Verfügung stellt und ein regionales Ernährungssystem aktiv ausbaut und die Akteur\*innen hierbei unterstützt. Das geht über die Vergabe von öffentlichen Pachtverträgen für Land an Landwirt\*innen, die ökologische Landwirtschaft betreiben und Nahrungsmittel für die Region anbauen bis zur Bereitstellung von Flächen für regionale Markthallen.    