---
title: "Runder Tisch"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Es findet regelmäßig ein Austausch von verschiedenen Akteur*innen zum Thema Lebensmittelverschwendung statt oder/ und es gibt einen Ernährungsrat in der Stadt. 

An einem runden Tisch kommen verschiedenste Akteur\*innen zusammen, lernen sich und ihre Projekte besser kennen und diskutieren gemeinsam über Lösungen und bessere Zusammenarbeit. Wenn es bei euch noch keinen Ernährungsrat oder runden Tisch gibt, schaut euch mal in einer Nachbarstadt um. Vielleicht könnt ihr euch dort Anregungen holen oder das Projekt gemeinsam angehen.

Eventuell gibt es in eurer Stadt bereits einen runden Tisch zu einem anderen Thema. Stattet den Veranstalter\*innen einen Besuch ab und fragt sie nach ihren Erfahrungen. Das und eure gute Vernetzung mit vielen lokalen Akteur\*innen (Abfallwirtschaft, Tafeln, Betrieben, Bildungseinrichtungen, etc.) gibt euch eine gute Basis, um selbst einen runden Tisch ins Leben zu rufen. Weitere Infos zum Thema Ernährungsrat findet ihr hier: [ernaehrungsraete.de](http://ernaehrungsraete.de/)

![](/user/images/icons/runder_tisch.jpg)



