---
title: "Jugendarbeit"
description: "Menschen unter 18 Jahren für Lebensmittelwertschätzung motivieren"
published: true
---

### Es gibt eine Gruppe junger Menschen, die sich für mehr Lebensmittelwertschätzung in eurer Stadt einsetzen.

**Warum ist das wichtig?**
Alle, die jünger als 18 sind, können leider nicht über foodsharing Lebensmittel retten. Es gibt aber noch viele andere Wege für mehr Lebensmittelwertschätzung einzutreten. Zum Beispiel an Infoständen, bei Bildungsarbeit oder als Mitglied der foodsharing-Städe Ortsgruppe. Außerdem finden wir es wichtig, dass junge Menschen eine eigene Gruppe haben, in der ihr Utopien leben oder aufbauen könnt. Wir wollen das Engagement junger Menschen explizit fördern, weil eure Ideen und Stimmen wertvoll sind.  Wir denken, dass es wichtig ist, dass ihr als junge Menschen erfahrt, dass ihr als Teil der Gesellschaft etwas bewirken könnt. Gleichzeitig wollen wir erreichen, dass Erwachsene vermehrt die Stimmen junger Menschen hören und einbeziehen. 

**Wie könnte es aussehen?** 
Engagement ist an vielen Orten möglich, z.B. als Arbeitsgemeinschaft in eurer Schule. Das tägliche Mittagessen ist ein guter Start. Ihr könnt euch z.B. mit diesen Fragen auseinandersetzen: Was essen eure Mitschüler\*innen und wo? Wer geht in die Schulmensa und wo kommt das Essen dort eigentlich her? Was passiert mit dem Essen in der Mensa, das am Ende des Tages übrig bleibt?
Natürlich müsst ihr nicht unbedingt eine neue Gruppe gründen. Vielleicht gibt es in eurer Stadt schon Jugendgruppen, dort könnt ihr das Thema Lebensmittelwertschätzung integrieren. Das können umweltorientierte Gruppen wie die BUND Jugend oder Fridays for Future sein - Das Thema Essen lässt sich gut in jede Art von Gruppe bringen. Wie wäre es zum Anfang mit einer Schnibbelparty mit geretteten Lebensmittlen?  

**Was ist bei dieser Idee die Rolle von Erwachsenen?**
Die Gruppe kann von Erwachsenen unterstützt und begleitet sein. Gleichzeitig finden wir es wichtig, dass ihr als Jugendliche bestimmen könnt, welche Themen ihr angehen wollte und wie. Ihr solltet aktive Gestalter\*innen sein. 



![](/user/images/icons/jugendarbeit.jpg)