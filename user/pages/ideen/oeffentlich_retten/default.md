---
title: "Lebensmittel retten öffentlich"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Bei einer Veranstaltung in der Stadt (z. B. Stadtfest, Weihnachtsmarkt etc.) werden Lebensmittel gerettet.. 

Findet heraus, wer die Veranstaltung organisiert und erklärt ihnen, warum Lebensmittelretten wichtig ist. Stoßt ihr bei der Verwaltung eurer Stadt auf Widerstand, versucht zunächst, bei kleineren Veranstaltungen anzufangen. Konferenzen und Tagungen sind für die Öffentlichkeit oft nicht so sichtbar wie ein Stadtfest, aber auch dort kann viel Essen gerettet werden. Vielleicht entsteht so Vertrauen und eine langfristige Kooperation, die euch den Weg für das nächste Stadtfest ebnet.

Habt ihr das OK der Veranstalter\*innen, müssen die verschiedenen Lebensmittelstände des Fests häufig trotzdem einzeln angefragt werden. Versucht, mit Menschen zu reden, die Entscheidungen treffen dürfen und verweist auf andere Stände, die mitmachen und auf die Unterstützung durch die Veranstalter\*innen. Argumente für eine Kooperation findet ihr [hier](https://wiki.foodsharing.de/Kooperationsaufbau_-_Checkliste). 

![](/user/images/icons/oeffentliche_rettung.jpg)