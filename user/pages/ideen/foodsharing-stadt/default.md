---
title: "Kriterien für foodsharing-Städte"
description: "Das ist zu erfüllem"
published: true
---

### Kriterien für foodsharing-Städte


In der Stadt gibt es Menschen, die vor Ort aktiv an der Umsetzung des
Ideenkatalogs arbeiten. Das Organisationsteam der foodsharing-Städte
Bewegung ist über eure Teilnahme informiert und ihr habt eine
Ansprechperson, an die ihr euch bei Fragen wenden könnt.


Die öffentliche Hand und die lokale foodsharing Gruppe (und ggf. weitere
Partner*innen) unterzeichnen gemeinsam die Motivationserklärung.

![](/user/images/tree/logo.png)