---
title: "Weitere lokale Initiativen"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Unterstützung/Initiierung eines weiteren Projekts vor Ort, das sich aktiv gegen Lebensmittelverschwendung einsetzt (z. B. Secondhand Supermarkt, ein foodsharing Café o. Ä.).

In eurer Stadt gibt es noch kein derartiges dauerhaftes Projekt? Euch fällt bestimmt etwas Gutes ein! Hier kommen wieder eure Partner\*innen ins Spiel: Jede Organisation sollte überlegen, was sie beitragen kann. Erinnert sie ggf. an eure gemeinsame Motivationserklärung, in der ihr euch zusammen ein Ziel gesetzt habt. Genehmigungen oder Räumlichkeiten sind oft leichter zu bekommen, wenn Verantwortliche hinter der Idee stehen. 

Als Ansprechperson der öffentlichen Hand fällt es dir vielleicht leicht, den\*die Kantinenchef\*in von einem no-waste Projekt zu überzeugen oder für foodsharing Cafés in Schulen zu werben. Als Engagierte\*r bei der Tafel, einer Umweltgruppe oder in einem sozialen Projekt hast du vielleicht schon Erfahrungen im Projektmanagement oder kennst Menschen, die sich um das Alltagsgeschäft in eurem Projekt kümmern möchten. Wir sind gespannt, was ihr mit vereinten Kräften auf die Beine stellt! Hier sind zwei Beispiele aus anderen Städten: [The Good Food](https://www.the-good-food.de/) und [Raupe Immersatt](https://www.raupeimmersatt.de/).
Vielleicht findet ihr dort ja Ideen für euer eigenes Projekt.

**Inspiration und Partnerprojekt: foodsharing Cafés**

<a href="http://foodsharingcafe.net/">
  <img style="float: left;" width=200px src="/user/pages/ideen/weitere_projekte/FSCN_Logo-Stempel_RGB.png">
</a>
foodsharing Cafés teilen noch genießbare, aber überschüssige Lebensmitteln kostenfrei. Dadurch rücken sie das Bewusstsein für den ideellen Wert von Nahrung lokal näher in den Mittelpunkt der Öffentlichkeit. Ein gemeinsames Lernen und Inspirieren in offener Atmosphäre wird ermöglicht.
Mehrere foodsharing Cafés haben sich zum foodsharing Café Netzwerk zusammengeschlossen. Eine Plattform die sowohl Betreiber\*innen von bestehenden foodsharing Cafés als auch Gründungsinteressierte miteinander verbindet. [Hier](http://foodsharingcafe.net/) könnt ihr vorbeischauen wenn ihr wissen möchtet wo bereits Cafés bestehen, wo sich gerade etwas entwickelt und wo vielleicht noch großes Potential für weitere motivierte Inititator:innen schlummert.


![](/user/images/icons/mehr_projekte.jpg)