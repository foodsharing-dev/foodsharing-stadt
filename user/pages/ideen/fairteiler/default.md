---
title: "Fairteiler"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Es gibt Fairteiler vor Ort bzw. ein anderes zugängliches Fairteilsystem

Fair-Teilsysteme sind Orte, an denen Lebensmittel umverteilt werden können. Die Reste der Party, der Fehlkauf oder gerettetes Essen, das du nicht verbrauchen kannst – all das kann zu der Abgabestelle gebracht werden und findet dort hoffentlich eine\*n neue Esser\*in. Auf [dieser Karte](https://foodsharing.de/karte) kannst du sehen, wo es in Europa bereits eingetragene foodsharing Fairteiler gibt. 

Du möchtest einen neuen Fairteiler o. Ä. in deiner Stadt einrichten? Richte z. B. ein Regal oder einen Kühlschrank an einem gut zugänglichen Ort dafür ein. Viele Fairteiler finden sich in Mitmachgärten, sozialen Projekten oder Cafes. Deiner Fantasie sind natürlich keine Grenzen gesetzt. Du kennst deine Stadt am besten und weißt, wo sich ein Fairteilsystem aufbauen lässt und es von vielen genutzt wird. Wichtig ist, dass jede\*r Essen zum Fairteiler bringen und kostenlos daraus entnehmen darf. Überlege dir, wie du Lebensmittel vor Tieren schützt und wie du den Fairteiler sauber hältst. Weitere Tipps findest du [hier](https://wiki.foodsharing.de/Fairteiler_und_Abgabestellen).

![](/user/images/icons/fairteiler.jpg)