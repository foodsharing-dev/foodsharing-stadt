---
title: "Anstellung"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Es gibt eine von der öffentlichen Hand angestellte Person, die den Prozess der Steigerung der Lebensmittelwertschätzung aktiv begleitet.

Um diesen Punkt zu erfüllen, muss keine eigene Stelle geschaffen werden. Lebensmittelwertschätzung soll in die Strategie eurer Stadt eingebunden und bei jedem Schritt mitgedacht werden. Das kann z. B. gut in ein Nachhaltigkeitsbüro oder die Stelle des*der Nachhaltigkeitsbeauftragten integriert werden. Wichtig ist, dass die Lebensmittelwertschätzung im Aufgabenfeld dieser Person definiert ist. So zeigt die Verwaltung den Stellenwert, den Lebensmittelwertschätzung in eurer Stadt einnimmt, vergrößert den Handlungsspielraum und ebnet den Weg für nachhaltige Veränderungen. 

![](/user/images/icons/mitarbeit_stadt.jpg)