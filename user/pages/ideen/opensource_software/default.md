---
title: "Nutzung von Open-Source Software"
description: "Relevanz von freier Software für foodsharing"
published: true
---

### Für die digitale Infrastruktur der lokalen Community werden frei zugängliche Softwareprogramme verwendet z.B. zur Dateiablage (Cloud Dienst) oder für Video-Konferenzen.

**Warum ist das wichtig?**
foodsharing.de ist seit 2019 Open-Source und somit eine freie Software. Das heißt, alle Menschen haben die Möglichkeit, die Software zu verbreiten, auszuführen, zu kopieren, zu verbessern oder daran mitzuentwickeln. Wir verfolgen damit das Ziel, Hierarchien abzubauen, indem Wissen geteilt und zugänglich gemacht wird. Auch der Datenschutz spielt dabei eine Rolle: Hinter Open-Source Software stehen in der Regel keine gewinnorientierten Unternehmen, die ein Interesse an Nutzer\*innen Daten haben.


 Freie Software? "Um das Konzept zu verstehen sollte man an frei wie in Redefreiheit denken, nicht wie in Freibier."


**Wie könnte das aussehen?**
Hier ein paar  Beispiele für Anwendungsbereiche und passende Open-Sorce Programme, die ihr verwenden könnt:  
**Videokonferenzen**: Dafür steht inzwischen über die foodsharing Plattform das Programm [BigBlueButton](https://docs.bigbluebutton.org/) zur Verfügung. Für jeden Bezirk und jede Arbeitsgruppe gibt es einen eigenen Konferenzraum, den ihr über die Menüleiste → Bezirk/Arbeitsgruppe → Videokonferenz öffnen könnt.  
**Cloud Dienst**: Es gibt die Möglichkeit über einen eigenen Server eine Nextcloud zu hosten, in der ihr all eure Dateien ablegen und sammeln könnt. [https://nextcloud.com/ ](https://nextcloud.com/)  
**Office Programme**: Libre Office verfügt über alle Funktionen herkömlicher Office Software (Textdokumente, Tabellen, Präsentationen etc.) [https://de.libreoffice.org/](https://de.libreoffice.org/) 



![](/user/images/icons/opensource.jpg)