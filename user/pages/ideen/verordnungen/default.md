---
title: "Verordungen der öffentlichen Hand"
description: "Mit Verordnungen Lebensmittelwertschätzung in der Politik verankern"
published: true
---

### Es gibt rechtlich wirksame Beschlüsse der öffentlichen Hand, die Lebensmittelwertschätzung fördern.

Die freiwillige Mitarbeit aller lokalen Akteuer\*innen ist sehr wertvoll und eine gute Basis, um Veränderungsprozesse zu starten. Mit Vorschriften und Regelungen kann die öffentliche Hand diese Selbstverpflichtungen stärken, es Vorreiter\*innen einfacher machen und Prozesse beschleunigen.

**Wie könnte das aussehen?**
Die öffentliche Hand kann beispielsweise Verordnungen für öffentliche Mensen ändern oder lebensmittelwertschätzende Kriterien zur Vergabe von Aufträgen für das Catering bei öffentlichen Veranstaltungen einführen. Möglich sind auch Quoten für vegetarisches/veganes, gerettetes und/oder biologisch angebautes Essen auf Weihnachtsmärkten oder Stadtfesten. Denn die Organisation und Genehmigung aller Essensstände bei diesen Veranstaltungen liegt in der Hand der Stadt. Außerdem regelt die Stadtverwaltung die Grünflächen in der Stadt. Sie bestimmt, wo und was in Parks, an Seitenstreifen oder auf Friedhöfen gepflanzt werden darf. Die Grünflächenverordnung ist somit ein Ansatzpunkt für Urban Agriculture, ein Schritt in Richtung essbare Stadt oder für eine Bepflanzung, die das Leben von Insekten fördert. Zudem gehören der Stadt Räume und Flächen, die sie privaten Investor\*innen oder Inititiativen zur Verfügung stellen kann. Dies wird über Vergabeverfahren geregelt. Um Initiativen und Projekten Raum zur Verfügung zu stellen, die eine lebenswertere Gesellschaft und Stadt schaffen wollen, kann die Stadt die Kriterien dieser Vergabeverfahren anpassen. Mehr Informationen zu Kriterien für die Vergabe von Flächen findet ihr in der [Broschüre des Umweltbundesamtes](https://www.umweltbundesamt.de/publikationen/handlungsansaetze-fuer-die-foerderung).

Um solche Veränderungen anzustoßen, kann ein möglicher Weg sein, Beschlussvorlagen über Parteien in die Stadtverordnetenversammlung oder ähnliche Gremien einzubringen. Dies ist ein guter Anknüpfungspunkt für die politische Zusammenarbeit mit diesen und anderen Initiativen.


![](/user/images/icons/verordnungen.jpg)