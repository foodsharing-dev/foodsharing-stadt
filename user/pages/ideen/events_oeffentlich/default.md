---
title: "Öffentliches Events"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Aktionen der lokalen foodsharing Gruppe werden durch städtische Unterstützung beworben und realisiert (z. B. Schnippeldisko, SharingBrunch in öffentlichem Raum etc.).

Viele lokale foodsharing Gruppen veranstalten bereits selbst coole Aktionen oder sind bei Veranstaltungen anderer Organisationen präsent. Wir fänden es super, wenn es mehr davon gäbe. Ganz besonders toll wäre es, wenn die öffentliche Hand dabei unterstützt, z. B. in dem sie Räumlichkeiten oder bei Bedarf Gelder für die Realisierung der Projekte zur Verfügung stellt. Zum Gelingen beitragen kann auch die Bewerbung der Aktion (z. B. über einen lokalen Veranstaltungskalender, die Website der Stadt oder in einem Newsletter).

![](/user/images/icons/gemeinschaftliche_events.jpg)