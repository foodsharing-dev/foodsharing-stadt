---
title: "Herausragende Betriebskooperationen"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Möglichst viele örtliche (Super-)Märkte und Gastronomiebetriebe (z.B. Restaurants, städtische Mensen etc.) setzen sich aktiv und kreativ für Wertschätzung von Lebensmitteln ein. 

Wir wollen Lebensmittelwertschätzung zum Trend machen! Betriebe
sollen stolz darauf sein, dass sie ihre Lebensmittel nicht wegwerfen
und sich Maßnahmen überlegen, wie sie Verschwendung verhindern.
Vorbildliches Verhalten wäre z. B. das öffentliche Bekenntnis zur
Kooperation mit foodsharing oder einer anderen
Retter\*innen-Organisation. Oder sie informieren Kund\*innen über
guten Umgang mit Lebensmitteln (z.B. Unterschied MHD und
Verbrauchsdatum, Lagerung von Lebensmitteln). Weitere Ideen zu
Maßnahmen findet ihr in der Liste
[“Vorbildliches Verhalten:Tipps für Betriebe”](https://cloud.foodsharing.network/s/YDeGNKqBm72B78B).
Besonders vorbildliche Betriebe könnt ihr mit einem Zertifikat
auszeichnen, dass diese sich dann in ihr Schaufenster hängen, um so
ihr positives Verhalten sichtbar zu machen.


![](/user/images/icons/betriebskooperationen.jpg)