---
title: "Nachhaltiger Umgang mit Geld"
description: "Arbeiten mit Geld und die Wichtigkeit von Transparenz und Kommunikation"
published: true
---

### Geld kann als Ressource angesehen werden. Es wird besonnener und transparenter verwendet.

**Warum ist das wichtig?**
Manche Prozesse funktionieren in unserer Gesellschaft vorrangig oder ausschließlich mit Geld. Wir können uns dem Thema annehmen und  einen bestmöglichen Umgang damit finden. Dafür schlagen wir das Motto „So wenig wie möglich, so viel wie nötig“ vor.

**Wie könnte das aussehen?**
Wenn ihr ein Konto habt, könnt ihr bewusst entscheiden, bei welcher Bank ihr es eröffnet. Banken investieren Geld in unterschiedliche Bereiche. Bei der Auswahl könnt ihr auf ethische Grundsätze, Unternehmensmodelle oder Gemeinwohlanspruch der Bank achten. So sorgt ihr dafür, dass euer Geld keinen Schaden anrichtet.  
Außerdem könnt ihr euch darüber Gedanken machen, von wem ihr unter welchen Bedingungen Spenden annehmen wollt. Es kann hilfreich sein sich unabhängig von einer konkreten Spende dazu auszutauschen, damit ihr vorbereitet seit und darüber kein Streit in eurer Gruppe entsteht. Eine nahliegende und gute Quelle für Gelder sind in der Regel öffentlich Fördertöpfe. In manchen Städten gibt es sogenannte „Bürgerhaushalte“, bei denen man z.B. für bestimmte Vorhaben Gelder beantragen kann.  
Vielleicht stellt ihr euch sogar so auf, dass ihr Menschen aus eurer Gruppe für bestimmte Tätigkeiten bezahlen möchtet oder jemanden für eine Dienstleistung (z.B. ein Honorar für einen Vortrag) von euch bekommt. Dabei ist ist schön, wenn diese Menschen faire Gehälter bekommen. Oft sind finanzielle Ressourcen begrenzt, aber es ist sehr wertvoll, über Bedarfe offen zu sprechen und/oder sich am Branchenstandard zu orientieren. 
Insgesamt ist ein transparenter Umgang mit dem Thema wünschenswert. Legt offen, wieviel und wofür ihr Geld ausgebt und sorgt dafür, dass alle einen gerechten Zugang zu den zur Verfügung stehenden Mitteln haben. Es kann hilfreich sein, festzulegen, wer wie wofür Geld aus eurem foodsharing Topf bekommen kann und was dafür vorliegen muss.  
Ein Thema in diesem Kontext kann auch die Zusammenarbeit von bezahlten und unbezahlten Menschen (z.B. Foodsaver\*innen und Mitarbeiter\*innen der öffentlichen Hand) sein. Dabei ist es hilfreich, sich diese Tatsache und das damit einhergehende bewusst zu machen. Beispielsweise sind mit bezahlten Stellen häufig größere Zeitkapazitäten für Aufgaben verknüpft, aber es geht in der Regel auch eine gößere Verpflichtung für verschiedene Tätigkeiten damit einher. Eine Zusammenarbeit kann für alle Beteiligten sehr bereichernd sein, solange nicht Unzufriedenheit oder Missgunst damit einhergehen. Wir möchten euch ermutigen euch darüber auszutauschen, um Erwartungen und Bedürfnisse abzugleichen.



![](/user/images/icons/geld.jpg)