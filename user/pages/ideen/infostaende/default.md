---
title: "Infostände"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Bei einer öffentlichen Veranstaltung (z. B. Stadtfest, Weihnachtsmarkt o.ä) wird ein foodsharing Infostand organisiert. 

Bestimmt seid ihr bereits Expert\*in auf dem Gebiet der Lebensmittelrettung. Teilt euer Wissen mit anderen. Ihr könnt euren Stand ansprechend gestalten, indem ihr gerettetes Essen fairteilt (unbedingt mit den Veranstalter\*innen absprechen), ein kleines Quiz oder eine andere Mitmachaktion vorbereitet.

![](/user/images/icons/info_staende.jpg)