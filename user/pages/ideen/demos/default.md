---
title: "Beteiligung in politischen Bündnissen und Demos"
description: "die Relevanz von Kooperation mit anderen politischen Gruppen"
published: true
---

### Die lokale foodsharing Gruppe beteiligt sich an politischen Bündnissen und Demonstrationen.

Es gibt Unterschiede zwischen politischen Gruppen und ihren Mitgliedern und es ist wichtig, diese anzuerkennen. Gleichzeitig sind wir auf gegenseitige Unterstützung angewiesen und sollten darauf achten, nicht in Konkurrenz zu stehen oder unsere Ziele unter Benachteiligung anderer durchzusetzen. Deshalb ist es gut, sich umzuschauen und zusammenzuarbeiten. Welche anderen Bündnisse gibt es in unserer Stadt? Wo können wir kooperieren? Wie können wir dafür sorgen, dass unsere Vision von einer guten Gesellschaft alle einschließt?  

Kooperation und Unterstützung kann viele Formen annehmen. Ihr könnt zum Beispiel Aktionen mit geretteten Lebensmitteln unterstützen, gemeinsam Demonstrationen besuchen oder eure Events in einen größeren Zusammenhang setzen (beispielsweise als Aktion im globalen Streik oder an (deutschlandweiten) Aktionstagen). Zu Kooperationen können auch Austauschtreffen gehören. Das  Kennenlernen von neuen Gruppe und ihren Ziele kann spannende neue Perspektiven aufwerfen. So könnt ihr zum Beispiel reflektieren, für wen eure Gruppe zugänglich ist, wen euer Engagement bisher erreicht und was es noch für Möglichkeiten gibt, die Welt zu verändern. Konkrete Akteur*innen für die Beteiligung an politischen Bündnissen und Aktionen können zum Beispiel Fridays for Future, Schnibbeldiskos von Slowfood oder die „Wir haben es satt Demo“ sein.


![](/user/images/icons/politisches.jpg)