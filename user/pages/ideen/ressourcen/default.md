---
title: "Unterstützung durch Ressourcen der öffentlichen Hand"
description: ""
published: true
---

### Ressourcen werden im Sinne des Teilens genutzt und die Öffentliche Hand unterstützt foodsharing mit ihren Ressourcen.

**Warum ist das wichtig?**
Die lokale foodsharing-Gruppe verfügt über eine hohe Motivation und Know-How, um viele Lebensmittel vor der Tonne zu retten und Lebensmittelwertschätzung in die Öffentlichkeit zu tragen. Dabei kann die öffentliche Hand durch Bereitstellung ihrer Infrastruktur unterstützen. Im Sinne des Teilens von Ressourcen, könnte die Stadt foodsharing und anderen lokalen Organisationen ihre Ressourcen zur Verfügung stellen. Insbesondere, wenn sich die Nutzungszeiten unterscheiden, können so Ressourcen effizienter eingesetzt werden. 

**Wie könnte das aussehen?**
Die Stadt könnte foodsharing z.B. dauerhaft oder für regelmäßige Treffen einen Raum zur Verfügung stellen. Diese könnte auch für die Lagerung von Material für Öffentlichkeits- und Bildungsarbeit genutzt werden. Denkbar sind dafür auch leerstehende, sich im Besitz der öffentlichen Hand befindliche Ladenflächen. Auch eine temporäre Nutzung, z.B. für ein (Pop-Up) foodsharing Café oder einen Fairteiler mit beteiligung der öffentlichen Hand ist denkbar.
Darüber hinaus könnte die Stadt foodsharing Fahrzeuge zur (Mit-) Nutzung zur Verfügung stellen. Primär könnte es dabei um Lastenräder und gelegentlich Transporter für besonderes große Abholungen gehen. Dabei könnte relevant sein, dass foodsharing Abholungen z.B. nach Feierabend stattfinden, wenn städtische Fahrzeuge nicht mehr genutzt werden.
Ebenfalls interessant können Gegenstände für Stände oder andere foodsharing Aktivitäten sein (z.B. technisches Equitment, wie Musikanlagen, Zelte, Tische und Bänke, sowie Strom und Wasseranschlüsse).
Wenn ein direktes Teilen von vorhandenen Gegenständen oder Räumlcihkeiten nicht möglich ist (z.B. weil der organisatorische Aufwand zu groß wäre), kann die öffentliche Hand foodsharing alternativ durch Fördergelder unterstützen. Beispielsweise können einzelne Aktionen oder Projekte gefördert werden

![](/user/images/icons/ressourcen.jpg)