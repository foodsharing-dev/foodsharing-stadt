---
title: 'Blog unter Fremdregie: heute- Eislingen'
date: '22:00 01/05/2022'
taxonomy:
    category:
        - blog
first_image: 2022-01-05_fsStadt_Eislingen/20211213_175638.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Wir träumen davon, dass Zivilgesellschaft und Stadtverwaltung als foodsharing-Städte Hand in Hand für mehr Lebensmittelwertschätzung arbeiten. In diesem Blogbeitrag erzählt Mirjam davon, wie Eislingen foodsharing-Stadt wurde. Wir hoffen, dass es euch hilft, selbst mehr mit der öffentlichen Hand zusammenzuarbeiten und foodsharing-Stadt zu werden. 

### EISLINGEN IST FOODSHARING STADT

Der Landkreis Göppingen hat seine erste foodsharing-Stadt. Seit Montag den 13.12.21 ist die Stadt Eislingen, als zweite Stadt in Baden-Württemberg, offizielle foodsharing-Stadt.

Bei der Gemeinderatssitzung der Stadt Eislingen, am vergangenen Montag, wurde die foodsharing Initiative von Verena Scholz und mir vorgestellt.

Zur Idee von foodsharing- Städte hat sich in der Stadt Eislingen eine Gruppe gebildet, in der sich derzeit ca. 10 Menschen für dieses Thema engagieren.
Der Kirchengemeinderat der Lutherkirche Eislingen plant momentan seinerseits, bei der anstehenden Renovierung des Gemeindehauses in der Doktor-Engelstraße einen Fairteilerschrank einzurichten.

Die Bitte von Verena und mir an den Stadtrat war, die Motivationserklärung foodsharing-Städte zu unterschreiben und damit die öffentliche Erklärung der Stadt zu erhalten, dass sie diese Initiative unterstützen.

Zu unserer großer Begeisterung wurde der Antrag nicht nur einstimmig angenommen, sondern auch im Anschluss an die Abstimmung die Motivationserklärung sofort unterzeichnet.
Wir freuen uns sehr über die Offenheit und die Bereitschaft zur Unterstützung des gesamten (!) Stadtrates dem Projekt gegenüber und sind gespannt auf die nächsten Jahre der Zusammenarbeit.

![Die Südwest Presse verkündet die Neuigkeit: Eislingen ist foodsharing-Stadt](Artikel_eislingen.jpg)

Wir hoffen, dass es gemeinsam gelingt eine Bewusstseinsänderung bei den Konsument\*innen herbeizuführen. Der Oberbürgermeister Klaus Heininger fasste dies sehr gut zusammen: Das innere Umdenken müsse in den Köpfen beginnen, die Verbraucher\*innen hätten es in der Hand. Die umfangreiche Vernichtung von guten Lebensmitteln sei aber nicht nur nicht nachhaltig, sie sei auch moralisch verwerflich, insbesondere wenn man den Teil der Menschheit bedenke, die an Hunger leide oder verhungerten.

In die geplante Zusammenarbeit mit der Stadt fällt unter Anderem, dass foodsharing die Reste des Caterings bei offiziellen Anlässen oder Festen vor der Mülltonne bewahrt und verteilt.
In weiterer Zukunft könnte z.B. auch übriggebliebenes Essen aus Mensen oder Kantinen so vor der Vernichtung gerettet werden. Zudem kann die Stadt unkompliziert Räume zur Verfügung stellen, wenn die foodsharing Initiative Öffentlichkeitsarbeit betreiben will, z.B. bei sogenannten Schnippelpartys, bei denen man gemeinsam mit geretteten Lebensmitteln kocht.
Lebensmittel zu retten, den Fairteilerschrank verantwortlich und hygienisch sicher zu betreiben und gute Aufklärung zu leisten, ist ein enormer Aufwand. Darum freuen sich die Foodsaver\*innen immer über neue Mitstreiter\*innen.

Liebe Grüße Mirjam
