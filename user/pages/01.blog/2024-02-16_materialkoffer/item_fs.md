---
title: 'Jetzt bewerben: Materialkoffer der foodsharing Akademie'
date: '12:00 02/16/2024'
taxonomy:
    category:
        - blog
first_image: 2024-02-16_materialkoffer/Materialkoffer.png
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---

Wir freuen uns euch heute den ersten Vorgeschmack auf eine neue Idee geben zu können: Den Materialkoffer der foodsharing Akademie. Es ist ein Koffer voller Bildungsmaterialien, welcher die lokale Bildungsarbeit in den Bezirken stärken und unterstützen soll. Mit ihm seid ihr z.B. für Workshops an Schulen, für Vorträge, aber auch für Infostände bestens ausgerüstet. Er baut auf dem [Methodenbuffet der foodsharing Akademie](https://www.foodsharing-akademie.org/materiallager/) auf und wurde innerhalb der letzten Jahre mit viel Liebe zum Detail konzipiert.

Nun befindet sich das Projekt auf der Zielgeraden und es wird Zeit die ersten Koffer zu verteilen. Ihr habt Lust so einen Koffer in eurem Bezirk zu verwenden?

[Klickt hier für weitere Informationen zum Koffer sowie zur Bewerbung](/materialkoffer)

**Bewerbungsschluss ist der 05.03.2024**

Wir sind gespannt und freuen uns auf eure Nachrichten!

*Euer Team der foodsharing Akademie*