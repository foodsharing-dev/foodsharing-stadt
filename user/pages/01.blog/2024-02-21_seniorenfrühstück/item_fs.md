---
title: 'Ein besonderes Senior*innenfrühstück im Zeichen der Nachhaltigkeit'
date: '12:00 02/16/2024'
taxonomy:
    category:
        - blog
first_image: 2024-02-21-seniorenfrühstück/blog_eislingen.png
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
In der foodsharing-Stadt Eislingen hat ein besonderes Frühstück stattgefunden. In unserem Gastbeitrag erzählen die Beteiligten von ihrer Veranstaltung:

Am 24. Januar 2024 erlebten die Senior\*innen des regelmäßig stattfindenden Seniorenfrühstücks im Luthergemeindehaus in Eislingen einen ganz besonderen Vormittag. Anstelle des gewohnten Ablaufs stand diesmal die Verteilung geretteter Lebensmittel im Mittelpunkt, begleitet von einem inspirierenden Vortrag unter der Leitung von Eva-Maria Hahlbeck-Stehle und Iris Euteneuer im Rahmen der Initiative "FreeFood e.V".

FreeFood e.V, ein gemeinnütziger Verein, der einen Verteilerschrank am Luthergemeindehaus in Eislingen betreibt, setzt sich aktiv für die Reduzierung von Lebensmittelverschwendung ein. Der Vortrag diente nicht nur als soziales Ereignis, sondern auch als Beitrag zur Sensibilisierung für nachhaltiges Handeln. Die Veranstaltung bot eine bedeutungsvolle Erfahrung für alle Teilnehmer und führte zu durchweg positiver Resonanz.

Die Senior\*innen zeigten sich erstaunt und verärgert darüber, wie viel Lebensmittel verschwendet und somit nicht geschätzt werden. Im Abschluss erklärte Frau Hahlbeck-Stehle den Senior\*innen, wie sie über die WhatsApp-Gruppe Informationen zu öffentlichen Verteilungen erhalten können, und half dabei, die Gruppe am Handy hinzuzufügen. Diese Diskussionsrunde wird sicherlich noch einigen lange in guter Erinnerung bleiben.

Die gelungene Veranstaltung weckte bei vielen Interesse und eine Wiederholung, um das Bewusstsein für nachhaltiges Handeln weiter zu fördern, wird angestrebt. FreeFood e.V bedankt sich herzlich bei den Senior\*innen für das erfolgreiche "Diskussionsfrühstück". 

Betriebe, die gerne mit FreeFood zusammenarbeiten möchten, sind herzlich eingeladen, sich bei der Initiative zu melden, um gemeinsam gegen Lebensmittelverschwendung anzukämpfen.

Weitere Informationen findet ihr unter: https://www.freefood-ev-goeppingen.de/

