---
title: 'Schwung in die Bewegung: Online Team Wochenende von foodsharing-Städte'
date: '12:00 10/31/2020'
taxonomy:
    category:
        - blog
first_image: 2020-10-31-treffen/20201024_Teamfoto.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Vergangenes Wochenende hat sich das foodsharing-Städte Orgateam online getroffen. Zusammen haben wir auf die bisherige Entwicklung der Bewegung “foodsharing-Städte” und unsere Zusammenarbeit geschaut.

Gestartet sind wir am Freitag mit einem kleinen Spieleabend: Passend zur Bewegung haben wir uns eine Abwandlung von Stadt, Land, Fluss überlegt, bei der wir die klassischen Kategorien ersetzt haben, durch persönlichere Fragestellung (z.B. Orte, an denen wir schon waren oder Essen, das wir gerne mögen). Das hat auch digital gut funktioniert und Spaß gemacht! Hier unsere Version zum Nachspielen: https://cloud.foodsharing.network/s/Q6n36PTLHoAREqE 

Den Samstag haben wir dann vor allem dem Thema Reflektion gewidmet und verschiedene Methoden genutzt. Unter anderem haben wir mit der “Zielscheibe” auf Aspekte unserer Tätigkeit geschaut und jede\*r konnte seine Einschätzung dazu abgeben (je weiter der Punkt in der Mitte ist, dasdo besser geht es einer Person mit dem Thema).

![Zielscheibe](online.jpg)

Anschließend haben wir darüber gesprochen und teilweise Veränderungen beschlossen. So gibt es ab jetzt z.B. zweimal wöchentlich eine online Co-working Session, bei der wir gemeinsam an der Gestaltung der Bewegung arbeiten. Als Team organisieren wir den Webauftritt, die Öffentlichkeitsarbeit, das Design, die inhaltliche Ausarbeitung und die Vernetzung zwischen den teilnehmenden Städten. 

Falls Du Lust bekommen hast, mit uns gemeinsam die Bewegung foodsharing Städte zu organisieren und mitzugestalten, komm gerne in die Gruppe “foodsharing Städte – Organisationsteam” auf foodsharing.de oder schreib uns an kontakt@foodsharing-staedte.org 

**Save the Date – für teilnehmende Städte** 

Am 05.11. findet ein online Vernetzungstreffen mit einem spannenden Input zum Thema “Kooperation mit der öffentlichen Hand aus der Praxis in Jena” von Sebastian statt. Wir freuen uns, wenn ihr dabei seid! Kommt dazu gerne in die Gruppe “foodsharing Städte – Vernetzung” auf foodsharing.de.

*Von Clara*


