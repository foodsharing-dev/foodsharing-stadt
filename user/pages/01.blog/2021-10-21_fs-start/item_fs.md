---
title: 'Wie gründet man eigentlich eine foodsharing-community?'
date: '22:00 10/21/2021'
taxonomy:
    category:
        - blog
first_image: 2021-10-21_fs-start/Team_icon.png
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Wie gründe ich eine foodsharing-Stadt Gruppe?

In unserer letzten Veranstaltung hat Thomas aus der foodsharing-Stadt St. Ingbert und dem ersten foodsharing-Landkreis uns mit zu den Anfängen genommen: Wie gründe ich eine foodsharing-Stadt Gruppe? Wie sorge ich dafür, dass die Gruppe besteht und nachhaltig wächst? Und wie initiiere ich gute Zusammenarbeit mit der Stadt zusammen?

In St. Ingbert lief der Weg für die Zusammenarbeit mit der Stadt über einen Antrag im Stadtrat. Thomas rät uns, diesen Weg zu gehen, denn: wer sollte schon nein sagen? In St.Ingbert, Blieskastel und im Landrat des Saarpfalz-Kreises wurde einstimmig für ein Mitwirken bei foodsharing-Städte gestimmt. Der eingebrachte Antrag kann auch für andere Städte genutzt werden. Ihr findet ihn zusammen der Präsentation unter diesem Link: https://cloud.foodsharing.network/s/xNoyo2FiixJYYni
    
Der Tipp für eine funktionierende Gruppe ist gute und offene Kommunikation. In St.Ingbert gibt es regelmäßge Treffen, die für alle zugänglich gemacht werden sollen. Für Menschen, die neu in die Gruppe finden wollen und Menschen, die  nicht physisch dabei sein können. 

Natürlich ist das nicht alles, aber
Thomas kann diese Geschichten am besten selbst erzählen. Schaut euch den Vortrag gerne hier an: 
https://www.youtube.com/watch?v=h6icjfUxz5o

*Von Manon*

