---
title: 'Wie funktioniert ein foodsharing Café ?'
date: '13:00 02/26/2021'
taxonomy:
    category:
        - blog
first_image: 2021-02-26raupe/raupe.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
“Mein Internet ist leider weg. Es war super spannend :) Wann eröffnen wir unser foodsharing Café?”, fragt Clara mich am Donnerstagabend. 
Das ist eigentlich alles, was wir über unsere Veranstaltung zu foodsharing Cafés mit Max von der Raupe Immersatt sagen müssen. Aber natürlich ist das nicht alles, was wir sagen können.

Zurück auf Anfang:
In einem online-Vortrag erzählt uns Max wie das erste foodsharing Café  in Deutschland entstanden ist. Ein Kernteam von fünf Menschen, ohne Gastroerfahrung mit viel Motivation, möchte Lebensmittelverschwendung bekämpfen, denkt sich ein Konzept aus, gründet einen Verein, bildet sich weiter und sammelt Startkapital über eine Crowdfunding Kampagne. Nach zwei Jahren Raumsuche und
anschließender Renovierung öffnet dann endlich die Raupe Immersatt im Westen Stuttgarts. Im Café werden gerettete Lebensmittel kostenlos fairteilt, während die Kosten durch den Getränkeverkauf
gedeckt werden. Aber Max und die anderen Mitglieder der Raupe können ihre Entstehungsgeschichte viel besser erzählen als ich (zum Nachlesen https://www.raupeimmersatt.de/).
 
Was ich vor allem mitnehme ist, dass ein foodsharing Café ein Ort ist, um Veränderung zu leben. Durch den Fairteiler und das ausgegebene gerettete Essen im Café kommen Menschen mit unserer Wegwerfgesellschaft in Kontakt. Die Getränke werden möglichst von lokalen Betrieben geliefert und kosten so viel, wie jede\*r zahlen kann und möchte. Diese Kombination regt zum Nachdenken an: Wie viel Wert steckt eigentlich in Lebensmitteln? Auf welche Arten kann ich diesen Wert anerkennen? 

Als foodsaver\*innen sind unsere Teilnehmenden mit diesen Fragen vertraut, sie beschäftigt noch etwas anderes. Viele der gestellten Fragen drehen sich daher um das Finanzierungskonzept und… das “zahl, was du willst” Prinzip funktioniert! Zum einen deckt es die Kosten des Cafés, inklusive der Gehälter der Mitarbeitenden, die alle den gleichen Nettostundenlohn erhalten. Zum anderen ist es für mich wieder ein Weg, Veränderung zu leben. Das foodsharing Café stößt die Gäste darauf, was selbstverständlich und alternativlos scheint. Verweilen ohne Konsumzwang, selbst entscheiden, was mir der Kaffee an Geld wert ist und dazu Essen durchs essen wertschätzen und nicht mit einem bedruckten Stück Papier. Warum ist das anderswo undenkbar? 

Alles ist also darauf angelegt, mit Menschen ins Nachdenken und Gespräch zu kommen, übers Teilen und Solidarität, über Probleme und Alternativen. 

Soviel zu den Momenten, in denen ein foodsharing Café seine Gäste aufrütteln kann. Als Mensch, der auch Lust hat, Gastgeber\*in zu sein, möchte ich aber noch zwei andere Erkenntnisse aus der Veranstaltung mit euch teilen.
Max hat erzählt, wie die Inneneinrichtung des Cafés vom Team selbst eingesammelt, abgeschliffen und poliert wurde. Das Podest, mit Schubladen für Kinderspielzeug, hat dann ein Schreiner angefertigt. Das zeigt mir, dass ein motiviertes Team vieles schaffen und lernen kann, und manchmal doch Hilfe von gelehrten Menschen braucht. 
Zweite Erkenntnis: Die Crowd, die das Startkapital bereitstellt, gibt es.
Und sie kann auch aus anderen Landesteilen kommen. Außerdem kann die Unterstützung so vieler, breit verstreuter Menschen motivieren weiterzumachen (zum Beispiel angesichts astronomischer Mietpreise und
Raumknappheit).
 
Fazit: foodsharing Cafés sind eine coole Sache und wenn man für etwas brennt, dann klappts auch (irgendwann). Zurückbleibt eigentlich nur die Frage: Was wollen wir bei uns ändern und wann legen wir los?  

Einen guten Start in die Veränderung 

wünscht Manon


P.S.: Es gibt noch viele andere Wege, wie ihr Veränderung vor Ort anstoßen könnt.
Schaut doch mal in unserem Ideenkatalog (https://foodsharing-staedte.org/de/ideenkatalog) vorbei und lasst euch inspirieren. 


P.P.S.: Falls ihr jetzt in den Startlöchern steht, aber doch noch Fragen zur Gründung von foodsharing Café s habt, meldet euch gerne bei uns und/oder der Raupe Immersatt.

*Von Manon*
