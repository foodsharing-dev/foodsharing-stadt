
---
title: 'Neuenhagen bei Berlin ist erste foodsharing-Stadt in Brandenburg'
date: '01/12/2023'
taxonomy:
    category:
        - blog
first_image: 2023-01-12-neue-stadt-neuenhagen/neuenhagen.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Neuenhagen: Unterzeichnung des Motivationsvertrags mit der Stadt

Neuenhagens Bürgermeister Ansgar Scharnke, die Botschafterin der Bezirksgruppe foodsharing Neuenhagen/Hoppegarten, Andrea Brackertz, und die Vorsitzende der Gemeindevertretung Neuenhagens, Dr. Ilka Goetz, haben am 12. Januar 2023 die „Motivationserklärung foodsharing-Städte“ unterzeichnet. Damit ist die Gartenstadt die erste Kommune in Brandenburg, die sich foodsharing-Stadt nennen darf. Andrea Brackertz zeigte sich begeistert: „Ich bin total glücklich, dass wir es geschafft haben Neuenhagen als foodsharing-Stadt zu etablieren. Wir haben lange dafür gearbeitet. Es wäre schön, wenn wir durch den neuen Status noch mehr Menschen und Läden gewinnen könnten, die bei uns mitarbeiten und zukünftig noch mehr Lebensmittel gerettet werden können.“ Ansgar Scharnke dankte allen Lebensmittelrettern in der Gemeinde für ihr Engagement und überreichte Andrea Brackertz ein Handtuch mit dem Wappen der Gartenstadt: „Ihr ehrenamtlicher Einsatz ist sicher oft schweißtreibend. Für die anstehenden Einsätze soll dieses Handtuch ihrem gesamten Team symbolisch den Rücken stärken. Die Gemeindeverwaltung wird sie in Zukunft bestmöglich unterstützen.“

Angestoßen wurde der Beschluss von den örtlichen Fraktionen Bündnis 90/Die Grünen, der SPD, der CDU und der Fraktion Wählergemeinschaft Die Parteilosen. In der Gemeindevertretersitzung vom 28. November 2022 erhielt der Antrag ein einstimmiges Votum. Damit erklärt sich die Gemeinde Neuenhagen bei Berlin bereit, die Arbeit von foodsharing in Neuenhagen zu unterstützen und dem Ziel einer höheren Wertschätzung von Lebensmitteln und der Reduktion von Lebensmittelverschwendung näher zu kommen. Dr. Ilka Goetz, Vorsitzende der Gemeindevertretung, erklärte bei der Unterzeichnung der Motivationserklärung: „Die Weitergabe nicht benötigter Lebensmittel trägt zum ressourcenschonenden Umgang mit Natur und Umwelt bei. Mit der öffentlichen Erklärung zur Teilnahme an der Foodsharing-Initiative unterstützen wir auch zivilgesellschaftliches Engagement und machen es sichtbar.“ 

In der Gemeinde Neuenhagen gibt es mittlerweile mehr als 200 Lebensmittelretter, die die Foodsaver Andrea Brackertz und Nicole Waluga für dieses Projekt gewinnen konnten. Aktuell werden Lebensmittel in 15 Betrieben von den ehrenamtlich Engagierten gerettet. Bislang konnten bei mehr als 1.400 Rettungseinsätzen knapp 18.000 Kilogramm Lebensmittel vor dem Wegwerfen bewahrt werden. Anlaufstellen für die noch zu verwendenden Produkte sind etwa das Haus der Senioren, ein Wohnprojekt der Lebenshilfe oder die Jugendwerkstatt Hönow e.V. Die foodsharing-Initiative entstand in Deutschland im Jahr 2012. Seitdem machen sich die Ehrenamtler für einen verantwortungsvollen Umgang mit Ressourcen und gegen Lebensmittelverschwendung stark. Die Gruppe in Neuenhagen ist seit dem 26. Februar 2017 aktiv.

Schaut doch mal auf der Seite von Neuenhagen vorbei: [Hier](https://foodsharing-staedte.org/de/stadt/neuenhagen)
 

*Pressemitteilung aus Neuenhagen*



