---
title: 'Es geht los!'
date: '12:00 12/12/2019'
taxonomy:
    category:
        - blog
first_image: 2019-12-12_Release/00_blaetter_2.png
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle: 'Die Bewegung der foodsharing-Städte beginnt'
---

Schön, dass du den Weg auf unsere Website gefunden hast. Seit Sommer 2018 basteln wir an der Idee und jetzt ist sie Wirklichkeit geworden. Pünktlich zum 7. foodsharing-Geburtstag geht die Seite www.foodsharing-staedte.org online. 

Worum geht es?
foodsharing ist mehr als Lebensmittelretten! Und genau das wollen wir hier darstellen. Wir möchten den Austausch zwischen den Städten fördern und mit Ideen dazu anregen, gemeinsam lokal  aktiv zu werden. Ein besonders großes Anliegen ist uns dabei die Zusammenarbeit zwischen der Zivilgesellschaft (Foodsaver\*innen sowie Bewohner\*innen der Stadt, die bisher noch keine Berührung mit foodsharing hatten) und der öffentlichen Hand (also der Stadt-/Gemeindeverwaltung, Abgeordneten und Bürgermeister\*innen). Wir sind überzeugt davon, dass wir durch gemeinsames Engagement positiv zur weltweiten Lebensmittelwertschätzung beitragen können. 

![](Bewegung_Land_Stadt.jpg)

Mehr zur Vision der Bewegung foodsharing-Städte findet ihr [hier](/bewegung/vision).

*Euer foodsharing-Städte Team*