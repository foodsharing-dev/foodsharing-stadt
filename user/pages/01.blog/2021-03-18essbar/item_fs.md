---
title: 'Wie wird man eigentlich eine essbare Stadt? - oder warum wir alle in Köln oder Eupen wohnen wollen'
date: '13:00 03/18/2021'
taxonomy:
    category:
        - blog
first_image: 2021-03-18essbar/essbar.jpeg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Essbare Stadt klingt erstmal gut. Was sich hinter dem Konzept
verbirgt haben uns Judith Mayer vom Ernährungsrat in Köln und
Alexandra Hilgers, die als Umweltberaterin bei der Stadtverwaltung
Eupen arbeitet, am 18.03.2021 in einem gutbesuchten Online-Austausch
im Rahmen der foodsharing Städte-Vortragsreihe erzählt.

In Köln wurde zwei Jahre lang an einem Konzept zu verschiedenen Themen
wie essbares öffentliches Grün, urbane Gemeinschaftsgärten und
partizipative Landwirtschaft, gefeilt. Unter anderem in Barcamps
konnten Bewohner*innen der Stadt Köln ihre Ideen für ihre Stadt
einbringen und mit anderen diskutieren. In dem Prozess entstand ein
Aktionsplan, der schließlich auch in eine Stadtratsbeschlussvorlage
mündete. Am 4. Juni 2020 hat der Ausschuss “Umwelt und Grün”
der Stadt Köln dem Aktionsplan ,,Essbare Stadt Köln” zugestimmt
und Köln wurde von offizieller Seite zur essbaren Stadt. 

In Eupen lief es ganz anders ab, verschiedene Projekte wurden umgesetzt
und sind nach und nach zusammengewachsen. Um uns die verschiedenen
Projekte vorzustellen, nimmt Alex uns mit auf einen digitalen
Rundgang durchs essbare Eupen. Sie erzählt zu jedem Projekt, wo die
finanziellen Mittel herkamen, und lässt uns an Erfolgen und
gescheiterten Aktionen teilhaben. 

Wie man an der anschließenden angeregten Diskussion gemerkt hat, haben
den zeitweise bis zu 80 Teilnehmenden die Beiträge sehr gut gefallen
und sie inspiriert. Es wurden viele Nachfragen zu praktischen Punkten
der Umsetzung der Projekte gestellt und man merkte, dass viele Leute
gerade an verschiedenen Orten in den Startlöchern stehen, um ihre
Kommunen essbarer zu gestalten. 

Wer den Vortrag verpasst hat oder noch mal nachhören möchte, findet die Video-Aufzeichnung hier: https://www.youtube.com/watch?v=9OqmUk4CpDQ

Weitere Infos zur essbaren Stadt Köln: 

https://www.essbare-stadt.koeln

Einen Überblick über das Engagement in Eupen, könnt ihr euch hier verschaffen: 

https://www.foodsharing-staedte.org/de/stadt/eupen


*Von Rebecca*

