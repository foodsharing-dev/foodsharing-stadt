---
title: 'Ernährungsräte'
date: '13:00 05/11/2021'
taxonomy:
    category:
        - blog
first_image: 2021-05-11_ernaehrungsrat/playmobil.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Am Dienstag, den 27.05. hatten wir das Glück, dass Anna Wissmann – Expertin für Ernährungsräte - sich Zeit genommen hat, um uns einen Input zu geben und für alle Rückfragen zur Verfügung zu stehen.

**Was sind Ernährungsräte?**

Ernährungsräte haben das Ziel wortwörtlich “alle an einen runden Tisch zu holen”: Bürger\*innen, Aktivist\*innen, die lokale Politik und die regionale (Land-) Wirtschaft. Gemeinsam wollen sie ihr lokales Ernährungssystem aktiv gestalten. Häufig beginnt die Arbeit eines Ernährungsrates mit einer Bestandsaufnahme, um daraus eine gemeinsame Vision und eine Strategie zu entwickeln. Im Rahmen dieser Strategie wird dann Einfluss auf das Ernährungssystem genommen – oft über die Förderung von urbaner Landwirtschaft, die Einrichtung und Förderung von Gemeinschaftsküchen, die Förderung regionaler Ernährungssysteme, die Bekämpfung sozialer Benachteiligung im Ernährungsbereich, die Optimierung der Gemeinschaftsverpflegung und die Veranstaltung von Konferenzen und anderen Events.
Weitere Infos unter: [http://ernaehrungsraete.de/](http://ernaehrungsraete.de/) 

**Was war besonders spannend an der Veranstaltung?**

Mir ist besonders das Thema “Framing” im Kopf geblieben. Da bei Ernährungsräten viele verschiedene Akteur\*innen zusammenkommen, ist es wichtig sich gut aufeinander einzustimmen. Dazu gehört neben dem persönlichen Kennenlernen auch das Finden einer gemeinsamen Sprache. 
Das ist eine tolle Gelegenheit um die Wörter und Ausdrücke, die wir manchmal sehr unbewusst verwenden, genau unter die Lupe zu nehmen. Denn: Sprache ruft in uns Bilder hervor, welche wiederum sehr unterschiedliche Verknüpfungen in unserem Denken anregen. So kann es z.B. einen Unterschied machen, ob wir von Bürger\*innen oder Konsument\*innen sprechen. Beides sind Menschen. Bei dem Wort Kosument\*innen wird ihnen jedoch eine eher passive Rolle zugesprochen. Sie stehen ganz am Ende der “Wertschöpfungskette”. Beim Wort Bürger\*innen hingegen, schwingt der Zuspruch von Gestaltungskompetenz mit. Sie sind aktive Mitglieder in unserem “Ernährungssystem”.

Auch bei der foodsharing Städte-Bewegung haben wir uns schon mit dem Thema “Framing” auseinander gesetzt. Wir versuchen z.B. statt von “weniger Lebensmittelverschwendung” lieber von “mehr Lebensmittelwertschätzung” zu sprechen. Dieser Ausdruck ist positiver und steht für uns umfangreicher für den Umgang mit Essen, den wir uns wünschen!


**Übrigens:**

Anna hat erzählt, dass häufig Foodsaver\*innen Mitinitiator\*innen oder Mitstreiter\*innen der ersten Stunde von neuen Ernährungsräten sind! Sehr cool, wie wir finden. Seid ihr vielleicht die Nächsten?


Veranstaltung verpasst?
Kein Problem! Hier findest du einen Link zur Präsentation und eine Aufzeichnung des Vortrags:
- [Präsi](https://cloud.foodsharing.network/s/kFoCKJToj8kDmGL) 
- [Youtube](https://www.youtube.com/watch?v=s6ldV6TiKJI)


*Von Clara*

