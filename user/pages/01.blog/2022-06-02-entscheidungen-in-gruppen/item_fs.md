---
title: 'Gewusst wie: Gutes, nachhaltiges Arbeiten in Gruppen'
date: '06/09/2022'
taxonomy:
    category:
        - blog
first_image: 2022-06-02-entscheidungen-in-gruppen/gruppen.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Funktionierende Gruppenprozesse sind der Kern einer funktionierenden foodsharing-Gemeinschaft.

Doch wie treffen wir Entscheidungen? Wie organisieren wir unsere Treffen? Wie schaffen wir eine Gruppe, in der alle gut und gerne zusammen arbeiten? Nathalie und Clara haben in unserem letzten Vortrag Einblicke in gute Gruppenprozesse- und Strukturen gegeben. Beide beschäftigen sich schon länger mit Gruppen und ihren Dynamiken, vorallem im foodsharing-Rahmen.

Sie konnten uns einen ganzen Werkzeugkoffer anbieten, um Gruppenprozesse möglichst gut zu gestalten. Beispielsweise einen gemeinsamen Gruppenleitfaden zu schreiben, in der die Gruppe ihr Ziel, ihre Tätigkeit und ihre Entscheidungsprozesse festhält.

Zur Entscheidungsfindung haben sie verschiedene Methoden vorgestellt, wie Mehrheitsentscheid, Konsens, Doocracy oder den Konsent. Wichtig ist es auch regelmäßig in der Gruppe zu reflektieren, um sich gegenseitig zu stärken und Konflikte zu vermeiden. Desweiteren wurden viele Ausblicke zu weiteren Themenfeldern (z.B. Soziokratie) gezeigt. Die vielen Links machen Lust auf mehr.

Schaut euch den Vortrag unbedingt auf [Youtube](https://www.youtube.com/watch?v=DpmZG3vMjx4) an, falls ihr ihn verpasst habt.

Die Folien findet ihr [hier](https://cloud.foodsharing.network/s/A6CJwQrJQWRdBdm).
 

*Von Sebastian*


