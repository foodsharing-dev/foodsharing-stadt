---
title: 'Blog unter Fremdregie: heute- St. Ingbert'
date: '22:00 10/21/2021'
taxonomy:
    category:
        - blog
first_image: 2021-12-03_VHS_StIngbert/VHS_Stingbert.jpeg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Wir träumen davon, dass Menschen sich gegenseitig mit ihrem Wissen unterstützen und voneinander und gemeinsam Lernen. Wie viele andere unserer teilnehmenden Städte setzt die foodsharing-Stadt St.Ingbert diesen Traum in der Bildungsarbeit um. In diesem Blogbeitrag stellt Thomas die seit kurzem bestehende Zusammenarbeit von foodsharing und der Volkshochschule vor. Wir freuen uns, heute von St. Ingbert zu lernen und hoffen, dass es einige von euch in eurer Bildungsarbeit inspiriert. 

### Weiterbildungspartnerschaft von foodsharing St. Ingbert mit der städtischen  Biosphären-Volkshochschule St. Ingbert

Seit Oktober 2021 läuft sie endlich: Die Weiterbildungspartnerschaft von foodsharing St. Ingbert mit der Biosphären-VHS St. Ingbert.
Schon in der Vergangenheit gab es immer mal wieder Infoveranstaltungen von foodsharing St. Ingbert bei der VHS. Aber nachdem St. Ingbert foodsharing Stadt wurde, wurde aus einer lockeren Kooperation die Weiterbildungspartnerschaft. Unter www.vhs-igb.de findet man nun auch die unterschiedlichsten Angebote von foodsharing St. Ingbert. Meist kostenfrei – wenn überhaupt, sind nur zusätzlich anfallende Kosten von den Teilnehmer*innen zu übernehmen.

Die Biosphären-VHS St. Ingbert übernimmt Organisation, Bereitstellung der (virtuellen) Räumlichkeiten und Technik, sowie das Marketing (Online, social media & Presse). Die foodsharing Community sorgt für Themen und Dozent\*innen. In der Kürze konnten schon 4 Dozent\*innen für 3 unterschiedliche Maßnahmen aus den eigenen Reihen gefunden werden.
Die Botschafter*innen Bianca und Thomas sind schon mit Infoveranstaltungen über foodsharing St. Ingbert gestartet. Diese werden in Zukunft alle 2 Monate angeboten. Kerstin, ebenfalls Botschafterin, bereitet gerade den Workshop: "Plastikfrei und selbstgemacht - DIY Deo und Handpflege" vor, der am 21.01.22 erstmals in der „Grünen Neune“, dem Creativcafe in St. Ingbert in Zusammenarbeit mit der VHS stattfinden wird.

Der Start der lebendigen Vortragsreihe: "Mit allen Sinnen: Lebensmittelkunde für den Haushalt", die sich bis zu den Sommerferien hinzieht, ist für den 08.01.22 geplant. Referentin der Vortragsreihe ist unsere foodsaverin Hannah. Sie hat Oecotrophologie in Gießen und Fulda studiert und war in den vergangenen zehn Jahren in der Gemeinschaftsverpflegung tätig. Aktuell ist Sie im Referat Ernährung des Ministeriums für Umwelt und Verbraucherschutz.
Wo es möglich ist, werden die Angebot in hybrider Form angeboten. Dies bedeutet, dass die Teilnahme sowohl vor Ort als auch Online möglich sein wird. Weitere Veranstaltungen sind in Planung.

*von Thomas für das foodsharing-Städte Team in St.Ingbert *
