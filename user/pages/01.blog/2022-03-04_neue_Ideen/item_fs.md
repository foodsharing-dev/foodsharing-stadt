---
title: 'Neue Ideen für eure Bäume'
date: '03/04/2022'
taxonomy:
    category:
        - blog
first_image: 2022-03-04_neue_Ideen/22_idea.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Liebe foodsharing-Städte Teams,

der Frühling steht an und mit ihm fangen die Bäume an zu blühen. Damit auch eure Bäume bald viele Früchte tragen können, haben wir sechs neue Ideen für euch parat. Diese sind:

- [Jugendarbeit](/ideen/jugendarbeit)
- [Politische Bündnisse und Demonstrationen](/ideen/demos)
- [Unterstützung durch Ressourcen der öffentlichen Hand](/ideen/ressourcen)
- [Verordnungen der öffentlichen Hand](/ideen/verordnungen)
- [Nutzung von Open Source Software](/ideen/opensource_software)
- [Nachhaltiger Umgang mit Geld](/ideen/umgang_geld)

Ihr findet sie im Ideenkatalog hier auf der Homepage.
Also schüttelt den Winterschlaf ab und macht euch mit neuer Motivation daran, unsere Idee vorwärts zu bringen und gemeinsam eure Städte nachhaltig zu verändern.
Wir sind gespannt darauf, was ihr umsetzt und freuen uns auf eure kurzen Statements für eurer foodsharing-Städte Profil.
Habt ihr Wünsche oder Fragen, dann wendet euch gerne an: [teilnehmende@foodsharing-staedte.org](mailto:teilnehmende@foodsharing-staedte.org)

 

*Von Mirjam*

