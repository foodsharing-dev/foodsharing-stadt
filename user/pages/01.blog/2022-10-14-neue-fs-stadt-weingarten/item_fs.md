
---
title: 'Wir begrüßen Weingarten als neue foodsharing-Stadt'
date: '10/14/2022'
taxonomy:
    category:
        - blog
first_image: 2022-10-14-neue-fs-stadt-weingarten/weingarten.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Weingarten: Unterzeichnung des Motivationsvertrags mit der Stadt

Endlich sind wir offiziell foodsharing-Stadt. 😊

Am 18.05.2022 wurde der Motivationsvertrag von unserem neuen Bürgermeister Alexander Geiger und zwei Vereinsmitgliedern unterzeichnet. 
Damit setzen wir gemeinsam mit der Stadt Weingarten ein Zeichen gegen die Lebensmittelverschwendung. Wir freuen uns auf die gute Zusammenarbeit und hoffen durch unser Engagement mit Aktionen, Bildungsangeboten und unseren täglichen Abholungen nachhaltig Veränderung zu bewirken und ein Umdenken anzustoßen. 

Schaut doch mal auf der Seite von Weingarten vorbei: [Hier](https://foodsharing-staedte.org/de/stadt/weingarten)
 

*Von Lena aus Weingarten*


