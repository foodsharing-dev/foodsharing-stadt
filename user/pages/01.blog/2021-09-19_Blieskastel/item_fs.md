---
title: 'Herzlich Willkommen Blieskastel!'
date: '10:00 09/19/2021'
taxonomy:
    category:
        - blog
first_image: 2021-09-19_Blieskastel/Blieskastel_Bliesbrücke.JPG
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Wir freuen uns, Blieskastel als neue foodsharing-Stadt willkommen zu heißen!

In der Stadt im Saarpfalz-Kreis wurden von 119 foodsaver\*innen in nur einem Jahr mehr als 12.000 kg Lebensmittel über foodsharing gerettet. Trotz der Coronapandemie hat sich die Gruppe in Blieskastel nicht in ihrer Arbeit für mehr Lebensmittelwertschätzung aufhalten lassen. Einführungen in die Nutzung des Fairteilers wurden virtuell mit einem selbstgedrehten Video durchgeführt und auch zwei Infoveranstaltungen haben schon stattgefunden. 

Lebensmittelwertschätzung wird nicht nur von der foodsharing-Gruppe vorangetrieben, sondern auch von der Stadt unterstützt zum Beispiel mit dem Bürgergarten. Besonders gespannt sind wir auf den gemeinsamen Fairteiler mit der Stadt, der bereits in Planung ist.

Beste Voraussetzungen also um gemeinsam weitere Ideen für mehr Lebensmittelwertschätzung in Blieskastel umzusetzen. Wir freuen uns darauf, die Gruppe in Blieskastel dabei zu begleiten!

*Von Manon*
