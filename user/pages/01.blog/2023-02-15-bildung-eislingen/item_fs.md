
---
title: 'Bildungsarbeit in Eislingen'
date: '02/15/2023'
taxonomy:
    category:
        - blog
first_image: 2023-02-15-bildung-eislingen/bildung-eis.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Eislingen: Bildungsarbeit an einem Gymnasium mit leckeren Überraschungen

Von verschwendeten Lebensmitteln und geretteten Weihnachtsmännern: Mirjam Veit warb im Gemeinschaftskundekurs für einen wertschätzenden Umgang mit der Nahrung 
Vor Mirjam Veil auf dem Pult türmten sich süße Stückle und Obst – alles aussortierte Nahrungsmittel, die weggeworfen werden sollten. Die engagierte Referentin von „Freefood“ hat sie jedoch gerettet und mit in den Gemeinschaftskundekurs des Erich Kästner Gymnasiums gebracht. Sie wollte die Jugendlichen sensibilisieren für einen achtsameren Umgang mit Nahrungsmitteln. 
Bei ihrem Besuch versorgte sie die Schülerinnen und Schüler nicht nur mit interessanten Hintergrundinformationen. Sie brachte auch eine ganze Palette geretteter Schokoweihnachtsmänner mit, deren Jahreszeit vorbei war. Ihre Art der Lebensmittelrettung besteht darin, in Kooperation mit den Supermärkten die aussortierten Nahrungsmittel abzuholen. Containern ist so gar nicht nötig. 

Die Referentin stellte den Jugendlichen den Verteiler-Schrank vor, der seit Kurzem in Eislingen Süd am evangelischen Gemeindehaus in der Nähe des Friedhofs steht. Der Schrank wird von der Initiative „Freefood“ mit Essbarem gefüllt, das aus den Supermärkten aussortiert wurde, weil die Mindesthaltbarkeit abgelaufen ist. Das Mindesthaltbarkeitsdatum werde von den Herstellern selbst festgelegt und oft ganz knapp angesetzt, um den Warendurchlauf zu erhöhen. Damit werde bewusst eine Überproduktion in Kauf genommen, erläuterte Veit. Aus der Lebensmittelstation, die in Kooperation mit der Stadt Eislingen entstanden ist, kann jeder gerettete Nahrung mitnehmen, „aber bitte nur so viel, wie man wirklich selbst braucht und verwerten kann“, bat Veit. Dabei mache „Freefood“ den Tafelläden keine Konkurrenz, weil diese zum Teil nur Waren weitergeben dürften, die noch nicht abgelaufen sind oder deren Verpackungen nicht beschädigt sind. 
In ihrer Präsentation machte Mirjam Veit deutlich, dass es um sehr viel mehr geht als um den Verzehr von abgelaufenen Lebensmitteln. Ihr Anliegen ist eine größere Wertschätzung für Nahrung. Dass ein Drittel aller Lebensmittel weggeworfen wird, ist für sie schwer zu ertragen. Mit zahlreichen Daten und Fakten machte sie deutlich, wie viele Ressourcen an Boden, Dünger, Energie, Wasser und Arbeitskraft auf diese Weise verschwendet werden. „Pro Jahr werden in Deutschland ca. 12 Millionen Tonnen Lebensmittel weggeworfen, die noch für den Verzehr geeignet wären“, rechnete Veit vor. Und die Nahrung fehle dann an anderen Stellen der Erde, denn die Verschwendung treibe auch die Lebensmittelpreise nach oben. Ganz besonders durch den Anbau von Futtermitteln für Nutztiere, die nicht gegessen werden, werden Ackerfläche knapp für Nahrungspflanzen für Menschen. Von den 58 Millionen in Deutschland geschlachteten Nutztieren landen rechnerisch 4 Millionen ungenutzt im Müll. 

Manche Ursache für die Verschwendung liege auch in den Erwartungen der Verbraucher. „Es ist Wahnsinn, was wir an Backwaren retten“, weil die Bäckereinen das ganze Angebot bis zum Ladenschluss vorhalten müssten, berichtete Veit und versorgte die Schülerinnen und Schüler mit süßen Stückle vom Vortag. Die Schülerinnen und Schüler befanden sie nach den von Veit empfohlenen Regeln „sehen, riechen, fühlen , schmecken“ noch für gut und verspeisten sie mit Genuss. 
Aber wie kommt man zu einem sorgfältigeren Verbrauch von Lebensmitteln? „Regional und saisonal einkaufen, den Einkauf planen und phantasievoll kochen“ sind wichtige Leitlinien von Veit.
„Wir von Freefood haben unser Ziel erreicht, wenn wir überflüssig werden und keine Lebensmittel mehr übrigbleiben, die gerettet und verteilt werden müssen“, meinte Veit zum Abschluss – auch als Bitte und Auftrag an die nächste Generation. 


*Dorothea aus Eislingen*


