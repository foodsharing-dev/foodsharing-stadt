---
title: 'Was bisher geschah..'
date: '12:00 08/03/2019'
taxonomy:
    category:
        - blog
first_image: 2019_Sommer/2019_Sommer_3.JPG
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle: 'Entstehung der Kampagnenidee'
---

Die Idee wurde auf der 1. foodsharing PolKa-Tagung im Sommer 2018 geboren. PolKa – das steht für “Politische Kampangen” und ist eine überregionale foodsharing Arbeitsgruppe, in die sich jede\*r einbringen kann, der\*die Lust hat politisch bei foodsharing mitzumischen. Während dieses Treffens entstanden viele Ideen – einige davon wurden bereits umgesetzt, andere wieder verworfen. Darüber hinaus gab es Ideen, die es wert schienen, besonders intensiv betrachtet zu werden Die Idee, eine Art Auszeichnung an Orte zu vergeben, die sich in besonderer Weise gegen Lebensmittelverschwendung einsetzen, war eine dieser Ideen. 

![](2019_Sommer_1.JPG)
![](2019_Sommer_2.JPG)
![](2019_Sommer_3.JPG)

Anfangs starteten wir in einem recht großen Team direkt durch: Kurz nach der 1. Tagung stand das foodsharing Festival an, auf dem wir direkt mit einem Stand auf dem Markt der Möglichkeiten präsent waren und viele Menschen von unserer Idee erzählten.

![](2019_Sommer_4.jpg)
![](2019_Sommer_5.jpg)

Nach und nach wurde unser Team leider immer kleiner. Ein harter Kern aber blieb und trieb die Idee weiter voran. Im Rahmen eines  zweiten und dritten Treffens wurde die Umsetzung weiter verfolgt.

![](2019_Sommer_6.jpeg)


Damit aus einem Samen jedoch etwas Großartiges wachsen kann, braucht es nunmal mehr als nur einen “harten Kern”. Deshalb war Anfang des Sommers klar: Wir brauchen mehr Menschen im Team. 

Gesagt, getan – aus dem “Mutterschiff” der großen, bunten foodsharing-Community stießen nach und nach neue Menschen zu uns und erweiterten die Gruppe um viele tolle Persönlichkeiten. Jedes neue Mitglied brachte frische Motivation mit ein und bereicherte das Potpourri vorhandener Fähigkeiten um weitere Stärken. Seitdem arbeiten wir mit Volldampf an der Idee der foodsharing-Städte, damit diese bald blühen kann. Konkret bedeutet das: Viel schriftlicher Austausch und regelmäßige Video- und Telefonkonferenzen, um das  an vielen verschiedenen Orten in ganz Europa lebende Team zu koordinieren. Das nächste Treffen ist bereits in Planung, damit wir uns alle auch persönlich kennen lernen und das Go-Live der foodsharing-Städte-Website gebührend feiern können!

*Euer foodsharing-Städte Team*