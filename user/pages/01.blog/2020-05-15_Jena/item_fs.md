---
title: 'Jena'
date: '12:00 05/18/2020'
taxonomy:
    category:
        - blog
first_image: 2020-05-15_Jena/jena-2196873_1920.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---

Wir freuen uns darüber, Jena in die Liste unserer
foodsharing-Städte aufnehmen zu dürfen! 


Seit der Gründung
des foodsharing-Bezirks im März 2014 wurden bereits 320 Mitglieder
gewonnen und 14 Kooperationen erfolgreich begründet. Dabei sind es
in Jena vor allem Studenten, die sich als Ehrenamtliche für die
Lebensmittelwertschätzung einsetzen. Etwa die Hälfte der Mitglieder
engagiert sich mindestens einmal im Monat aktiv als Foodsaver.



In Jena werden sechs
Fairteiler aktiv durch foodsharing betrieben, beliefert und
gereinigt. Seit Neuestem unterstützt sogar ein mobiler Fairteiler
beim Verteilen der Lebensmittel: Der Fahrradanhänger F-RED zieht
regelmäßg seine Runden durch die Stadt, um Menschen mit Essen zu
beglücken. Beeindruckend!



Herzlich
willkommen, liebes Jena. Wir finden euer Engagement toll und freuen
uns, euch als unsere vierte foodsharing-Stadt dabei zu haben.

(Anm. Orgateam: Uuups, da ist uns ein Fehler passiert: Jena hat bereits
die unterschriebene Motivationserklärung vorgelegt und ist somit direkt
als foodsharing-Stadt zu unserer Bewegung beigetretet. Wir entschuldigen
uns für das Missgeschick und haben obenstehenden Blogeintrag entsprechend
korrigiert.)

*Von Vroni*