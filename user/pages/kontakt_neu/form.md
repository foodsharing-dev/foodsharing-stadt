---
title: Kontakt zum foodsharing Städte Team
form:
    name: contact-form
    fields:
        name:
          label: Name
          placeholder: Dein Name
          autofocus: on
          autocomplete: on
          type: text
          validate:
            required: true

        email:
          label: E-Mail
          placeholder: Deine E-Mail Adresse
          type: email
          validate:
            required: true

        message:
          label: Nachricht
          size: long
          placeholder: Deine Nachricht an uns!
          type: textarea
          validate:
            required: true

    buttons:
        submit:
          type: submit
          value: Abschicken!
        reset:
          type: reset
          value: Reset

    process:
        email:
          from: "{{ config.plugins.email.from }}"
          to:
            - "{{ config.plugins.email.to }}"
            - "{{ form.value.email }}"
          subject: "[Kontaktformular] {{ form.value.name|e }}"
          body: "{% include 'forms/data.html.twig' %}"
        message: Danke für deine Nachricht!
        display: thankyou

---

# Kontakt

Schreib uns eine Nachricht!

Du erreichst das foodsharing Städte Team auch unter: <kontakt@foodsharing-staedte.org>.