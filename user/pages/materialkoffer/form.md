---
title: Materialkoffer
# form:
#     name: Materialkoffer
#     fields:
#         bezirk:
#           label: Für welchen Bezirk möchtet Ihr einen Materialkoffer haben?
#           placeholder: Dein Bezirk
#           autofocus: off
#           autocomplete: on
#           type: text
#           validate:
#             required: true

#         name:
#           label: Wer ist die Ansprechperson (vollständiger Name)?
#           placeholder: Dein Name
#           autofocus: off
#           autocomplete: on
#           type: text
#           validate:
#             required: true

#         email:
#           label: Unter welcher E-Mailadresse erreichen wir dich?
#           placeholder: Deine E-Mail Adresse
#           type: email
#           validate:
#             required: true

#         phone:
#           label: Unter welcher Telefonnummer können wir dich erreichen?
#           placeholder: Deine Telefonnummer
#           autofocus: off
#           autocomplete: on
#           type: text
#           validate:
#             required: false

#         message:
#           label: Was sind Eure bisherigen Erfahrungen im Bereich der Bildungsarbeit im Bezirk und wo seht ihr Potenziale den Materialkoffer künftig einzusetzen?
#           size: long
#           placeholder: Deine Erfahrungen
#           type: textarea
#           validate:
#             required: true

#         dates:
#             type: checkboxes
#             label: An welchen der folgenden Wochenenden könntest du (oder eine andere Person aus eurem Bezirk) in Marburg den Koffer abholen und an einer kleinen Einführung teilnehmen (Bitte ALLE möglichen Wochenenden ankreuzen und nicht nur den Favoriten)?
#             default:
#             options:
#               option1: 13.-14. April
#               option2: 20.-21. April
#               option3: 27.-28- April
#               option4: keinen der angegebenen Termine
#             validate:
#               required: true

#     buttons:
#         submit:
#           type: submit
#           value: Abschicken!
#         reset:
#           type: reset
#           value: Reset

#     process:
#         pages.process:
#           type: checkboxes
#           label: PLUGIN_ADMIN.PROCESS
#           help: PLUGIN_ADMIN.PROCESS_HELP
#           default:
#               markdown: true
#               twig: true
#           options:
#               markdown: Markdown
#               twig: Twig
#           use: keys    
        
#         email:
#           from: "{{ form.value.email }}"
#           to:
#             - "{{ config.plugins.email.to }}"
#           subject: "[Materialkoffer] {{ form.value.bezirk|e }}"
#           body: "{% include 'forms/data.html.twig' %}"
#         display: thankyou

#         save:
#           fileprefix: contact-
#           dateformat: Ymd-His-u
#           extension: txt
#           body: "{% include 'forms/data.txt.twig' %}"

---

### Materialkoffer für die Bildungsarbeit

Liebe aktive Foodsaver\*innen,

einer der Wege Lebensmittelverschwendung zu reduzieren ist es, Menschen über
die Folgen aufzuklären, sie weiterzubilden und damit zu einem Umdenken zu
bewegen. Mit diesem Ziel wurde innerhalb der foodsharing Bewegung die
foodsharing Akademie ins Leben gerufen. Wir haben uns zur Aufgabe gemacht
für das Thema Lebensmittelverschwendung im globalen, sozial-ökologischen
sowie klimapolitischen Kontext zu sensibilisieren. Hierzu bieten wir einerseits
Seminare und Workshops an, stellen aber auch vielseitige Bildungsmaterialien
zur Verfügung. Vielleicht habt ihr ja schon einmal unser Online-Format „Am
Küchentisch“ besucht.

Aktuell wurde ein Materialkoffer konzipiert, der auf das von uns erstellte
[Methodenbuffet](https://www.foodsharing-akademie.org/materiallager/) aufbaut und die Bildungsarbeit in den foodsharing Bezirken
unterstützen soll. Doch was steckt dahinter?

Der Materialkoffer ist die perfekte Ausgangsbasis für eure Bildungsaktivitäten.
Mit ihm seid ihr z.B. für Workshops an Schulen, für Vorträge, aber auch für
Infostände bestens ausgerüstet. Dafür beinhaltet der Koffer folgende
Materialien:
- ausgedruckte Methodensammlungen (das Methodenbuffet der foodsharing
Akademie, aber auch weitere wie z.B. “Lebensmittel zum Zweck”)
- vorbereitete Bildungsmaterialien, um sämtliche Bildungseinheiten aus dem
Methodenbuffet durchzuführen (z.B. das Spiel „Die Tomatenrallye“, ein
Wimmelbild zur globalen Lebensmittelproduktion sowie verschiedene
Arbeitsblätter als Kopiervorlagen),
- spannende Bücher (u.a. „Benja & Wuse – Essensretter auf großer Mission“ und
„Die Essensvernichter – Warum die Hälfte aller Lebensmittel im Müll landet und
wer dafür verantwortlich ist“),
- ergänzende Materialien wie eine Weltkarte und Flyer zum Auslegen und
- auch einige Moderationsmaterialien (Marker, Moderationskarten, Magnete,
Klebepunkte usw.)

Du findest die Liste insgesamt sehr interessant, kannst dir aber wenig darunter
vorstellen? Hier ein Foto von unserem Prototypen:

![Aufgeklappter Koffer gefüllt mit Büchern und Material für Workshops. Ein paar Mappen im Vordergrund u.a. mit der Aufschrift "Bildungsmaterialen"](inhalt-koffer.png)

Das Projekt wurde über Fördermittel finanziert und wir haben nun die
Möglichkeit die ersten 10 Koffer kostenlos an interessierte Bezirke zu
übergeben, am liebsten breit verteilt im gesamten foodsharing-Raum! Damit ihr
einen guten Start mit dem Materialkoffer habt, bieten wir euch, bei der
Übergabe im April in Marburg, eine kleine Einführung in den Materialkoffer an.
Ihr habt Lust so einen Koffer in eurem Bezirk zu verwenden? Dann füllt **bis zum 05.03.2024** das untenstehende Formular aus. Anschließend sichten wir die
Bewerbungen und melden uns bis Mitte März mit den genauen Informationen
zum Übergabewochenende im April bei Euch.

Damit die Koffer möglichst vielseitig eingesetzt werden können arbeiten wir bei
der Verteilung mit den foodsharing Städten zusammen. Die Idee ist, den Koffern
Namen zu geben und deren Standorte sowie die Ausleihbedingungen auf der
foodsharing-Städte Webseite zu veröffentlichen. So können die Koffer von vielen
verschiedenen Menschen genutzt, aber auch von anderen Bezirken ausgeliehen
werden. Auch eine dauerhafte Weitergabe („Wanderkoffer“) ist möglich.

Wenn ihr Fragen habt, schreibt uns gerne eine Mail an <kontakt@foodsharing-akademie.org>. Wir freuen uns auf eure Bewerbungen.

Da wir die Formularfunktion hier auf der Webseite erstmalig nutzen und wir uns
nicht hundertprozentig sicher sind, ob alles klappt, schreibt uns unter
<kontakt@foodsharing-akademie.org>, wenn ihr bis zum 15.03.2024 nichts von uns
gehört habt. 

Danke :-)

**Formular geschlossen**

Kurzes Update: 
Wir sind überwältigt von den mehr als 40 Bewerbungen die bei uns eingegangen sind. Mit so vielen Anfragen haben wir nicht gerechnet. Wir werden uns nun besprechen und schauen, wie wir die Aufteilung vornehmen. Anschließend melden wir uns bei euch. Hier eine Übersichtskarte über alle Bezirke, die sich beworben haben:


![Deutschlandkarte mit 40 Markierungen verteilt übers Land](Übersichtskarte_bewerbungen.png)


