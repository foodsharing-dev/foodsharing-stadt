---
title: 'Material'
published: true
visible: true
hide_page_title: true
---

### Material zum Download

<img src="/user/images/icons_old/tisch.png" alt="Tisch" height=40px />  Geräteschuppen 
(Unser Handbuch zum Mitmachen)  
<a href="/user/pdf/20220316_foodsharingstadte_handbuch.pdf">Download</a>


<img src="/user/images/tree/tree_0_4.png" alt="Baum" height=40px />  Motivationserklärung (Vereinbarung zwischen foodsharing Gruppe und öffentlicher Hand)  
<a href="/user/pdf/foodsharing-Staedte_Motivationserklaerung.pdf">Download</a>

<img src="/user/images/icons_old/blatt2.png" alt="Blatt" height=40px />  Dokumentationsbogen (zur Dokumentation eurer erfüllten Früchte)  
<a href="/user/pdf/foodsharing-Staedte_Dokumentation.docx">Download</a>

<img src="/user/images/icons_old/aubergine_board.png" alt="Aub" height=40px />  Antragsentwurf für die Stadtratssitzung (aus der foodsharing-stadt St. Ingbert)

<a href="/user/pdf/Antragsentwurf_foodsharingstadt.pdf">Download</a>
