---
title: 'Unser Ziel'
published: true
visible: false
hide_page_title: true
---

### Unser Ziel

foodsharing-Städte verwenden Lebensmittel und minimieren deren Verschwendung. Die Lebensmittelwertschätzung ist in Gesellschaft und Politik verankert. Dieses Ziel wird durch die Zusammenarbeit von Zivilgesellschaft und Politik erreicht. 

Die Idee der Bewegung ist, lokale Veränderungen herbeizuführen, die zur Sensibilisierung und zu einem Umdenken in der Bevölkerung führen. Durch den dezentralen Ansatz hat die Kampagne großes Potential, Aufmerksamkeit von Bürger\*innen und politischen Akteur\*innen zu erlangen. Der Einsatz gegen Lebensmittelverschwendung wird dann nicht mehr nur das Anliegen von foodsharing sein, sondern vereint viele lokalpolitische Akteur\*innen hinter sich.

Unter dem Motto “Jede\*r kann etwas verändern – jeder kleine Schritt zählt” glauben wir daran, dass von der ersten Ebene – Menschen, die sich in ihrem Alltag und vor ihrer Haustür für mehr Lebensmittelwertschätzung engagieren – eine Veränderung auf zweiter Ebene – in der Landes- und Bundespolitik, dem Handel, der Produktion und der Landwirtschaft – ausgehen kann. 

<img src="/user/pages/02.bewegung/01.Ziel/Bewegung_Land_Stadt.jpg" alt="foodsharing Stadt Land" width=400 class="center" />

