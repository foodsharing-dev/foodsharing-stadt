---
title: 'Hinter den Kulissen'
published: true
visible: false
hide_page_title: true
---

### Hinter den Kulissen

Wer steckt hinter der Bewegung foodsharing-Städte, was bedeutet foodsharing überhaupt und worin liegt eigentlich das Problem? 

#### Unser Selbstverständnis
Die Bewegung foodsharing-Städte wurde von foodsaver\*innen gegründet. Wir sind aus foodsharing entwachsen und eng mit foodsharing verknüpft. Als eigene Bewegung sind wir in unserer Arbeitsweise und Organisation von foodsharing unabhängig. 

Durch unser Ziel, Ehrenamtliche und Stadtverwaltungen kommunal zu verbinden, die Bedeutung von foodsharing in der Lebensmittelrettung und unsere Verbundenheit mit foodsharing, sind die foodsharing-Ortsgruppen oft zentrale Akteur\*innen in den foodsharing Städten. Gleichzeitig beschränkt sich foodsharing-Städte nicht auf foodsharing-Ortsgruppen als Repräsentant\*innen von zivilgesellschaftlichem Engagement für Lebensmittelwertschätzung. 

Die Bewegung foodsharing-Städte will möglichst viele Akteur\*innen im Bereich Lebensmittelwertschätzung zusammenbringen, die gemeinsam die Ideen im Ideenkatalog umsetzen. Ein Kriterium um Teil der Bewegung foodsharing-Städte zu sein, ist, dass sich vor Ort eine Gruppe von Menschen dafür einsetzt, den Ideenkatalog zu verwirklichen. Wir freuen uns, dass Aktive der foodsharing-Ortsgruppen oft treibende Kraft in diesen Gruppen sind.  Gleichzeitig ist es möglich, foodsharing Stadt oder foodsharing Stadt auf dem Weg zu sein, ohne eine foodsharing-Ortsgruppe zu haben oder Mitglied einer foodsharing-Ortsgruppe zu sein.
