---
title: 'Unser Leitfaden'
hide_page_title: true
---

## Leitfaden für die Zusammenarbeit des Organisations-Teams foodsharing-Städte

*letzte Überarbeitung: Januar 2023*

## Ziele 

Durch Gründung der Bewegung foodsharing-Städte, möchten wir das Potential von foodsharing - Veränderung anzustoßen - vergrößern. 
Wir wollen zeigen, wie wichtig die Lebensmittelwertschätzung für das Klima und den Lebenswert in unseren Städten ist. 
Wir möchten aufklären, Menschen sensibilisieren und Druck auf Entscheidungsträger\*innen in Landwirtschaft, Industrie, Handel und Politik erzeugen. 
All das möchten wir mit der Bewegung foodsharing-Städte fördern. Dafür addressieren wir die lokalen foodsharing Ortsgruppen, um sie zu motivieren über das tägliche Lebensmittel retten hinaus aktiv zu werden und sich insbesondere mit der öffentlichen Hand und anderen lokalen Initiativen zu vernetzen und so die Vision der fs-Städte Bewegung zu verwirklichen/ entwickeln.

Unsere Vision: [https://foodsharing-staedte.org/de/bewegung/vision](https://foodsharing-staedte.org/de/bewegung/vision) 

Als Organisationsteam der Bewegung foodsharing-Städte begleiten wir diese und entwickeln sie stetig weiter.

### Selbstverständnis der Zusammenarbeit
Offenheit und Transparenz sind uns sowohl intern, als auch extern wichtig.
Darüber hinaus setzen wir in unserer Zusammenarbeit auf folgende, weitere Schwerpunkte: 
- neue Menschen an die Hand nehmen (persönlicher Kontakt, Arbeitsprozesse, Spirit/Stimmung weitergeben)
- möglichst keine/niedrige Hierarchien 
- Fehlerfreundlichkeit 
- wertschätzender Umgang
- alle Fragen dürfen und sollen gestellt werden 
- gegenseitige Unterstützung
      
### Entscheidungsfindung
Entscheidungen treffen wir gemeinsam. Eine Entscheidung ist getroffen, wenn es keine Einwände gegen einen Vorschlag gibt. Einwände werden angehört und integriert. Sollte keine Entscheidungsfindung möglich sein, kann durch jede an der Entscheidung beteiligte Person veranlasst werden,dass durch [Systemisches Konsensieren](https://www.sk-prinzip.eu/methode/) eine Entscheidung herbeigeführt wird.
- Wenn eine die Gesamtgruppe betreffende Entscheidung geplant ist, kündigen wir dies an, damit alle die Möglichkeit haben, ihre Stimme abzugeben. Dies kann in einer gemeinsamen online Konferenz oder asynchron über Trello passieren. 
- Es ist gewünscht, dass eine Entscheidungsvorlage ausgearbeitet wird, bevor eine Diskussion ins Team getragen wird.

Um unser Ziel zu erreichen und gut zusammenzuarbeiten, teilen wir unsere arbeiten auf und nutzen dafür Rollen und Projektteams.


## Kommunikation und Tools
      
Wir kommunizieren untereinander und mit anderen ehrlich, wertschätzend, rücksichtsvoll und offen.
      
### Intern
- Inhaltliche Kurzdiskussionen finden in der Regel bei Trello statt.
- Online Gespräche über BigBlueButton wahlweise als Co-Working oder Besprechung.
- Co-Working: Wir probieren in dieser AG das Format von Co-Working-Meetings aus. Das heißt wir verabreden online uns für einen Zeitraum von meist 1-1 1/2 Stunden, um zeitgleich an unseren Aufgaben zu arbeiten. 

**Co-Working**      
Zur verabredeten Zeit wählen wir uns ein, besprechen kurz wie es uns geht und was wir heute machen wollen. Anschließend gehen dann jeweils in die Umsetzung. Je nachdem, wieviel es zu Besprechen gibt, kann es auch Diskussionsrunden geben. Zum Abschluss kommen wir wieder zusammen und schließen gemeinsam.
Die Idee ist, dass wir uns durch dieses Format gegenseitig motivieren können und das gemeinsame Arbeiten (wenn auch nur digital) einfach mehr Spaß macht, als sich alleine dafür Zeit zu nehmen. Gleichzeitig können Fragen direkt gestellt und eine zweite Meinung eingeholt werden. 
Alle weiteren Infos findest du auf [dieser Trello Karte](https://trello.com/c/JK8TJbNB/85-co-working-sessions).
- einmal im Monat treffen wir uns zu einer allgemeinen Austausch Videokonferenz mit allen Teammitgliedern. Alle weiteren Informationen auf [dieser Trello Karte](https://trello.com/c/j11MlFEs/64-%F0%9F%93%9E-n%C3%A4chste-konferenz-1412-um-1930-immer-mittwochs-in-der-2-woche-des-monats-um-1930 )
- Alle 3 Monate gibt es einen Termin, bei dem wir über Inhaltliches reflektieren. Dafür nutzen wir den regulären Telko-Termin des Monats.
- Regelmäßige, persönliche Treffen: Wir streben an, uns persölich kennen zu lernen und regelmäßig (2x/Jahr) persönlich zu treffen.

### Extern

Für den Kontakt zu unseren Städten nutzen wir: kontakt@foodsharing-staedte.org. So könnt ihr euch einloggen: 

**Link**: [https://webmail.manitu.de/](https://webmail.manitu.de/)  
**Benutzername**: bitte an Technischer Unterstützer:in Rolle wenden  
**Passwort**: bitte an Technischer Unterstützer:in Rolle wenden  
**Domain**: foodsharing-staedte.org 

In unserer [Kontaktdaten-Tabelle](https://cloud.foodsharing.network/f/293612) gibt es ein 2. Tabellenblatt "Kampagnen E-Mail" um unsere E-Mail Adressen zu verwalten und auf einen Blick zu sehen, wer worauf Zugriff hat.

Hier einige Hinweise zur Nutzung des E-Mail Postfachs:
- Wir arbeiten mit mehreren Personen in einem Postfach 
- Bitte Postfach ordentlich halten! 
- Im Posteingang liegen nur unbearbeitete E-Mails. 
- Verschiebt eure E-Mails (mit Antwort aus dem gesendet Ordner!) bitte in die entsprechenden Ordner.
- Jede*r verschiebt nur seine/ihre E-Mails 
- Jede\*r Mitarbeiter\*in hat eine eigene Absender-Identität: Achtet darauf die richtige Absender-Identität beim Versenden auszuwählen (Dropdown-Menü).
- Wenn ihr mögt, könnt ihr die E-Mail-Adresse auch in euem E-Mail Programm wie thunderbird einfügen.
- Signaturen sind auch hinzugefügt 
- Bei allem gilt "work in progress" - Änderungen können wir immer vornehmen! 

Wir nutzen folgende Tools:
       
- Die AG “foodsharing-Städte Organisationsteam” auf foodsharing.de ist die Brücke zu foodsharing, damit jederzeit neue Menschen einsteigen können.      
- Trello: Unsere Aufgaben koordinieren wir in [Trello](https://trello.com/foodsharingstadte/home). Dort kannst du dir kostenlos ein Konto erstellen.      
- Bigbluebutton: Wir nutzen den AG Raum, der über die foodsharing.de Seite zugänglich ist. Unter “Deine Gruppe – foodsharing Städte Organisationsteam – Videokonfernz”, Vorraussetzung: Du musst Mitglied in der AG sein      
- Nextcloud: Unsere Dateien sammeln wir in der foodsharing [Nextcloud](https://cloud.foodsharing.network). 
  - Bitte achte darauf, hier keine Datein zu löschen und bei der Freigabe von Dokumenten die passende Einstellung zu verwenden (Bearbeitung erlauben, Passwortschutz etc.). 
  - Mitglied werden in der Cloud: AG Cloud anschreiben (cloud@foodsharing.network). Bitte um Account und hinzufügen zur Gruppe "AG foodsharing Städte". Den Ordner "AG foodsharing Städte" findet ihr auf eurem home Verzeichnis in der Cloud (ggf. einmal neu einloggen)
  - Ordnerstruktur: Es gibt einen Ordner für inhaltliche Arbeit an der Kampagne (Kampagneinhalte) und einen Ordner für Organisatorisches wie z.B. Protokolle, diesen Leitfaden etc. (Interne Orga). Darin befinden sich mehrere Unterordner. Bevor du einen neuen Unterordner anlegst, überprüfe bitte, ob es wirklich notwenig ist oder deine Datei einem anderen Ordner zugefügt werden kann. Bitte möglichst alle Datein einem Unterordner zuordnen und nicht einfach irgendwo speichern.
          
## Zuständigkeiten und Aufgaben

### Aufgaben und Aktivitäten im Gesamtteam

- Berichte der einzelnen Rollen und Projektteams anhören
- Gesamtrichtung der Bewegung foodsharing Städte im Blick haben
- entscheiden über den "Leitfaden der Zusammenarbeit" 
- entscheiden über unsere Mission und Vision
- für (neue) Aufgaben den richtigen Ort finden
- Planung der Finanzen
- Planung von persönlichen Treffen

### Rollen
Rollen kommen mit einer Beschreibung und Dauer.  
Gruppe entscheidet über die Vergabe von Rollen.  
Nicht jede Aufgabe braucht eine Rolle, nicht jedes Mitglied braucht eine Rolle.  
Personen können mehrere Rollen haben.  
Rollen-Inhaber\*innen können innerhalb ihrer Rolle selbstständig handeln und Entscheidungen treffen (im Rahmen der getroffenen Vereinbarungen)  
Wiederkehrende Aufgaben werden in einer Rolle zusammengefasst.   

#### Rollenbeschreibung
- Name der Rolle  
- Aufgaben und Aktivitäten  
- Unterstützt von (andere Rollen, Gruppenvereinbarungen)  
- Was braucht es (Fähigkeiten)  
- Review der Rolle  
- (optional: warum braucht es diese Rolle?)  


### Projektteam
Projektteams werden nach Bedarf gebildet und beschäftigen sich mit einem bestimmten Projekt für einen gewissen Zeitraum.  
Das Projektteam entscheidet in dem ihm anvertrautem Bereich.  
Die Gruppe entscheidet über die Bildung von Projektteams und deren Mitglieder.  

#### Projektteam Beschreibung
- Name des Projektteam  
- Aufgaben und Aktivitäten  
- Review des Team  

Hier findest du einen Überblick über unsere aktuellen Rollen und Projektteams: Trelloboard “Organisatorisches”, erste Spalte “Organisationsstruktur”


## Abläufe

### Gegenlesen von Textentwürfen 
- Wenn ein Textentwurf, ein Protokoll o.ä. von einer Person erstellt wird, verlinken wir es auf der entsprechenden Trello Karte. 
- Auf dieser Karte können die anderen dann einen Kommentar hinterlassen, sobald sie es gegengelesen haben. 
- Wenn ihre euch ein Feedback wünscht, verlinkt bitte @board oder die betreffenden Teammitglieder persönlich, sodass eure Bitte auch gesehen wird.

### Dokumentation 
Bei online Gesprächen führen wir ein Protokoll. Alle Dokumente die wir erstellen, sammeln wir in unserem gemeinsamen Nextcloud Ordner.

### Einführung neuer Menschen 
- Wenn jemand Teil unseres Teams werden möchte (z.B. eine Bewerbung an die AG auf foodsharing.de schreibt), schreiben wir der Person zunächst eine PN [Vorlage](https://cloud.foodsharing.network/s/76RqNWZY2YxiGGW).
- Wenn sie sich zurück meldet, fügen wir sie der AG hinzu, schicken ihr den Leitfaden und vereinbaren ein Telefonat, um sie kennen zu lernen, Fragen zu klären und eine passende Aufgabe für sie zu finden. 
- Im besten Fall wird ein Aufgabengebiet für den neuen Menschen gefunden und er*sie an die jeweilige Ansprechperson weitergeleitet. Es wäre super, wenn sich neue Menschen in die [Kontaktliste](https://cloud.foodsharing.network/f/293612) eintragen.
Abwesenheit kommunizieren:
- Wenn wir an einer Videokonferenz nicht teilnehmen können, kündigen wir das vorher an. 
- Auch, wenn wir für einen längeren Zeitraum nicht erreichbar sind (z.B. in den Urlaub fahren) kommentieren wir das auf [dieser Trello Karte](https://trello.com/c/2VQiyfMZ/50-abwesenheiten)  
      
### fs-Städte Material
- Material welches im Team der fs-Städte Bewegung entsteht kann auf Nachfrage beim Team angefordert werden
- Das Team entscheidet über die Nutzung des Materials durch Außenstehende

## Umgang im Konfliktfall 

### persönliche Konflikte 
- Ensteht ein Konflikt ist der erste Schritt, dass die betreffenden Personen das gemeinsame Gespräch suchen.
- Wenn kein Ergebnis bzw. Keine geminsame Basis gefunden werden kann, können die Betroffenen sich eine neutrale Person aus dem Team zur Mediation heranziehen 
      
### thematische Konflikte oder Konflikte mit Gruppen der Städtebewegung
- die Konfliktlösung kann durch hinzuziehen anderer Teammitglieder angegangen oder ggf. in der monatlichen Besprechung besprochen und gelöst werden.
      
### Austritt aus dem Organisationsteam    
- bei Austritt eines Mitglieds aus dem Orgateam wird die betreffende Person von den Boards gelöscht und von der Teamseite der Homepage entfernt
- Wir wünschen uns, dass ein Austritt mit ausreichend Vorlauf kommuniziert wird um die Aufgaben neu verteilen und bestenfalls eine gute Übergabe gewährleisten zu können
- hilfreich ist, wenn möglich, eine Kontaktmöglichkeit, um in der ersten Zeit Möglichkeit für eventuelle Rückfragen zu haben

## Änderung des Leitfadens

Der Leitfaden kann jederzeit durch alle ergänzt oder Änderungen vorgeschlagen werden. Damit der Prozess nicht unübersichtlich wird, werden Änderungsvorschläge in schriftlicher Form auf der Trello-Karte [“Teamstruktur und Leitfaden der Zusammenarbeit”](https://trello.com/c/3wAUd4yY/125-%F0%9F%92%9A-teamstruktur-und-leitfaden-der-zusammenarbeit) gepostet und müssen dann von allem mit einem “Daumen hoch” Symbol bestätigt werden. Passiert dies nicht, werden sie in der nächsten Telko oder beim nächsten Treffen besprochen. Die Einarbeitung der Ergänzung oder Änderung erfolgt dann durch die dafür zuständige Person (siehe Zuständigkeiten).  
Einmal im Jahr wird der Leitfaden auf seine Aktualität geprüft.