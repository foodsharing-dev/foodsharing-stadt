---
title: 'Unsere Vision'
published: true
visible: false
hide_page_title: true
---

### Unsere Vision

Stell dir vor, du lebst in einer Stadt, in der es **ausreichend gute Lebensmittel
für alle** gibt. Genug, damit jede\*r leckeres, selbstgewähltes Essen genießen kann,
unabhängig von finanziellen Mitteln, Alter, Geschlecht, sexueller Orientierung oder
Identität, Behinderung, Religion, Weltanschauung und ethnischer Herkunft. Alles,
was nicht sofort gegessen wird, machen die Bewohner\*innen gemeinschaftlich haltbar.

Es herrscht eine **Kultur des Tauschens und Teilens** – wer von etwas zu viel hat,
gibt es an Freund\*innen, Bekannte oder Nachbar\*innen weiter. Alternativ kann es
natürlich auch in einen der vielen Fair-Teiler gebracht werden, die an öffentlichen
Orten stehen. Die Lebensmittelversorgung ist großteils **lokal organisiert**: Viele
Menschen haben sich zu solidarischen Landwirtschaften zusammengeschlossen und
es gibt food-Kooperativen. Häufig finden Volksküchen statt, bei denen eine Gruppe
für viele Menschen kocht. Neben den Lebensmitteln wird auch Wissen gerne geteilt
und weitergegeben. **Menschen lernen gemeinsam und voneinander.**

Die Lebensmittel werden möglichst **regional und biologisch** angebaut und sind
der **Saison entsprechend** immer frisch. Alle Produkte werden fair gehandelt. Bei
der Versorgung steht das Wohl von Mensch, Tier und Umwelt im Mittelpunkt.

Damit es ein ganzheitliches Konzept für die Versorgungssicherheit in der Stadt
gibt, treffen sich Vertreter\*innen aus Politik, Landwirtschaft, Handel, Außer-Haus-
Verpflegung und Zivilgesellschaft regelmäßig zu einem runden Tisch. Dort wird **gemeinsam** überlegt und entschieden, wie mit dem Thema Ernährung möglichst nach-
haltig im Alltag Aller umgegangen werden kann und wie Menschen für mehr Lebensmittelwertschätzung sensibilisiert werden können. So wollen wir Politik neu denken:
**Partizipativ und gemeinwohlorientiert!** Auch zwischen Städten herrscht eine
gute Vernetzung.

Menschen organisieren sich in Gruppen, um gemeinsam zu einer Veränderung beizutragen. Sie bringen sich proaktiv mit Ideen und Taten ein und gestalten so ihre
gemeinsame Zukunft. Die öffentliche Hand unterstützt die Bürger*innen dabei und
fördert ihr Engagement. Es werden Ressourcen wie Räume und Materialen zur Verfügung gestellt und so ein fruchtbarer Boden für die Aktivitäten von Bürger\*innen
geschaffen.
