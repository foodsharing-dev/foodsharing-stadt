---
title: "Spenden"
description: "Spenden für das Projekt"
published: true
---
## Spenden

Du möchtest die Bewegung foodsharing-Städte finanziell unterstützen? Darüber freuen wir uns sehr.

Wir verstehen uns als Teil der foodsharing-Organisation. Wie auch foodsharing funktioniert unsere Bewegung vor allem dank ehrenamtlichem Engagement. Unsere laufenden Kosten – wie beispielsweise Kosten für das Webhosting – werden aktuell durch den foodsharing e. V. übernommen.

Wie du den foodsharing e. V. und somit die Bewegung foodsharing-Städte unterstützen kannst, erfährst du hier: [https://foodsharing.de/unterstuetzung](https://foodsharing.de/unterstuetzung)


