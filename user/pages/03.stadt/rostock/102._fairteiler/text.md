---
title: Fairteiler
menu: Fairteiler
published: true
image_align: left
---

## Fairteiler

Einen Fairteiler haben wir aktuell nur der in der <a href="https://foodsharing.de/?page=fairteiler&sub=ft&bid=219&id=1667">Stralsunder Straße</a>, nach dem Lockdown hoffentlich auch wieder im PWH!

Abholungen von den Tafeln werden derzeit „mobil“ geteilt. 
