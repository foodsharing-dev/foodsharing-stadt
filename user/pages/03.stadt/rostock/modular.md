---
title: Rostock
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 54.09268
        lng: 12.12867
    status: pending

content:
    items: @self.modular
---
