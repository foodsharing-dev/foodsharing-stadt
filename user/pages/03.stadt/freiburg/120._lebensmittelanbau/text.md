---
title: Lebensmittelanbau
menu: lebensmittelanbau
image_align: left
published: true
---

## Lebensmittelanbau

Es gibt in Freiburg mehrere SoLaWi-Gruppen, bei denen man teilnehmen kann und auch einige Projekte zum Urban Gardening:

GartenCoop Freiburg (SoLaWi): [https://www.gartencoop.org/tunsel/](https://www.gartencoop.org/tunsel/) 
Lebensgarten Dreisamtal (SoLaWi): [https://lebensgarten-dreisamtal.de/](https://lebensgarten-dreisamtal.de/)

Übersicht über Gartenprojekte: 
[https://www.urbanes-gaertnern-freiburg.de/de](https://www.urbanes-gaertnern-freiburg.de/de) 

