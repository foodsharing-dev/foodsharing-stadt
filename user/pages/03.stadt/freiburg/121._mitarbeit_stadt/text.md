---
title: Anstellung bei der Stadt
menu: mitarbeit_stadt
image_align: right
published: true
---

## Anstellung bei der Stadt

Wir stehen in sehr gutem Austausch mit Personen vom Amt für Schule und Bildung, sowie dem Umweltschutzamt.