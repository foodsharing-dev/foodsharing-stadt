---
title: Freiburg
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 47.9961
        lng: 7.8494
    status: pending

content:
    items: @self.modular
---
