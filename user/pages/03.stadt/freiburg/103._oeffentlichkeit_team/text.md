---
title: Öffentlichkeitsarbeit Team
menu: oeffentlichkeit_team
image_align: right
published: true
---

## Öffentlichkeitsarbeit Team

Die AG Öffentlichkeitsarbeit kümmert sich um die Bearbeitung von Pressanfragen. Sie steht zur Verfügung für Interviews oder vermittelt passende Personen aus der fs-Community. Zudem gehört die Organisation von Events zum Aufgabenfeld der AG. 

Hier ein paar Medienberichte zum Reinlesen und -hören:

- [Bericht des Stadtmagazins chilli](https://www.chilli-freiburg.de/stadtgeplauder/wirtschaft/zu-schade-fuer-die-tonne-wie-freiburger-lebensmittel-retten-und-teilen/)
- [Podcast von Scientists4Future](https://s4f-leipzig.de/Podcast-84/) 