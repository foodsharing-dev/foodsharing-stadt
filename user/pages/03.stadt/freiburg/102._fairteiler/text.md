---
title: Fairteiler
menu: fairteiler
image_align: left
published: true
---

## Fairteiler

Momentan gibt es in Freiburg gut 15 Fairteiler, die über das gesamte Stadtgebiet verteilt sind. Manche sind mobile Fairteiler, andere stationär. Einige werden nur von foodsharing, andere in Kooperation betrieben. Hier geht es zur Übersicht:

[Übersicht der Fairteiler auf foodsharing.de](https://foodsharing.de/?page=fairteiler&bid=64)
