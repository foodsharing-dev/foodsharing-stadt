---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Freiburg

- foodsharing Bezirk seit: 2013
- Anzahl der Kooperationen: Über 100
- Anzahl der aktiven Foodsaver\*innen: Über 1000
- Anzahl der aktiven Foodsaver\*innen über das Retten hinaus: Sehr, sehr viele! :)

Besonderes: foodsharing Freiburg ist in den letzten Jahren in Freiburg immer bekannter geworden und wurde zu einer wichtigen städtischen Umweltorganisation. Inzwischen sind auch im Freiburger Umland foodsharing Bezirke entstanden, mit denen wir in engem und sehr gutem Austausch stehen.

(Stand: April 2024)

