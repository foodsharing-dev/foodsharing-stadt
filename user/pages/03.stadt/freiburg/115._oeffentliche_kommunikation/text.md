---
title: Öffentlichkeitsarbeit der Stadt
menu: oeffentliche_kommunikation
image_align: left
published: true
---

## Öffentlichkeitsarbeit der Stadt

Seit 2022 taucht foodsharing im [Nachhaltigkeitsbericht der Stadt Freiburg](https://www.freiburg.de/pb/1073066.html) auf – als ein Indikator zur Verringerung der Lebensmittelverschwendung. Zudem wird im Nachhaltigkeitsbericht für Bürger*innen auf foodsharing und die Möglichkeit zum Engagement verwiesen.

Freiburg macht auch bei der Aktion „Gelbes Band – hier darf geerntet werden“ des Bundesministeriums für Ernährung und Landwirtschaft mit. Das Garten- und Tiefbauamt markiert dazu Bäume, an denen Obst jederzeit geerntet werden darf.

2022 realisierte das Studierendenwerk Freiburg gemeinsam mit der Stadt Freiburg unseren größten Fairteiler in der Studierendensiedlung am Seepark. Der Fairteiler war Teil der von 2022-2023 laufenden städtischen [17-Ziele-Rallye](https://17ziele.de/blog/detail/17ziele-rallye-freiburg.html). Der Fairteiler steht stellvertretend für das Ziel (12) der Vereinten Nationen: „Nachhaltige Konsum- und Produktionsmuster sicherstellen“.