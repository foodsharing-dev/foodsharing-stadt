---
title: Beratungsteam für Betriebe
menu: betriebe_team
image_align: left
published: true
---

## Beratungsteam für Betriebe

In Freiburg sind wir in sehr gutem Austausch mit dem Amt für Schule und Bildung (ASB). Das ASB ist zuständig für den Mensa-Betrieb der öffentlichen Schulen.
Foodsharing und die Stadt Freiburg haben gemeinsam ein Konzept zur Vermeidung von Lebensmittelverschwendung an Schulen entwickelt. Die Ergebnisse wurden auf dem Online-Fachtag des Landeszentrums für Ernährung vorgestellt.
Die Stadt hat einen eigenen Haftungsausschluss erstellt, der es Privatpersonen außerhalb von foodsharing ermöglicht, übriges Essen an Schulen mitzunehmen. Alle Unterlagen sind auf Anfrage beim ASB und bei foodsharing Freiburg erhältlich.