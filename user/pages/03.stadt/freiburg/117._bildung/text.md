---
title: Integrierte Bildungsarbeit
menu: bildung
image_align: right
published: true
---

## Integrierte Bildungsarbeit

Die AG Bildung zielt darauf ab, die Lebensmittelwertschätzung in verschiedenen Bildungsbereichen zu fördern und sicherzustellen, dass Schüler\*innen und die breite Öffentlichkeit ein besseres Verständnis für die Bedeutung der Reduzierung von Lebensmittelverschwendung entwickeln. Die Arbeitsgruppe führt selbst Workshops an allen Schulformen durch und unterstützt Akteuer\*innen der Ernährungsbildung bei der Implementierung des Themas Lebensmittelverschwendung in ihre Bildungsworkshops. Das Bildungskonzept von foodsharing zielt darauf ab, Schüler*innen und der Gemeinschaft ein tiefes Verständnis für Lebensmittelwertschätzung zu vermitteln. Dazu werden praxisnahe Bildungsangebote und Materialien entwickelt, die auf die Reduzierung von Lebensmittelverschwendung und nachhaltige Ernährung abzielen. Die Bedeutung von praktischem Handeln und bewussten Konsumentscheidungen im Einklang mit Umwelt- und sozialen Zielen werden betont.


Weiterhin pflegt Foodsharing Freiburg kontinuierlichen Dialog mit dem "Amt für Schule und Bildung" sowie dem "Amt für Abfallwirtschaft" und trägt aktiv zur Erstellung von Bildungsmaterialien bei, die auf die Verminderung von Lebensmittelverschwendung abzielen.


Foodsharing Freiburg steht außerdem Cateringdiensten der städtischen Schulen und Kindertagesstätten beratend zur Seite, um die Vermeidung von Lebensmittelabfällen zu unterstützen.


Auf der Freiburger Plattform [„WIZZN – Lernorte auf einen Klick“](https://wizzn.freiburg.de/suche-lernangebote/redaktionelle-suche?tx_bne_angebotsliste%5Bfilter%5D%5Bsuchwort%5D=foodsharing&tx_bne_angebotsliste%5Bfilter%5D%5Bumkreis%5D%5Bdist%5D=20) findet man weitere Infos über unsere schulischen Angebote. 


Außerschulische Angebote zur Ernährungsbildung werden z.B. vom Foodsharing-Café Freiburg durchgeführt: Hier gibt es regelmäßig öffentliche Veranstaltungen rund um die Themen Lebensmittel und Ernährung. Alle Termine findet man auf der Website: [https://foodsharing-cafe-freiburg.de/](https://foodsharing-cafe-freiburg.de/)
