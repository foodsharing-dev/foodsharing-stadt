---
title: Herausragende Betriebskooperationen
menu: betriebskooperationen
image_align: right
published: true
---

## Herausragende Betriebskooperationen

Seit der Verbreitung der App „To Good To Go“ hat sich die Menge der Lebensmittel, die foodsharing abholt signifikant reduziert, was wir sehr begrüßen. In Freiburg nehmen verschiedene Supermärkte, Bäckereiketten, zahlreiche Gastronomiebetriebe und weitere Betriebe teil.
Bei einigen Supermärkten beobachten wir auch vermehrt „Reduziert-Sticker“. Wir glauben, dass sich auch durch unser Engagement Betriebe verstärkt nach Lösungen umsehen und kreative Ideen ausprobieren. Viele Cafés und Marktstände geben auch zuerst ihren Mitarbeiter\*innen übrige Lebensmittel mit. Hier agiert foodsharing-Freiburg nur auf Abruf. Wir freuen uns sehr über diese Entwicklung.


