---
title: Verordungen der öffentlichen Hand
menu: verordnungen
image_align: right
published: true
---

## Verordungen der öffentlichen Hand

Die Stadt Freiburg hat 2017 die [städtischen Nachhaltigkeitsziele](https://www.freiburg.de/pb/206112.html) beschlossen. Dabei wurde das Ziel festgelegt, die Lebensmittelverschwendung auf Einzelhandels- und Verbraucherebene bis 2030 zu halbieren.