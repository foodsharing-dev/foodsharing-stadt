---
title: Info Stände
menu: info_staende
image_align: left
published: true
---

## Info Stände

Wir organisieren regelmäßig Infostände bei öffentlichen Veranstaltungen. 2023 waren wir zum Beispiel mit einem Infostand bei der **greenflair Messe** vertreten, wo wir mit Interessierten ins Gespräch kamen und über Lebensmittelverschwendung und foodsharing informierten. Besucher*innen konnten außerdem an einem Quiz und einem Workshop teilnehmen und auch gleich vor Ort ein paar der mitgebrachten Lebensmittel retten. 
Auf dem **Agrikulturfestival** haben wir in Zusammenarbeit mit Solare Zukunft e. V. eine Schnippeldisko mit Solarkochern veranstaltet. Auch beim **Sommerfest des Foodsharing-Cafés** im September wurde unser Stand sehr gut besucht. 
Im Februar 2024 waren wir mit einem Stand auf der **Veggienale** zu finden und haben auch schon ein paar Ideen für den Tag der Lebensmittelverschwendung. 