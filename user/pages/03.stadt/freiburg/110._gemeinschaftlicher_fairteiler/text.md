---
title: Gemeinschaftlicher Fairteiler
menu: gemeinschaftlicher_fairteiler
image_align: left
published: true
---

## Gemeinschaftlicher Fairteiler

In Freiburg gibt es inzwischen zahlreiche Fairteiler. Die Stadt hat foodsharing 2023 mit einem Zuschuss für den Bau und den Unterhalt von Fairteilern unterstützt.
