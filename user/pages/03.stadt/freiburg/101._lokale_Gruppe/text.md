---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
image_align: right
published: true
---

## Lokale foodsharing Gruppe

Die foodsharing-Gruppe Freiburg besteht seit 2013 und erfreut sich einem starken Zuwachs. Neben dem eigentlichen Retten von Lebensmitteln bei Kooperationspartnern engagieren sich auch zahlreiche Menschen in verschiedenen Arbeitsgruppen: 
Seit 2021 gibt es das [Foodsharing-Café](https://foodsharing-cafe-freiburg.de/), das zweimal pro Woche öffnet. Über das Café bieten wir Workshops für alle Zielgruppen an, gestalten Events und schaffen einen Ort, um Lebensmittel zu retten, zu verteilen und sich auszutauschen.
Die AG „foodsharing für alle“ baut Barrieren ab und macht foodsharing für wirklich alle zugänglich. Es gibt auch eine Ernte- und Einkochgruppe, die Ernteaktionen und Veranstaltungen zum Haltbarmachen von Lebensmitteln organisiert sowie ihr Wissen weitergibt. Seit Herbst 2023 haben wir eine Arbeitsgruppe zur Außer-Haus-Verpflegung, die Konzepte erarbeitet, wie Lebensmittel beispielsweise in Schulmensen oder bei Events effizient gerettet werden können. 


Kontakt: [freiburg@foodsharing.network](mailto:freiburg@foodsharing.network)

