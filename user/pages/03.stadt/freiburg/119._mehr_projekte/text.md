---
title: Weitere lokale Initiativen
menu: mehr_projekte
image_align: right
published: true
---

## Weitere lokale Initiativen

Die Freiburger Tafel nimmt ebenfalls gespendete Lebensmittel entgegen und verteilt diese an Menschen, die Unterstützung benötigen. Die Tafel hat dabei immer Vorrang vor foodsharing. Das soll eine Konkurrenz der beiden Organisationen ausschließen.
[https://freiburger-tafel.de/](https://freiburger-tafel.de/)

Außerdem kooperieren in Freiburg zahlreiche Betriebe mit der App toogoodtogo, worüber täglich ebenfalls viele Lebensmittel und Mahlzeiten gerettet werden.
[https://www.toogoodtogo.com/de](https://www.toogoodtogo.com/de)


