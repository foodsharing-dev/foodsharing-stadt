---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
image_align: right
published: true
---

## Öffentliche Lebensmittelrettung

Wir retten bei verschiedenen öffentlichen Festivals und Veranstaltungen Lebensmittel. Herausfordernd kann es sein, wenn spontan große Mengen an Lebensmitteln anfallen oder wenn große Mengen an zubereiteten Speisen schnell verteilt werden müssen. Für flexible Rettungen entwickelt die AG Außer-Haus-Verpflegung derzeit ein Pilot-Projekt.
