---
title: Nachhaltiger Umgang mit Geld
menu: geld
image_align: right
published: true
---

## Nachhaltiger Umgang mit Geld

Um den Umgang mit Geld transparenter zu machen und finanzielle Mittel besser verwalten und einsetzen zu können, sind wir momentan dabei, foodsharing Freiburg als Verein zu organisieren. Das Foodsharing-Café ist bereits als Verein organisiert und daher schon in der Lage, finanzielle Mittel sinnvoll einzusetzen. 