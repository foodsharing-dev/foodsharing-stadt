---
title: Gemeinschaftliche Events
menu: gemeinschaftliche_events
image_align: left
published: true
---

## Gemeinschaftliche Events

Die Stadt Freiburg fördert aktiv unsere Bildungsangebote. Für das Methodenseminar im September 2023 war sie eine wichtige Unterstützung.
Derzeit entwickelt das Amt für Schule und Bildung in Kooperation mit foodsharing Module für Grundschulen zum Thema Lebensmittelverschwendung. Für die Etablierung hat die Stadt die Finanzierung von über 20 Workshops für das Jahr 2024 zugesichert.