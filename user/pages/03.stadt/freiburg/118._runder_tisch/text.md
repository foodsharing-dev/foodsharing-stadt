---
title: Runder Tisch
menu: runder_tisch
image_align: left
published: true
---

## Runder Tisch

In Freiburg gibt es einen Ernährungsrat, der sich mit vielfältigen Fragen rund um nachhaltige Ernährung befasst. Unterschiedliche Themenkreise arbeiten zum Beispiel zu nachhaltiger Außer-Haus-Verpflegung, Ernährungsbildung oder dem Thema Essbare Stadt. Zudem werden Veranstaltungen und Kooperationen organisiert. Auch foodsharing ist seit 2023 im Sprecher*innenkreis des Ernährungsrats vertreten. 

Hier findet man mehr Infos: [https://ernaehrungsrat-freiburg.de/](https://ernaehrungsrat-freiburg.de/) 