---
title: foodsharing in Zeiten von Corona
menu: corona
image_align: right
published: true
---

## foodsharing in Zeiten von Corona

Die Kontaktbeschränkungen während der Corona-Pandemie hatten auch in Freiburg Einfluss auf das Retten von Lebensmitteln. Mit Vorgaben zum Maske-Tragen und der Reduzierung der Anzahl der rettenden Personen, bzw. der Aufteilung auf mehrere kleinere Teams konnten wir darauf gut reagieren.