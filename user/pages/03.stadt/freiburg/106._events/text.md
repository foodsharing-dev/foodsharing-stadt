---
title: Events
menu: events
image_align: left
published: true
---

## Events

Zwei Mal pro Woche findet das Foodsharing-Café in den Räumen des Strandcafés auf dem Grether-Gelände statt. Es wird von einer engagierten Gruppe Ehrenamtlicher organisiert. 
Jeder, der möchte, darf sich kostenlos an geretteten Lebensmitteln bedienen und durch ein solidarisches Preiskonzept können Getränke günstig an der Theke erworben werden. Das Foodsharing-Café bietet einen lebhaften Treffpunkt zum gemeinsamen Essen und Austauschen und sensibilisiert ganz nebenbei Menschen für das Thema Lebensmittelverschwendung.

Neben dem normalen Café-Betrieb werden über das Foodsharing-Café auch Bildungsangebote und andere Veranstaltungen organisiert.

Weitere Links
- [Website foodsharing Café](https://foodsharing-cafe-freiburg.de/)
- [Video über die Eröffnungsfeier (youtube)](https://www.youtube.com/watch?v=k-Ej-ci3Ocw )
- [Instagram](https://www.instagram.com/foodsharingcafefreiburg/?hl=en)