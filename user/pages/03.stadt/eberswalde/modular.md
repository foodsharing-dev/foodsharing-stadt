---
title: Eberswalde
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 52.834
        lng: 13.7984
    status: approved

content:
    items: @self.modular
---
