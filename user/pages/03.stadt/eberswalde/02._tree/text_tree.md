---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Eberswalde

Eberswalde liegt im Nordosten Brandenburgs und ist mittlerweile als Waldstadt bekannt. Früher bestimmte jedoch vor allem die Schwerindustrie das Stadtbild und die Stadtentwicklung. Die Hochschule für nachhaltige Entwicklung (HNEE) prägt seit Jahren im Sinne des Titels die Stadt, woraus sich auch die Aktivität für Foodsharing in Eberswalde begründet. Seit 2017 als Bezirk eingetragen, retten heute etwa 100 Foodsaver\*innen, vorwiegend Studierende bei über 1000 jährlichen Abholungen Lebensmittel. Ein Fairteiler an der HNEE sorgt für die bislang einzige direkte & regelmäßige Austauschmöglichkeit zwischen Foodsharing und der nicht daran beteiligten Stadtbevölkerung. Dieses wollen wir als Foodsharing-Stadt ändern.

(Stand: März 2023)
