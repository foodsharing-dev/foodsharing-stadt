---
title: Lebensmittelanbau
menu: lebensmittelanbau
image_align: left
published: true
---

## Lebensmittelanbau

Die GeLa Eberswalde, eine Initiative, die gemeinsames Landwirtschaften als Ziel hat, hat vor allem bei Studis zahlreiche Mitlandwirt\*Innen gefunden. Wöchentliche werden so Lebensmittel aus der Region in der Stadt an die Aktiven verteilt.
