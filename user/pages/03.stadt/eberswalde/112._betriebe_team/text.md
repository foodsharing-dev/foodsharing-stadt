---
title: Beratungsteam für Betriebe
menu: betriebe_team
image_align: left
published: true
---

## Beratungsteam für Betriebe

Unter pr.eberswalde@foodsharing.network können erfahrene Foodsaver\*innen kontaktiert werden, die gerne mit Ihnen die betriebsinternen Abläufe analysieren und Erfahrungen und Tipps zur Steigerung der Lebensmittelwertschätzung teilen. 
