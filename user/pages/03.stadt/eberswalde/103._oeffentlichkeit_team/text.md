---
title: Öffentlichkeitsarbeit Team
menu: oeffentlichkeit_team
image_align: right
published: true
---

## Öffentlichkeitsarbeit Team

Unsere Ziele, die wir gemeinsam mit der lokalen Bevölkerung und der Stadtverwaltung erreichen wollen:

-	Bewusstsein der Eberswalder\*innen für Lebensmittelwertschätzung schaffen
-	Gemeinsame Veranstaltungen mit lokalen Gruppen wie Greenpeace
-	Breiteres Bewusstsein für Möglichkeiten des Teilens von überschüssigen Lebensmitteln
  - Teilnahme an Foodsharing
  - Private Nutzung des Fairteilers
  - Privates Teilen von überschüssigen Lebensmitteln
-	Öffentliche Lebensmittelrettungen bei allen städtisch organisierten Festen
-	Öffentliche Bewerbung von Foodsharing durch die Stadt Eberswalde 

Weitere Ideen und Anregungen können gerne an unsere AG Öffentlichkeitsarbeit gesendet werden. Sie ist erreichbar unter pr.eberswalde@foodsharing.network!
