---
title: Info Stände
menu: info_staende
image_align: left
published: true
---

## Info Stände

Jährlich gestalten wir einen Stand bei der Erstie-Rallye der HNEE, wir versorgen die Teilnehmenden der Kleidertauschparties der lokalen Greenpeace-Gruppe mit Speisen aus geretteten Lebensmitteln und informieren Interessierte über unser Engagement auf dem Campusfest der HNEE und dem Tag der offenen Tür.

Die Stadt hat uns mittlerweile in ihre Organisation der Veranstaltungen eingebunden, sodass wir zu allen großen Veranstaltungen, bei denen Infostände möglich sind, einen solchen betreiben dürfen.