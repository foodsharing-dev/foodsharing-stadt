---
title: Ressourcen der öffentlichen Hand
menu: ressourcen
image_align: right
published: true
---

## Ressourcen der öffentlichen Hand
Bei öffentlichen Veranstaltungen der Stadt stellt sie sogenannte Marktstände für Info-Stände zur Verfügung, sodass wir überdacht und mit einer Auslagefläche gerettete Lebesmittel teilen und über unser Engagement berichten können.
