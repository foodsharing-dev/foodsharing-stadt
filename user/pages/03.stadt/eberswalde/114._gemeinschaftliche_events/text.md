---
title: Gemeinschaftliche Events
menu: gemeinschaftliche_events
image_align: left
published: true
---

## Gemeinschaftliche Events
Mehrmals im Jahr finden Kennenlerntreffen statt, bei denen Interessierte Menschen die Initiative kennen lernen können und Fragen zur Teilnahme stellen können. Ebendiese treffen werden durch die Stadt auf dessen Social-Media-Kanal beworben.
