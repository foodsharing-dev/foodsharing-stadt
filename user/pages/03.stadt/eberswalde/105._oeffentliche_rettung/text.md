---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
image_align: right
published: true
---

## Öffentliche Lebensmittelrettung

#### Rettungen auf dem Weihnachtsmarkt Eberswalde
Die Stadt hat uns mittlerweile in ihre Organisation der Veranstaltungen eingebunden, sodass wir zu allen Veranstaltungen, die von der Stadt organisiert werden, Abholungen durchführen. Eine davon ist die Lebensmittelrettung auf Weihnachtsmarkt:

Das Stadtmarketing war begeistert von unserer Kooperationsanfrage und einige Betriebe haben in den letzten Jahren mit uns kooperiert. Durch die großräumige Verteilung der Stände dauert die Abholung deutlich länger als bei Foodsharing üblich. Nicht wie üblich wird nach Betriebsschluss gerettet, sodass es zu Wartezeiten kommen kann und zu direktem Kontakt mit Kund\*innen. Bei diesem über Foodsharing zu informieren und gleichzeitig die Betriebe vor Ort vor „Imageverlust  durch unsere Kooperation“ zu schützen, stellt eine Herausforderung dar. Nicht alle sehen den Gedanken, dass wir mit den Kooperationen vor allem die Leistungen der Betriebe, also deren Lebensmittel, wertschätzen wollen! 
