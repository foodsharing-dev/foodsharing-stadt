---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
image_align: right
published: true
---

## Lokale foodsharing Gruppe

- Foodsaver: 207
- aktive Kooperationen: 17 
- Engagierte über Abholungen hinaus: 22
- Besonderes: Foodsharing wird vor allem von Studierenden in Eberswalde getragen. Durch den jährlichen, regen Zuzug entwickelt sich die Foodsharing-Community stets weiter.
