---
title: foodsharing in Zeiten von Corona
menu: corona
image_align: right
published: true
---

## foodsharing in Zeiten von Corona

Corona hat Foodsharing Eberswalde auf mehreren Ebenen getroffen. Die öffentlichen Plenen konnten nur digital stattfinden, jedoch wurden sie regelmäßig angeboten. Der Fairteiler, ein Tag vor öffentlichem Beginn der Pandemie eröffnet, ist für ein halbes Jahr nicht geöffnet gewesen. Nach erneuter Öffnung der Hochschule konnte der Fairteiler in Betrieb gehen und so eine große Kooperation mit einem Betrieb ermöglichen. Die Reduzierung von Slots war vereinzelt auch nötig, woraus eine Mehrbelastung der Foodsaver\*innen leider nicht vollständig ausgeschlossen werde konnte. Tragehilfen, welche in der Nähe der Betriebe warteten, konnten die Abholungen wiederum vereinfachen.
