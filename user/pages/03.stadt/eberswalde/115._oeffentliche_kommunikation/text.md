---
title: Öffentlichkeitsarbeit der Stadt
menu: oeffentliche_kommunikation
image_align: left
published: true
---

## Öffentlichkeitsarbeit der Stadt
Auf der <a href="https://www.eberswalde.de/foodsharing">Webseite der Stadt</a> wird über Lebensmittelwertschätzung und die Problematik der Verschwendung berichtet. Darüber hinaus wird die Initiative in Eberswalde vorgestellt.

Außerdem finden mehrmals im Jahr Kennenlerntreffen statt, bei denen Interessierte Menschen die Initiative kennen lernen können und Fragen zur Teilnahme stellen können. Ebendiese treffen werden durch die Stadt auf dessen Social-Media-Kanal beworben
