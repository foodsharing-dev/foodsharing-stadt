---
title: Weitere lokale Initiativen
menu: mehr_projekte
image_align: right
published: true
---

## Weitere lokale Initiativen
Die GeLa Eberswalde, eine Initiative, die gemeinsames Landwirtschaften als Ziel hat, hat vor allem bei Studis zahlreiche Mitlandwirt\*innen gefunden. Wöchentlichen werden so Lebensmittel aus der Region in der Stadt an die Aktiven verteile.

