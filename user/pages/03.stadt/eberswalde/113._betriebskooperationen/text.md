---
title: Herausragende Betriebskooperationen
menu: betriebskooperationen
image_align: right
published: true
---

## Herausragende Betriebskooperationen

Die Inhaberin des Café Liesbeth, eines der ersten Kooperationen, bat uns gelegentlich um Rezepte für ihren Betrieb. Dies ist zwar keine konkrete Maßnahme gegen Lebensmittelverschwendung, zeigte uns aber ihren Respekt gegenüber unserer Initiative. Ebenso verhält es sich mit den zwei Mensen der HNEE. Eine Arbeitsgemeinschaft, welche sich mit der Nachhaltigkeit des Betriebsablaufes der Mensa befasst, ermöglicht es Studierenden und Foodsaver\*innen, die Nachhaltigkeitsentwicklungen der Mensen aktiv mitzugestalten. Unsere Kooperation mit den Mensen ist hier nur ein Beispiel.

Die Kooperation mit der lokalen Tafel ist eine für Eberswalde in ihrer Form untypische Foodsharingkooperation. So hat sie ihren Betriebsablauf an uns angepasst und liefert uns gerettete Lebensmittel direkt zum Fairteiler. Hier finden öffentliche Fairteilungen statt.



