---
title: Fairteiler
menu: fairteiler
image_align: left
published: true
---

## Fairteiler

Der <a href="https://foodsharing.de/?page=fairteiler&sub=ft&bid=295&id=1665">Fairteiler</a> steht in dem Foyer eines Gebäudes der Hochschule. Der Kühlschrank wurde von der Hochschulgesellschaft und dem Asta gespendet. Zwei Regale wurden auf Eigeninitiative organisiert und machen den Fairteiler komplett, welcher durch öffentliche Fairteilungen für zahlreiche Nicht-Studierende sichtbar wird. Der Fairteiler ist für Studierende der HNEE, direkt an einer der Mensen gelegen und sehr attraktiv zu erreichen. Nicht-Studierende können den Fairteiler zu den üblichen Öffnungszeiten  erreichen, welcher auf Foodsharing.de registriert ist. Eine inoffizielle Telegramgruppe informiert über 900 Eberswalder\*innen über größere Lebensmittelmengen, die sich im Fairteiler befinden. Zentral in der Stadt gelegen bietet er einen unkomplizierten Tausch von Lebensmitteln.
