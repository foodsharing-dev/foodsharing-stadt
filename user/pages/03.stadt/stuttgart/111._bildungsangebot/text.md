---
title: Bildungsangebot
menu: bildungsangebot
image_align: right
---

## Bildungsangebot
 
Bildung- dazu gehören für uns regelmäßige Besuche an Schulen (z.B. Projekttage an der Waldorfschule Uhlandshöhe) oder Workshops für FSJ- und FÖJ-Gruppen (z.B. für den Internationalen Bund oder die AWO).

Wir wollen Kinder, Jugendliche aber auch Erwachsene im Rahmen von Bildungsveranstaltungen für Lebensmittelverschwendung sensibilisieren und ihnen diese Thematik inklusive all der globalen Zusammenhänge näher bringen. 
Aber nicht nur das. Wir wollen gemeinsam mit den jungen Menschen erarbeiten, wie wir alle etwas für weniger Lebensmittelverschwendung tun können. Lösungsansätze aufzeigen, Freude und Spaß am Wandel vermitteln, Mut machen.

Kontaktiert unsere Bildungs-AG gerne via [Email](mailto:bildung.stuttgart@foodsharing.network).

Auch in der Raupe Immersatt finden Bildungsangebote zum Thema Lebensmittelwertschätzung statt, die euch auch zeigen, wie ihr selbst aktiv werden könnt. Mehr Informationen zu den Workshops findet ihr auf der [Webseite der Raupe Immersatt](https://www.raupeimmersatt.de/konzept/bildung-workshops/). 