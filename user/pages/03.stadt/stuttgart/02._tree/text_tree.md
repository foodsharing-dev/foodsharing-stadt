---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Stuttgart
- foodsharing Bezirk seit November 2013
- Anzahl Kooperationen: 79
- Art der Kooperationen: Jugendhäuser, Unverpackt-Laden, Weltcafé, Restaurants, Bäckereien, Supermärkte...
- Besonderes: 2017 haben wir mit foodsharing Stuttgart den Bürgerpreis gewonnen, 2018 mit dem foodsharing-Café Raupe Immersatt. Die Raupe Immersatt ist ein aus foodsharing geborenes, eigenes Projekt: Ein Café, das gerettete Lebensmittel fairteilt und Getränke auf nach einem offenen, solidarischen Preisprinzip anbietet.


(Stand: Dezember 2019)
