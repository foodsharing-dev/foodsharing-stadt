---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
image_align: right
---

## Öffentlichkeitsarbeit Team
Wir wollen das Thema Lebensmittelverschwendung immer wieder in die Mitte der Gesellschaft tragen und foodsharing bekannter machen sowie neue Foodsaver\*innen werben. 

Die AG Öffentlichkeitsarbeit kümmert sich um die Teilnahme an Veranstaltungen, die Organisation von öffentliche Aktionen/Messeauftritten, Presseanfragen, die Vernetzung mit anderen Initiativen usw.  
<pr.stuttgart@foodsharing.network>