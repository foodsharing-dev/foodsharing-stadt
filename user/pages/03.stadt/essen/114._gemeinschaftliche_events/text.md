---
title: Gemeinschaftliche Events
menu: gemeinschaftliche_events
published: true
image_align: left
---

## Gemeinschaftliche Events

Beworben werden die gemeinschaftlichen Aktionen durch unsere Webseite foodsharing-essen.de, durch lokale Zeitungsartikel oder den sozialen Medien. Zu den Aktionen zählt z.B.: 

- 10.10.2019 Einführung des Falken Fairteilers Kray mit vorheriger Bewerbung über Radio Essen und im Anschluss WDR Ruhr Fernsehbeitrag
