---
title: Essen
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
   coordinates:
       lat: 51.4561
       lng: 7.0113
   status: pending

content:
    items: @self.modular
---
