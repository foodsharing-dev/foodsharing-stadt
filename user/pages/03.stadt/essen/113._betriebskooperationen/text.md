---
title: Herausragende Betriebskooperationen
menu: betriebskooperationen
published: true
image_align: right
---

## Herausragende Betriebskooperationen

Wir arbeiten mit einigen lokalen Betrieben zusammen. Als Beispiel sind hier Bio-Fleischerei Burchhard, Troll Ökologische Backwaren GmbH zu nennen. Einen besonderen Dank möchten wir an die Betriebe Pottsalat, Edeka Abaza und Backbrüder ausrichten. Die letzten beiden Betriebe unterstützen uns in der Öffentlichkeitsarbeit und helfen z.B. bei Dreharbeiten, um auf die Lebensmittelwertschätzung aufmerksam zu machen.
Zuletzt hat uns Pottsalat mit einer Spendenaktion unterstützt: [https://www.facebook.com/pottsalat/photos/a.898327270269836/3760612614041273/](https://www.facebook.com/pottsalat/photos/a.898327270269836/3760612614041273/)

Vielen Dank nochmal!
