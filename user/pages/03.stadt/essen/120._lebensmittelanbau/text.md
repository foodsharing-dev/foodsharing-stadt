---
title: Lebensmittelanbau
menu: lebensmittelanbau
published: false
image_align: left
---

## Lebensmittelanbau

Es gibt diverse Gemeinschaftsgärten und Urban Gardening-Projekte in Essen, welche privat oder durch städtische Unterstützung organisiert werden. Gemeinschaftliche Aktionen bzgl. Lebensmittelanbau sind unter : https://www.ernaehrungsrat-essen.de/kopie-von-zukunftsk%C3%BCche-essen zu finden.


