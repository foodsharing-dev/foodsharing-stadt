---
title: Bildungsangebot
menu: bildungsangebot
published: true
image_align: right
---

## Bildungsangebot

Wir halten Vorträge in Kirchengemeinden, Schulen oder anderen Bildungseinrichtungen. Zusätzlich werden für den DRK Nordrhein FreiWerk gGmbH Freiwilligendienste Inland auf kontinuierliche Anfragen 2-stündige Vorträge zum Thema foodsharing und Lebensmittelwertschätzung/-verarbeitung durchgeführt. 