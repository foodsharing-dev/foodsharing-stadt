---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
published: true
image_align: right
---

## Öffentliche Lebensmittelrettung

Einige Male haben wir bei dem auf dem Gelände der Zeche Zollverein stattfindenden Food Lovers Market am Ende der Veranstaltungen die nicht mehr verwendbaren Lebensmittel gerettet und weiter fairteilt. Ebenfalls sind wir beim Weihnachtsmarkt aktiv und haben eine flexible Gruppe für kurzfristige Abholungen. Für den 01.06.2020 war auf dem Pfingst Open Air eine gemeinsame Schnippeldisko geplant - wurde jedoch wegen der Covid-19 Pandemie abgesagt.
