---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: right
---

## Öffentlichkeitsarbeit Team

Für unsere Öffentlichkeitsarbeit haben wir eine eigene AG. Schreib uns gerne eine Mail an <oeffentlichkeitsarbeit.essen@foodsharing.network>.

Unsere letzten Videobeiträge sind:

- Foodsharing in Essen auf YouTube – [Don’t let good food go bad](https://www.youtube.com/watch?v=pEwfxdt6A4I) 
(vom 29.06.2018)

- WDR Lokalzeit Ruhr – Lebensmittelretter unterwegs gegen die Verschwendung (vom 10.10.2019 ab Minute 18)

Zu unseren Aktionen zählen u.a.:

- 10.10.2019 WDR Lokalzeit Beitrag "Lebensmittelretter unterwegs gegen die Verschwendung" mit anschließendem Redebeitrag
- 10.10.2019 festliche Fairteiler Eröffnung Fairteiler Falken Kray 
- 02.11.2019 Zeitungsbeitrag Stadtspiegel "Fair Teiler" in Kray 
- 14.11.2019 Vortrag foodsharing Essen in der Ev. Enmaus-Kirchengemeinde
- 11.02.2020 Vortrag foodsharing Essen in der Ev. Kirchengemeinde Essen-Kray
- 18.03.2020 WDR Lokalzeit Beitrag (Fokus: Corona bedingte überschüssige Lebensmittel von Gastronomien können an FS Essen weitergegeben werden) 
- 01.08.2020 Zeitungsbeitrag Stadtspiegel 6143/4 "Die Lebensmittelretter von Kray" 
- 07.11.2020 Zeitungsbeitrag Stadtspiegel 6143/8 "Fair-Teiler" in Kray feiert Geburtstag 
- 09.2021 Zeitungsartikel "Fair Teiler in Kray braucht Hilfe 
- 18.01.2022 foodsharing in Essen: Lebensmittel retten, Menschen helfen: [https://www.waz.de/staedte/essen/foodsharing-in-essen-lebensmittel-retten-menschen-helfen-id234346855.html](https://www.waz.de/staedte/essen/foodsharing-in-essen-lebensmittel-retten-menschen-helfen-id234346855.html)
