---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver: 771
- AbholerInnen mind. einmal im Monat: 230
- Engagierte über Abholungen hinaus: 40
- Botschafter\*innen: 4
- Fairteiler: 8
- Kontakt: <essen@foodsharing.network>
