---
title: Info Stände
menu: info_staende
published: true
image_align: left
---

## Info Stände

Gerne bieten wir Infostände an. Zu unseren bisherigen Aktionen zählen:

- 03.08.2017 IMPACT HUB RUHR
- 27.04.2018 Essens Ehrenämtern in der Rathausgalerie, Essen
- 05.05.2018 "das etwas andere Frühlingsfest" in Essen Kray
- 07.07.2018 Sommerfest des Mehrgenerationenhauses in Essen-Frohnhausen (Infostand)
- 12.02.2019 Stand bei „Essens Ehrenämter“ in der Rathaus-Galerie
- 29.06.2019 Stand Sommerfest Mehrgenerationenhaus Frohnhausen
- 27.06.2019 Infoveranstaltung bei Von Grünstadt, Essen
- 05.12.2019 Stand Ehrenamtstag in der Gertrud Kirche, Essen
- 01.04. 2020 „das etwas andere Frühlingsfest“ in Essen Kray, Essen
- 14.8.2021 "Gutes Klima-Festival 2021" Veranstaltungsorte Zeche Carl und Umgebung
- 26.03.2022 Städte gegen Food Waste Gemeinsam gegen Lebensmittelverschwendung in Essen, Schnibbelküche vor der Marktkirche, Essen 
- 14.5.2022 Infostand Nikolaus Schule, Essen

Geplant:

- 08.06.2022 Uni-Essen Sommerfest, Essen
- 18.06.2022 Steele bleibt bunt, Essen
- 20.08.2022 gutes Klima Festival im Stadtwandel, Schnibbelküche, Essen
