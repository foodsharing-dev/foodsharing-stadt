---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: right
---

## foodsharing in Zeiten von Corona

foodsharing in Essen konnte während Corona wachsen und sich weiterhin für Lebensmittelwertschätzung einsetzen. Die Foodsaver\*Innen im Bezirk retten trotz reduzierter Slots und höherer Hygienestandards weiter. Dank des ehrenwerten Engagements einiger foodsaver\*Innen können trotz der erschwerten Bedingungen weitere Einführungen gegeben werden, sodass die Community weiter wächst. Die Fairteiler Einführung wurde um eine digitale Möglichkeit erweitert. Auch neue Kooperationen wurden begonnen - teils sogar aufgrund der Pandemie. Bedauerlicherweise mussten auch einige Fairteiler vorübergehend geschlossen werden und sind noch nicht wieder geöffnet. Auf der anderen Seite boten der ein oder andere Fairteiler einen besonderen Austausch und es wurde versucht diese möglichst lange offen zu halten, z.B. intensiver Austausch mit dem VillaRü Fairteiler. Der Fairteiler Falken Kray konnte durch Hygienemaßnahmen aufrecht erhalten bleiben und stellte eine enorme Anlaufstelle während Corona-Pandemie dar. Es gab sogar zusätzliche Öffnungszeiten (Samstags), um der Pandemie entgegen zu wirken.

Besondere Aktionen während Corona sind hier zu finden:
- 18.03.2020 WDR Lokalzeit Beitrag (Fokus: Corona bedingte überschüssige Lebensmittel von Gastronomien können an FS Essen weitergegeben werden) 
- 17.10.2020 gemeinschaftliche Streichaktion des Falken Fairteiler Kray
