---
title: Weitere lokale Initiativen
menu: mehr_projekte
published: true
image_align: right
---

## Weitere lokale Initiativen

Unzählige Projekte setzen sich in unserer Stadt gegen Verschwendung ein. Zum Beispiel die Hummelbude und weitere verschiedene private und kirchliche Projekte. Aber auch diverse Bioläden, Unverpackt Laden (Grünstadt) und Miss Planty sind in Essen anzutreffen.
Des Weiteren arbeiten wir gerne mit anderen karikativen Initiativen wie den Fairsorger, Missionarinnen der Nächstenliebe (indische Schwestern), Raum 58 etc. zusammen und ergänzen uns gegenseitig bei Bedarf und Aktionen. Hierfür gibt es einen eigenen Austausch auf unserer Essener Plattform.