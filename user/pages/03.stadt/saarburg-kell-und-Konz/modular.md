---
title: Saarburg-Kell und Konz
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 49.6386
        lng: 6.5734
    status: pending

content:
    items: @self.modular
---
