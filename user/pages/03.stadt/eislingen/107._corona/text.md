---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: left
---

## foodsharing in Zeiten von Corona
Auf Grund von Corona wurde die Zahl der Abholungen reduziert und während der Abholungen und des Verteilens die Sicherheit durch das Tragen von Masken und Handschuhen erhöht. Geplante Infostände und Schnippelpartys mussten wir leider aufgrund des Infektionsgeschehens absagen.

