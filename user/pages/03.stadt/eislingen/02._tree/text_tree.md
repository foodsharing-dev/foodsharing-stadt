---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Eislingen
Aktive foodsharing Städte Gruppe seit Sommer 2021

Besonderes:
- Eislingen ist seit 2019 Fairtradestadt, Dieser Titel wurde 2021 für 2 Jahre verlängert.

Andere aktive Gruppen in Eislingen:
- To good to go
- foodsharing Bezirk Göppingen
- Freefood e.V. Projekt Göppingen
- Die Stadt hat die Motivationserklärung bereits unterschrieben, dazu gibt es einen schönen [Zeitungsartikel](https://www.swp.de/lokales/goeppingen/eislingen-ist-foodsharing-stadt-eislingen-setzt-zeichen-gegen-wegwerfmentalitaet-61766973.html).


(Stand: März 2022)
