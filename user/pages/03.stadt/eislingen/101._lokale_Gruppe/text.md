---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing-Städte Gruppe

Im Rahmen von foodsharing-Städte engagieren sich momentan ca.10 Personen für Eislingen.
Dabei besteht ein großes Interesse zur Zusammenarbeit bei der Stadtverwaltung, dem Stadtmarketing, der VHS, der Sozialdiakonischen Stelle und den 4 Kirchengemeinden.
  

