---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: right
---

## Öffentlichkeitsarbeit Team
Unser kurzfristiges Ziel ist die Installation einer Lebensmittelabgabestelle in Eislingen.
Die Planung hierzu läuft. Das das Projekt am Gemeindehaus der Lutherkirche aufgestellt werden soll, welches in den kommenden Monaten saniert wird, ist die zeitliche Perspektive von dem Verlauf der Sanierung abhängig. Die Kirchengemeinde wird ein Gartenhaus anschaffen. Eventuell kann dieses schon vorab an einem Zwischenstandort genutzt werden.

Unsere weiteren Ziele sind ein Vortrag zur Vernetzung mit pädagogischen Einrichtungen an der volkshochschule im Juni 2022, Infostände in der Stadt und ein Nachhaltigkeitstag (vermutlich 2023)
Eine Kooperation mit der Stadt für "Feste ohne Reste" wurde vom foodsharing Stadt Eislingen Team gestartet.

Wenn ihr mehr über unsere Arbeit erfahren wollt, kontaktiert unsere Ansprechperson Mirjam Veit unter der 
E-Mail Adresse: m.veit1@foodsharing.network
