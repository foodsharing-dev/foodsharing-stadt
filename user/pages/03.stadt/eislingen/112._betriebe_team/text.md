---
title: Beratungsteam für Betriebe
menu: betriebe_team
published: true
image_align: right
---

## Beratungsteam für Betriebe
Unsere Ansprechperson für das Team zur Beratung von Betrieben ist Verena Scholz. Ihr erreicht Verena unter der Emailadresse v.scholz@foodsharing.network
