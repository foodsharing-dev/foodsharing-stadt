---
title: Förderung von Alternativen
menu: alternativen
published: true
image_align: left
---

##Förderung von Alternativen
Es gibt einen Unverpackt Laden, der auch mit foodsharing Göppingen kooperiert. Mehr Informationen zum Unverpackt Laden Tante Nelli's findet ihr auf dieser [Webseite](https://tante-nellis.de/). Außerdem ist Eislingen seit dem 17.10.2019 Fairtrade-Town und setzt sich so für mehr Lebensmittelwertschätzung ein. Die Ansprechperson für die Fairtrade-Town Eislingen Thea Heinzler könnt ihr per Mail an <t.heinzler@eislingen.de> kontaktieren.
