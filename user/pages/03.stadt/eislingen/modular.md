---
title: Eislingen
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
       lat: 48.7077 
       lng: 9.6951
    status: approved

content:
    items: @self.modular
---
