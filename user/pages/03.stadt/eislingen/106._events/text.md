---
title: Events
menu: events
published: true
image_align: left
---

## Events
Im Sommer 2021 haben wir an ein paar Tagen an öffentlichen Plätzen in Eislingen gerettete Lebensmittel fairteilt. Die Fairteilertour fand am Schillerplatz, im Ösch, und in der Schlossstraße statt. Aufmerksam wurden die Menschen über soziale Medien und Mund-zu-Mundpropaganda. Die Aktion wurde sehr gut angenommen und wir konnten viele interessierte Bürger*innen über foodsharing informieren.

