---
title: Fairteiler
menu: Fairteiler
published: true
image_align: left
---

## Fairteiler

Aktuell gibt es in Weinheim einen Fairteilerschrank in der Nordstadt. Er ist seit Mai 2019 öffentlich zugänglich und wird in der Regel einmal täglich befüllt.

Fairteilerschränke für die Weststadt sowie den Ortsteil Lützelsachsen sind geplant und befinden sich derzeit im Bau. Die Öffnung der beiden Fairteiler ist für das Jahr 2021 angedacht.
