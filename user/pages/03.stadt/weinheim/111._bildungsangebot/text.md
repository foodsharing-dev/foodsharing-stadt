---
title: Bildungsangebot
menu: bildungsangebot
published: true
image_align: right
---

## Bildungsangebot

Der Stadtjugendring thematisiert foodsharing im Rahmen einer Kochgruppe, die mit geretteten Lebensmitteln arbeitet.

Die foodsharing Gruppe Weinheim, Birkenau, Viernheim und Umgebung veranstaltet auch Bildungsevents in den verschiedenen Bezirken, so zum Beispiel beim Klimathon in Viernheim.
