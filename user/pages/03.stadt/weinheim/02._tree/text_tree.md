---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Weinheim

- foodsharing Bezirk seit 2015
- Anzahl Kooperationen: 45
- Besonderes: Weinheim gehört zum foodsharing-Bezirk Weinheim, Birkenau, Viernheim und Umgebung.


(Stand: April 2021)
