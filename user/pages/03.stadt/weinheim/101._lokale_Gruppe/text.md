---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

Weinheim gehört zum foodsharing-Bezirk Weinheim, Birkenau, Viernheim und Umgebung.
Der Bezirk hat:

- 284 aktive Foodsaver\*innen
- 3 Botschafterinnen
- 45 laufende Kooperationen


