---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
image_align: right
published: true
---

## Öffentliche Lebensmittelrettung

Weihnachtsmarkt und Oktoberfest werden ab dem    nächsten Jahr berettet.
Ebenfalls werden bei den Pferderennen auf unserer Galopprennbahn die Stände ab 2023 berettet.
