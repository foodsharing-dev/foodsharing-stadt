---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Neuenhagen

- foodsharing Bezirk seit 2017
- Anzahl Kooperationen: 16 Betriebe
- Besonderes: Neuenhagen liegt am östlichen Berliner Stadtrand und gehört zum Land Brandenburg. Wegen ihres ausgeprägten Grüncharakters durch viele Gärten, zwei Pferdetrainierbahnen und über 8000 Straßen Bäumen sind wir auch als Gartenstadt Neuenhagen bekannt. Wir bilden mit unserer Nachbargemeinde den Foodsharing Bezirk „Neuenhagen Hoppegarten“   


(Stand: 17.12.22)
