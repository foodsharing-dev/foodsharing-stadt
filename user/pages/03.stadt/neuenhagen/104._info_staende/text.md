---
title: Info Stände
menu: info_staende
image_align: left
published: true
---

## Info Stände

Informiert haben wir bei:

- der Familienmesse (September 2021)
- Grünen Messe (Mai 2022)
- Oktoberfest (Oktober 2022)

Alle drei Veranstaltungen finden jährlich in unserer Gemeinde statt.

