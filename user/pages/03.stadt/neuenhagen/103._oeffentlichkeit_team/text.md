---
title: Öffentlichkeitsarbeit Team
menu: oeffentlichkeit_team
image_align: right
published: true
---

## Öffentlichkeitsarbeit Team

- Kontaktperson : Andrea Brackertz 
- Mail: Andrea.Brackertz@gmx.de

Eins unserer Ziele ist es, bei den Kindern in Kindergärten und Schulen über das Thema Lebensmittelverschwendung zu informieren und vor Ort mit praktichen Beispielen zu arbeiten.
Wir haben sehr guten Kontakt zur hiesigen Presse, es gab bereits mehrere Zeitungsartikel.
Vom RBB  Sender wurde auch hier vor Ort ein kurzer Film über unsere foodsharing Organisation gedreht. 
Im April 2023 findet in unserem „Haus der Senioren“ im Rahmen der „Senioren Uni“ eine Informationsveranstaltung zum Thema „foodsharing“ statt.