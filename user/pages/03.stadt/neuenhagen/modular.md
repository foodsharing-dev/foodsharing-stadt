---
title: Neuenhagen
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 52.5266
        lng: 13.6917
    status: approved

content:
    items: @self.modular
---
