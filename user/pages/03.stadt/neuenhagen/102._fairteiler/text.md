---
title: Fairteiler
menu: fairteiler
image_align: left
published: true
---

## Fairteiler

Wir haben im „Haus der Senioren“ eine Art Öffentlicher Faierteiler eingerichtet. Das Haus der Senioren ist eine Art Familienbildungsstätte und bis in die Abendstunden geöffnet. Dort werden die Lebensmittel den Hausbesuchern zur Mitnahme angeboten.
Ein öffentlicher Fairtailer ist geplant in unserem Jugendcub „Kontakt Sozialstation“. Dafür wird gerade ein Haus saniert und danach der Fairteiler in Betrieb genommen.
