---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
image_align: right
published: true
---

## Lokale foodsharing Gruppe

- Foodsaver: 207
- Engagierte über Abholungen hinaus: 30
- Wir arbeite hier mit dem NABU zusammen und auch mit dem Team „ Neuenhagen summt“ dadurch werden hier bienenfreundliche Pflanzen und auch Bäume angepflanzt.

