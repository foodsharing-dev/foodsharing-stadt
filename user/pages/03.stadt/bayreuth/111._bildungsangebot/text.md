---
title: Bildungsangebot
menu: bildungsangebot
image_align: left
---

## Bildungsangebot

Wir haben unsere Fühler nun erstmalig in Richtung Bildungsarbeit
ausgestreckt und durften Schülern einer siebten Klasse einen ganzen
Vormittag das Thema Lebensmittelverschwendung näher bringen. Das hat so
viel Spaß gemacht und wir haben tatsächlich das Gefühl gehabt bei den
Kindern (und Lehrern) etwas bewegt zu haben. Am 16.09. 2022 war es dann endlich soweit: Der erste Marktplatz der Nachhaltigkeit fand am Richard-Wagner-Gymnasium Bayreuth mit Beteiligung von foodsharing-Städte statt.

