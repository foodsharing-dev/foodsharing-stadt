---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        

         
    
---

Kurzbeschreibung von Bayreuth
- foodsharing Bezirk seit April 2014
- Anzahl Kooperationen: 18
- Wir kooperieren mit zum Beispiel mit 2 Edeka-Märkten, Denn's, Senor Taco, Bäckerei, Naupaka Poke, Rettberg, Transgourmet sowie weiteren Supermärkten und Gastronomiebetrieben


(Stand: September 2022)
