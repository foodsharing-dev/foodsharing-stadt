---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
image_align: left
---

## Öffentliche Lebensmittelrettung

Wir holen beim Foodtruckfestival im Herbst und dem Weihnachtsmarkt ab. Wir haben eine Gruppe für flexible (Einzel-)Abholungen und sind mit der Stadt Bayreuth im Austausch darüber, dass bei stattfindenden Festen Marktkaufleute direkt von der Stadt über das Angebot von Foodsharing informieren, Lebensmittelreste abzuholen.  

