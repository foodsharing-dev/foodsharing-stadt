---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: left
---

## Öffentlichkeitsarbeit Team
Nora Meides und Manuela Hertz sind in der Öffentlichkeitsarbeit aktiv. Hier werden Anfragen nach Vorträgen, Teilnahme von Foodsharing an Vernetzungstreffen, Klimademos etc koordiniert. Kontaktiert Nora gerne über ihr [foodsharing Profil](https://foodsharing.de/profile/111585). Manuela könnt ihr ebenso über ihr [foodsharing Profil](https://foodsharing.de/profile/179291) erreichen.  

Am 7.10. war foodsharing und foodsharing-Städte Bayreuth bei einer Podiumsdiskussion zum Thema "Lebensmittelverschwendung. Folgen für Klima, Umwelt und Welternährung" durch Manuela vertreten.
Diskutiert wurde mit Vertreter*innen von der Tafel, foodsharing Bayreuth, dem Umweltamt der Stadt Bayreuth und dem Kompetenzzentrum für Ernährung (siehe Foto).
