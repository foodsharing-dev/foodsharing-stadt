---
title: Info Stände
menu: info_staende
image_align: right
---

## Info Stände

Unser Infostand steht 2-3 mal pro Jahr auf passenden Veranstalutungen der
Uni. Zudem waren wir schon bei Fridays for Future mit Banner, Flyern und
Lebensmitteln dabei. Am 23.Januar 2022 hatten wir einen Infostand am Marktplatz anlässlich des Solidaritätstages mit dem Aufstand der Letzten Generation. Am 12. März hatten wir zudem eine öffentliche Verteilaktion von Lebensmitteln in der Stadt.
