---
title: Events
menu: events
image_align: right
---

## Events

Vor der Coronapandemie hatten wir ein monatliches Foodsaver\*innentreffen (der letzte Dienstag im Monat, 20
Uhr) sowie ein Neulingstreffen nach Bedarf (ca. zweimal im Jahr). 
Am ersten Sonntag im Monat gab es einen Brunch. Den Brunch gibt es leider nicht mehr, aber die Foodsaver\*innentreffen sollen demnächst wieder stattfinden.

