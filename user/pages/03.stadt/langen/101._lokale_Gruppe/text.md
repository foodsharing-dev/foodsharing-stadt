---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
image_align: right
published: true
---

## Lokale foodsharing Gruppe
Es gibt 13 aktive Kooperationen in Langen.
Außerdem gibt es 20 aktive Foodsaver\*innen in Langen und 15 Foodsaver\*innen in Langen engagieren sich über das Retten hinaus- zum Beispiel bei Veranstaltungen, AGs oder der Betreuung von Fairteilern. 
 