---
title: Förderung von Alternativen
menu: alternativen
image_align: left
published: true
---

## Förderung von Alternativen
Die Stadt Langen bewirtschaftet zudem die Streuobstwiesen und veranstaltet regelmäßige öffentliche Ernteaktionen. Der Erlös aus den gefertigten Produkten dient anschließend zur Finanzierung des Erhalts der Streuobstwiese.

[Link zum Flyer](http://www.neue-stadthalle-langen.de/download/44741/flyer_siebenschl%C3%A4ferprodukte.pdf)