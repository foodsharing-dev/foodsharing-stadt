---
title: Fairteiler
menu: fairteiler
image_align: left
published: true
---

## Fairteiler
Wir haben derzeit einen Fairteiler in Langen. Zudem hängt am Fairteiler ein QR-Code für die dazugehörige WhatsApp Gruppe. Außerdem haben wir eine foodsharing Gruppe auf facebook, welche es allen ermöglicht, ihre übrigen Lebensmittel zu fairteilen.

<a href="https://foodsharing.de/?page=fairteiler&bid=3869&sub=ft&id=2191"> Link zum Fairteiler an der Martin-Luther-Kirche in Langen</a>
<a href="https://www.facebook.com/groups/FoodsharingLangenEgelsbachDreieich"> Link zur facebook Gruppe von Langen Egelsbach und Dreieich</a>