---
title: foodsharing in Zeiten von Corona
menu: corona
image_align: right
published: true
---

## foodsharing in Zeiten von Corona
Während der Pandemie haben wir neben den allgemeinen Umsetzung der Regelungen wie Maskenpflicht vor allem die Anzahl der Slots pro Abholung reduziert, um die Kontakte zusätzlich zu reduzieren. Zudem konnten wir für unseren Bezirk eine Ausnahmeregelung für die Ausgangssperre beim zuständigen Kreis erreichen, was es uns ermöglichte, weiterhin zuverlässig zu retten.

Während die Tafel in Langen lange geschlossen war, konnte die Speisekammer schon recht schnell ihre Ausgabe in Form von gepackten Tüten wieder aufnehmen. Dies konnten wir immer wieder durch Großrettungen bei geschlossenen Betrieben durch Lebensmittel unterstützen und tun dies bis heute. Während der Schließung übernahmen wir zudem die Tage der Tafel / Speisekammer und organisierten Fairteilungen.

