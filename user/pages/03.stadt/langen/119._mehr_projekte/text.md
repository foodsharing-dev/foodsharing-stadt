---
title: Weitere lokale Initiativen
menu: mehr_projekte
image_align: right
published: true
---

## Weitere lokale Initiativen
In Langen ist die [Tafel Langen](https://www.langener-tafel.de/) verantwortlich. Diese holt regelmäßig noch genießbare Lebensmittel in den Betrieben ab und verteilt diese an Bedürftige aus Dreieich, Langen und Egelsbach. Hier besteht schon seit vielen Jahren eine freundschaftliche Kooperation zwischen der Tafel und foodsharing.

Langener Tafel e.V. – Lebensmittel retten. Menschen helfen. (langener-tafel.de)


