---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Langen
Langen mit seinen knapp 40.000 Einwohner\*innen  gehört zum foodsharing Bezirk Landkreis Offenbach West, welcher sich im September 2021 gegründet hat. 

Anzahl der Kooperationen: 
59 im gesamten Bezirk Landkreis Offenbach West
Anzahl der foodsaver\*innen: 88
Anzahl der aktiven foodsaver\*innen über das Retten hinaus: ca. 80%
Besonderes: Gemeinnütziger Verein foodsharing Landkreis Offenbach West e.V.



(Stand: )
