---
title: Superstadt
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 49.9922
        lng: 8.66597
    status: pending

content:
    items: @self.modular
---
