---
title: Öffentlichkeitsarbeit Team
menu: oeffentlichkeit_team
image_align: right
published: true
---

## Öffentlichkeitsarbeit Team
Unsere AG Öffentlichkeitsarbeit unterteilt sich nochmal in drei Untergruppen. Printmedien, Social Media und Homepage. Hierin bringen sich verschiedenen unserer Foodsaver entsprechend ihrer Stärken ein und erarbeiten die unterschiedlichen Themenbereich.

Kontakt: Oeffentlichkeitsarbeit.LK.Offenbach-West@foodsharing.network

