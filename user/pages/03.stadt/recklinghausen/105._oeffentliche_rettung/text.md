---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
published: true
image_align: right
---

## Öffentliche Lebensmittelrettung
Mit nahezu der gesamten foodsharing Gruppe aus Recklinghausen konnten wir eine große Rettungsaktion bei einem Foodlovers Streetmarket durchführen. Aufgrund der riesigen Menge an Lebensmitteln kamen wir an die Grenze unserer Möglichkeiten. Die Abholung dauerte inklusive der Weiterverteilung an z. B. Geflüchteten- und Obdachlosenunterkünfte drei bis vier Stunden und zog sich bis in den späten Samstagabend hinein.
