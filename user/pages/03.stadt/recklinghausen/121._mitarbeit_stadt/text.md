---
title: Anstellung bei der Stadt
menu: mitarbeit_stadt
published: false
image_align: right
---

## Anstellung bei der Stadt
Unsere Ansprechpersonen von der Verwaltung Recklinghausen sind u.a. Frau Wahrmann und Frau Germscheid. 
Sie sitzen in der Stabsstelle Klimaschutzmanagement und Kommunale Service Betriebe.

So könnt ihr sie kontaktieren: 
Frau Lara Wahrmann (Lara.Wahrmann@recklinghausen.de)
Frau Germscheid  (Lena.Germscheid@recklinghausen.de)

