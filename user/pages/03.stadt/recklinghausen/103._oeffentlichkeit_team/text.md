---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: right
---

## Öffentlichkeitsarbeit Team
Unsere Ansprechpersonen in allen Fragen rund um das Thema Öffentlichkeitsarbeit sind Marc Pracht und Karin Neuhaus. Ihr erreicht sie unter folgender Emailadresse: recklinghausen@foodsharing.network

Die Ziele unserer Gruppe sind der Aufbau nachhaltiger Kooperationsstrukturen zu Lebensmittelbetrieben, zur Stadtverwaltung, zu Bildungseinrichtungen und anderen Initiativen auf lokaler Ebene. Außerdem wollen wir die Einflussnahme auf lokalpolitische Entscheidungsträger bzw. auf die Entwicklung eines städtischen Konzeptes zur Lebensmittelwertschätzung erreichen (Stichwort Ernährungsrat). Besonders gefreut haben wir uns über die Auszeichnung des Agenda Preises 2016. Dieser wird regelmäßig von der hiesigen Nachhaltigkeits-Initiative „Lokale Agenda 21“ für besonders nachhaltiges Engagement in der Kommune verliehen.
