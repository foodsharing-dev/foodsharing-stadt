---
title: Info Stände
menu: info_staende
published: true
image_align: left
---

## Info Stände

Regelmäßig nehmen wir an Veranstaltungen in der Stadt teil, um das Thema Vermeidung von Lebensmittelmüll deutlich zu platzieren. Ruhrfestspiele, Nabu-Herbstfest, Komposttag der Abfallbetriebe sind einige Beispiele. Eine eigene Veranstaltung, mit Raphael Fellmer als Gastredner, haben wir direkt zu Beginn unserer foodsharing Aktivitäten durchgeführt. Unser Start in der Stadt Recklinghausen!