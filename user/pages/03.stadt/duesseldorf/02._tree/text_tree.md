---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Düsseldorf

- foodsharing Bezirk seit 2014
- Anzahl Kooperationen: 130
- Art der Kooperationen: Supermärkte, Einzelhändler, Bäckereien, Marktstand, Tafel, einmalige Rettungsaktionen, Messen, Großbetriebe, Bioläden, Feinkostläden



(Stand: Januar 2022)
