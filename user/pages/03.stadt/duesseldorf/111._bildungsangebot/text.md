---
title: Bildungsangebot
menu: bildungsangebot
published: true
image_align: right
---

## Bildungsangebot

Wir haben eine Bildungs-AG die an Düsseldorfer Schulen die Schüler\*innen über das Thema Lebensmittelverschwendung informieren. Zudem achten wir darauf, an Infoständen für jedes Alter verständliches Infomaterial auszulegen und dort zu erklären.
In der Vergangenheit wurden u.a. Menschen aus dem Bereich „Freiwilligendienst“ über eine Online-Infoveranstaltung zum Thema Lebensmittelwertschätzung/verschwendung beraten und informiert
