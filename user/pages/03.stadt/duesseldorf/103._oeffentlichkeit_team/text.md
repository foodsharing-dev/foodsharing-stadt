---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: right
---

## Öffentlichkeitsarbeit Team

In Düsseldorf läuft die Öffentlichkeitsarbeit über den Düsseldorfer Botschafter Justin Knigge.
Er ist über [duesseldorf@foodsharing.network](mailto:duesseldorf@foodsharing.network) für Presseanfragen erreichbar.

Es gab bereits mehrfache Zeitungsartikel in der rheinischen Post, sowie einen TV Auftritt bei WDR Aktuell. Durch den Instagram-Account [#foodsharingDuesseldorf](https://www.instagram.com/foodsharingduesseldorf/) und auf der Facebookseite [foodsharing Initiative Düsseldorf](https://www.facebook.com/lebensmittelretterD) wird regelmäßig über Veranstaltungen und unsere Rettungen informiert.

Ziele: Das Thema Lebensmittelverschwendung in Düsseldorf Mainstream machen und durch vermehrte öffentlichkeitswirksame Aktionen die Menschen auf foodsharing und das Thema Lebensmittelverschwendung aufmerksam zu machen.
Zudem möchten wir viele weitere Fairteiler im Stadtgebiet etablieren, um noch mehr Menschen die Möglichkeit zu geben Lebensmittel zu teilen.
