---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: right
---

## foodsharing in Zeiten von Corona

Einige unserer Fairteiler waren während der gesamten Lockdown-Phasen weiterhin offen, um vor allem bedürften Menschen die Möglichkeit zu geben kostenfrei an Lebensmittel zu gelangen.
Mit einem Hygienemaßnahmenkonzept haben wir die Düsseldorfer Community über die verschärften Abholregeln informiert.
Die Maßnahmen für unsere Fairteiler wurden verstärkt. Da die örtliche Tafel geschlossen war, haben wir einige Abholungen von dieser übernehmen können. Die monatlichen Gruppentreffen und Neuen-Vortreffen haben wir überwiegend online stattfinden lassen.
