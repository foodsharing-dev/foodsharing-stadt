---
title: Düsseldorf
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 51.234
        lng: 6.8095
    status: pending

content:
    items: @self.modular
---
