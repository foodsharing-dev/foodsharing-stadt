---
title: Weitere lokale Initiativen
menu: mehr_projekte
published: true
image_align: right
---

## Weitere lokale Initiativen

Zusammen mit dem Zentrum Plus „Lotsenpunkt“ kochen wir mehrmals im Monat mit geretteten Lebensmitteln und lassen diese bedürftigen Menschen zukommen.
In Zusammenarbeit mit den ehrenamtlichen der Caritas Düsseldorf Gerresheim unterstützen wir als foodsharing das Projekt „Kochen mit Geflüchteten“
Mit dem Cafe du Kräh, dem Niemandland und der Leben findet statt Halle kooperieren wir viele Jahre lang. So haben wir im Cafe du Kräh bereits eine Schnibbeldisco veranstaltet, im Niemandsland einen Fairteiler aufgebaut und in der Leben findet statt Halle Veranstaltungen organisiert.
Im Zentrum Plus Gerresheim (Diakonie Düsseldorf) haben wir seit 2016 unseren ersten Fairteiler mit Kühlschrank etablieren können. Zudem konnten wir die Räumlichkeiten für unsere Monatstreffen nutzen und haben in Kooperation mit dem Zentrum Plus und dem Netzwerk gegen Armut viele Veranstaltungen gegen Hunger umsetzen können. 
