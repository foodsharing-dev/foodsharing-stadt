---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

Den Bezirk Düsseldorf gibt es seit November 2013.
Aktuell 1577 angemeldete und davon 200 aktive foodsaver\*innen. Wir sind ein gemixter Haufen von jung bis alt – als Studierendenstadt natürlich mit vielen Studierenden.

Laufende Kooperationen: 130 – wir retten u.a. auch bei Tafelausgabestellen
Wir haben eine interne Messengergruppe um auch auf spontane Hilfegesuche reagieren zu können. Mit insgesamt 8 Botschafter\*innen die jeweils in 2-er Teams in den jeweiligen Bezirken zusammenarbeiten, sind wir gut aufgestellt in unserer Aufgabenverteilung.

Wir haben ein eigenes Melde- und Mediationsteam um Konflikte und Missverständnissen fair zu bearbeiten. In unserer Gruppe organisieren wir meistens einmal im Jahr ein Sommerfest und ein Weihnachtsfest, wo wir „Dankeschönkekse“ für unsere kooperierenden Betriebe backen.
Viele unserer Kooperationen sind schon jahrelange Kooperationen, sodass wir zu vielen Mitarbeiter\*innen ein gutes Vertrauensverhältnis aufbauen konnten.
Wir haben einen eigenen Bezirksflyer entworfen um Menschen vor Ort über unsere Arbeit zu informieren.
