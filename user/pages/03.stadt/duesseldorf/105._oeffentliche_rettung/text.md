---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
published: true
image_align: right
---

## Öffentliche Lebensmittelrettung

- Gourmetfestival – Rettung von verarbeiteten und nicht verarbeiteten Lebensmitteln bei 20 Verkaufsständen.

- Street Food Festival Düsseldorf – Mehrmals im Jahr retten wir bei über 20 Verkaufsständen.

- Veggie World/Heldenmarkt Düsseldorf – Am Ende jeder Messe retten wir die nicht verkauften Lebensmittel

- Eat & Style Düsseldorf – Wir retten die nicht gebrauchten Lebensmittel von den Kochshows. Zudem bei den Verkaufsständen die übrigen Lebensmitel.

- Asia Festival Düsseldorf – Rettung von asiatischen Lebensmitteln

- Rettungen von großen Mengen von Erbsenmilch, Hafermilch und Molkereiprodukten über 100Kg
