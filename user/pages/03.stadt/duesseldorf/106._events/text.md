---
title: Events
menu: events
published: true
image_align: left
---

## Events

Wir versuchen die Community in Düsseldorf so gut es geht zu beteiligen und veranstalten viele Veranstaltungen in Düsseldorf.
So gab es in der Vergangenheit Schnibbeldiscos, Monatstreffen, Sommerfest und ein Weihnachtsfest. Außerdem veranstalten wir Gemeinschaftsfahrten zum foodsharing Festival nach Berlin.
