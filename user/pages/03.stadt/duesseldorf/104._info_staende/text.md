---
title: Info Stände
menu: info_staende
published: true
image_align: left
---

## Info Stände

Durch einige Infostände auf Straßenfesten und Messen, informieren wir Menschen über das Thema Lebensmittelverschwendung und zeigen ihnen Möglichkeiten auf, wie sie über Fairteiler und Essenkörbe Lebensmittel weitergeben können.

- Eat & Style Düsseldorf
- Saatgutfestival
- Green World Tour Messe
- Veggie World
- Zakk Straßenfest
- Tag der deutschen Einheit
- unterschiedlichen Stadtteilfesten
- Tag der Nachhaltigkeit bei der HSD Düsseldorf

