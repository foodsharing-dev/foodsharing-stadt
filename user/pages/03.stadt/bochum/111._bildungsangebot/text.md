---
title: Bildungsangebot
menu: bildungsangebot
published: true
image_align: right
---

## Bildungsangebot

Es gab bereits mehrere Anlässe zu Bildungsarbeit vor allem im universitären Kontext und in der Öffentlichkeit, für die bereits vorhandene Ideen und Bausteine angepasst wurden.
Mehrere Male waren Foodsaver\*Innen in Präsenz oder digital an der Ruhr-Universität zu Gast, hielten Vorträge und diskutierten mit Studierenden beispielsweise bei einer Summer School 2019, im Sommersemester 2020 in einem Lehramtsseminar und mit einer Gruppe Geflüchteter. Außerdem nahmen wir an der Green Week der Ruhr-Universität teil, wo wir den Film Taste the Waste begleitet haben.
Eine für 2020 angesetzte Kooperation mit der [Schule der Künste](https://prokulturgut.net/kinder-und-jugend/schule-der-kuenste-jugendkunstschule-des-kulturgut-e-v/) musste leider abgesagt werden. Hier sollten unter anderem Ferienkurse mit Schüler\*Innen zum Thema Lebensmittelwertschätzung stattfinden inklusive Fairteilerbesuchen und gemeinsamen Kochaktionen.
