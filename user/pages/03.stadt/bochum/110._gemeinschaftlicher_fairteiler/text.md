---
title: Gemeinschaftlicher Fairteiler
menu: gemeinschaftlicher_fairteiler
published: true
image_align: left
---

## Gemeinschaftlicher Fairteiler

Die Bochumer Ehrenamtsagentur (bea) (https://www.ehrenamt-bochum.de/) ist ein Teil der so genannten Bochum Strategie, eine Initiative der Stadt Bochum, um die Stadt lebenswerter zu machen. So ist im August 2019 durch die Initiative der Bochumer Ehrenamtsagentur eine foodsharing-Ecke mit Infomaterialien über unsere Arbeit, sowie einem Kühlschrank zum Fairteilen von Lebensmitteln entstanden <a href="https://foodsharing.de/?page=fairteiler&bid=7&sub=ft&id=1409">Link</a>.
Darüber hinaus konnten wir die Räumlichkeiten der bea für unsere monatlich stattfindenden öffentlichen Plena und andere Austauschtreffen nutzen.
