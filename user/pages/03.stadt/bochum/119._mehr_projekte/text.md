---
title: Weitere lokale Initiativen
menu: mehr_projekte
published: true
image_align: right
---

## Weitere lokale Initiativen

An der Hochschule Bochum entsteht ein "Gärtnerei auf Dächern"-Projekt. Dieses startet im Frühling 2021. Es ist geplant, Teile der angebauten Lebensmittel auch über foodsharing zu verteilen.

Anfang 2021 erreichte uns die Anfrage eines Kleingartenvereins aus der Nähe unseres Fairteilers an der Hattingerstraße. Diese wollen in der Erntesaison mit uns kooperieren, indem sie den geerneten Lebensmittelüberschuss zentral sammeln und für die Weiterfairteilung zur Verfügung stellen.
Für das Jahr 2021 sind weitere solcher Kooperationen geplant.

Die Essbare Stadt Bochum befindet sich gerade in der Entwicklung und hat sich Ende 2020 als Arbeitsgruppe des Bochumer Ernährungsrats gegründet. Zukünftig wollen wir als Umweltinitiative Teil dieses neuen Vorhabens werden.
