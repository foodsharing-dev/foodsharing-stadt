---
title: Events
menu: events
image_align: right
published: true    

---

## Events

**Jährliches Fairteiler-Fest**


Jedes Jahr im Sommer wird gemeinsam mit dem Fairteiler-Team und der Emmaus-Kirchengemeinde ein Fest mit Mitbringbuffet aus geretteten Lebensmittelen am Fairteiler gefeiert. Alle Bürgerinnen und Bürger sind herzlich zu dem Fest am Fairteiler eingeladen! Es ist immer ein schöner Austausch. 