---
title: Lebensmittelanbau
menu: lebensmittelanbau
image_align: left
published: true

---


## Lebensmittelanbau

**Saatgut-Bibliothek**

Saatgutbibliotheken sind Orte, an denen man sortenreines und samenfestes Saatgut, wie zum Beispiel das von Tomaten “ausleihen” kann. Der Öffentlichkeit wird in einer Bibliothek kostenlos Saatgut zur Verfügung gestellt, welches einer Box o. ä. entnommen werden kann. Dieses Saatgut wird dann zuhause auf dem Balkon oder im Garten eingepflanzt. Nach der Ernte sollten einige Samen aufbewahrt und getrocknet werden, um diese dann (wie ein Buch) zurück in die Bibliothek zu bringen.

**Essbare Stadt Adendorf**

Ein kleines Team hat auf dem Rathausplatz in Zusammenarbeit mit dem Umweltbeauftragten der Gemeinde Adendorf vier Hochbeete aufgebaut und kümmert sich um Pflege und Beflanzung. Alle Bürgerinnen und Bürger dürfen sich beteiligen und ernten. Essbare Stadt nennen sich eine Reihe von Projekten für die Nutzung urbanen Raums zum Anbau von Lebensmitteln. Die Lebensmittel können dabei sowohl pflanzlichen als auch tierischen Ursprungs sein. 
Da die Anbauflächen anders als auf dem Land meist sehr begrenzt sind, umfassen die Aktivitäten im Rahmen der Essbaren Stadt vielfach auch vertikale Elemente wie die Nutzung von Balkonen, Wänden oder Dachflächen. Der Platzmangel macht es notwendig, die Flächen mehrfach genutzt zu bewirtschaften. So sind mit dem Nahrungsmittelanbau oft auch Aktivitäten der Freizeitgestaltung und der Landschaftsgestaltung verbunden. So werden Teile von Freizeitflächen wie Fußgänger\*innenzonen, Parks und Spielplätze mit essbaren Pflanzen bepflanzt. Die Trennung von Produktion und Distribution der Lebensmittel wird dabei aufgehoben, wenn diese öffentlich zugänglich sind und von allen Nutzer\*innen dieser Flächen beerntet werden dürfen. Bei einem Anbau durch die Stadtbewohner\*innen selbst entfällt die Trennung von Produzent\*in und Konsument\*in.