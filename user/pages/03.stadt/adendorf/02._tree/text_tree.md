---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Adendorf

- Gehört zum Bezirk Lüneburg
- Aktive Foodsaver*innen mit mindesten 1 Abholung pro Monat: 7
- Anzahl Kooperationen: 6 Betriebe
- Art der Kooperationen: Supermärkte, Tankstelle, Altenheim, Schulmensa
- 1 Fairteiler
- foodsharing-Stadt seit September 2023
- Besonderes: Adendorf ist eine Gemeinde mit nur ca. 10.000 Einwohnenden. Es gibt sowohl einen Fairteiler als auch ein E-Bike mit Anhänger, welche durch Spendengelder finanziert wurden. Das E-Bike wurde im örtlichen Fahrradgeschäft gekauft und mit einem großen Lastenanhänger, mit Platz für bis zu 12 Eurokisten, ausgestattet. Rad- und Anhänger können von Foodsaver:innen kostenfrei ausgeliehen werden. 
- Kontakt: lueneburg@foodsharing.network

(Stand: April 2024)
