---
title: Fairteiler
menu: fairteiler
image_align: left
published: true       

---

## Fairteiler

Mit einem kleinen Team aus der Gemeinde wurde im Sommer 2021 in Zusammenarbeit mit der Emmaus-Kirchengemeinde ein Fairteiler aufgebaut. Der Fairteiler steht am Kirchweg vor der Emmaus-Kirche und wird durch ein kleines Putzteam täglich gereinigt. Der Fairteiler, welcher mit Metallschwerlastregalen, Mülleimern, Reinigungsutensilien und einem Kühlschrank ausgestattet ist, wurde durch Spenden finanziert. Im Fairteiler gibt es mehrsprachige Informationen über foodsharing und darüber, was in einen Fairteiler darf und was nicht.
Das Fairteiler-Team sucht noch Mitstreiter\*innen. Kontakt: lueneburg@foodsharing.network


