---
title: Adendorf
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 53.28175
        lng: 10.43932
    status: approved

content:
    items: @self.modular
    
---