---
title: Superstadt
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
#foodsharing:
#    coordinates:
#        lat: 50.0502
#        lng: 8.6902
#    status: pending

content:
    items: @self.modular
---
