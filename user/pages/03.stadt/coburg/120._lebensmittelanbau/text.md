---
title: Lebensmittelanbau
menu: lebensmittelanbau
image_align: left
---

## Lebensmittelanbau

Es gibt mehrere Gemeinschaftsgärten, bei denen die Gruppe und auch Einzelne sich mit dem Anbau von verschiedenem Gemüse, Beeren, Obst usw. beschäftigen und auch so die Mühen des Anbaus von hochwertigen Lebensmitteln erfahren können.
Z.B. Gemeinschaftsgarten der Diakonie Coburg (Dialog) und der Gemeinschaftsgarten bei der Gärtnerei Callenberg.



