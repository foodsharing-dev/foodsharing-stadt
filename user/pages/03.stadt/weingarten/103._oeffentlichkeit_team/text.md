---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: right
---

## Öffentlichkeitsarbeit Team

Ansprechperson: Aline Steger

Durch das Vorhaben Foodsharing-Stadt zu werden, hat die Gruppe wieder neuen Schwung und Mitstreiter\*innen bekommen. Die Gruppe hat einen <a href="https://www.instagram.com/foodsharing.rv_wgt/">Instagram Account</a> gegründet und ist regelmäßig im Austausch mit der lokalen Zeitung „Weingarten im Blick“. Zukünftig werden Kooperationen mit Grundschulen und Kindergärten angestrebt.
