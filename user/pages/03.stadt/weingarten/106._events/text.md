---
title: Events
menu: events
published: true
image_align: left
---

## Events

Am 01.12.2021 haben wir auf dem Wochenmarkt in Weingarten mit einem Infostand über Lebensmittelwertschätzung informiert. Es gab ein Gewinnspiel, bei dem man gerettete Lebensmittel oder selbstgebastelte Adventskalender gewinnen konnte. Außerdem gab es viel Stoff zum Lesen, seien es Flyer oder Facts zur Lebensmittelverschwendung in Deutschland oder auf der ganzen Welt. Wer wollte konnte auch eine Runde mit unserem selbstgebastelten Memory spielen.
