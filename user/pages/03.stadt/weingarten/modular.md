---
title: Weingarten
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 47.8096
        lng: 9.6395
    status: approved
    

content:
    items: @self.modular
---
