---
title: Integrierte Bildungsarbeit
menu: bildung
published: true
image_align: right
---

## Integrierte Bildungsarbeit

Mit unserem Bildungskonzept gehen wir in die Weingartener Grundschulen, denn wir denken, dass es ganz wichtig ist, schon mit Kindern über das Problem der Lebensmittelverschwendung zu sprechen.  Karotte Lotti ist unser Maskottchen und erzählt den Kindern von ihrem Leben als Karotte. Dabei wird klar, dass viele Lebensmittel entlang der Lebensmittelkette weggeschmissen werden. Im Anschluss wird zuerst das Bewegungsspiel Obstsalat gespielt und dann danach thematisch passend, ein köstlicher Obstsalat aus geretteten Lebensmitteln geschnippelt.

