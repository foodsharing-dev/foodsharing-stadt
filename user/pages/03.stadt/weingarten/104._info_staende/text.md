---
title: Info Stände
menu: info_staende
published: true
image_align: left
---

## Info Stände

Dieses Jahr waren wir auf vielen Aktionstagen mit unserm Foodsharing Stand präsent.
Weitere sind in Planung.

- Familienfest Oberstadt Weingarten (Mai 2022)

Am sonnigen Familienfest haben wir an unserem Stand Obstsalat und Popcorn verteilt. Für die Kinder war unser Maskottchen die Karotte Lotti dabei. Gemeinsam im Gras sitzend hat uns Lotti ihre Lebensgeschichte erzählt. Danach durften sich die Kleinen etwas aus unserer Tonne aussuchen. Für die Erwachsenen lag ein spannendes Quiz über Foodsharing bereit. 

- Kleidertausch im Haus der Familie (Juni 2022)

Die Kleidertauschenden hungrig wieder gehen lassen? Nicht mit uns. An unserem Infostand gab es neben den üblichen Infomaterialien auch leckere Smoothies aus frisch gerettetem Gemüse. 

- Infostand beim Theaterfestival Isny (August 2022) (Kooperation mit Foodsharing Kempten)

Auch hier waren wir wieder mit einem bunten Stand dabei und freuen uns, 
dass die Kooperation mit foodsharing Kempten funktioniert hat. 

- Umsonst & Draußen (September 2022)

Das U&D, wie es im Volksmund genannt wird, ist ein lebendiges Spätsommerfestival in Weingarten. Zwei Tage haben wir unseren Stand betreut. Wieder mal war „Lotti Karotti“ dabei. Es war ein schöner Austausch auch mit den anderen Ständen. 

- Nachhaltigkeitstag (September 2022)

Beim Nachhaltigkeitstag in Ravensburg haben wir auch nicht gefehlt. Hier traten wir mit den Bürger\*innen aus Ravensburg in Kontakt. Wir wünschen uns, dass die Stadt Ravensburg auch bald Foodsharing Stadt wird. 