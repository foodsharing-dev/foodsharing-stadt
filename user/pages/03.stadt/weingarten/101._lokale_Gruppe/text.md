---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: ture
image_align: right
---

## Lokale foodsharing Gruppe

Die Foodsharing Gruppe „foodsharing Ravensburg/Weingarten e.V.“ gibt es seit März 2018.
Insgesamt gibt es 222 Foodsaver\*innen im Bereich Ravensburg und Umgebung. 
Der Verein hat aktiv laufende Kooperationen mit 23 Betrieben.
Über das Retten hinaus engagieren sich 5 Personen als Botschafter\*innen. 
Insgesamt wurden 38987 kg Lebensmittel gerettet bei 2094 Rettungseinsätzen. 
