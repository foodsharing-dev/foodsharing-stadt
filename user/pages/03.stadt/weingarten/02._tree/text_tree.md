---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Weingarten

- foodsharing Bezirk seit März 2018
- Anzahl Kooperationen: 23
- Besonderes: Weingarten gehört zur foodsharing Gruppe Ravensburg und Umgebung. Seit dem 26.11.2021 ist Foodsharing Weingarten/Ravensburg nun auch auf Instagram vertreten. Ihr findet uns unter: [https://www.instagram.com/foodsharing.rv_wgt/](https://www.instagram.com/foodsharing.rv_wgt/) . 


(Stand: 03.11.2021)
