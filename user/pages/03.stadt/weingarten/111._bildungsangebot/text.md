---
title: Bildungsangebot
menu: bildungsangebot
published: true
image_align: right
---

## Bildungsangebot

Wir haben einen Adventskalender entworfen und auf dem Wochenmarkt verschenkt. In einem Einmachglas befinden sich 24 zusammengerollte Zettelchen, die wenn man sie zusammen auf einen Karton klebt ein hübsches Mandala ergeben, mit Tipps zum Lebensmittelretten ringsherum. Die Tipps werden außerdem jeden Tag auf Instagram gepostet und am Fairteiler befindet sich auch ein Exemplar, an dem jeden Tag etwas frei gerubbelt werden kann. 
