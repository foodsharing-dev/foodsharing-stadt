---
title: Runder Tisch
menu: runder_tisch
image_align: left
---

## Runder Tisch

Ein runder Tisch wird in Form des wöchentlichen Stammtisches organisiert. Informationsabende für foodsharing Interessierte gibt es in Kooperation mit dem Kollektivcafe „Gmota“ oder auch in der Universität Graz.
