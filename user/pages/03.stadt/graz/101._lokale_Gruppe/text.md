---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
background: 
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver\*innen: 675
- Abholer\*innen mind. einmal im Monat: ca. 200
- Engagierte über Abholungen hinaus: 40
- Kontakt: <graz@foodsharing.network>