---
title: Info Stände
menu: info_staende
image_align: right
---

## Info Stände

Die letzten Veranstaltungen waren bei Oikis-Kleidertausch, sowie beim Kleidertausch von Nachhaltig in Graz.
Auch beim Umweltzirkus ist foodsharing mit einem Infostand vertreten. Sowie  beim Vereinsfest des Hip-Hop Vereins Four Elements und dem Tag der offenen Tür des Gemeinschaftsraums Schubertnest in der Uni Graz.