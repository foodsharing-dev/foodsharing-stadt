---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
published: true
image_align: right
---

## Öffentliche Lebensmittelrettung

Bei diesen öffentlichen Veranstaltungen haben wir Lebensmittel gerettet:
- Parkfoodfestival, September 2018: wir konnten allein 40 kg Fleisch retten
(Burgerpatties)
- Oktoberfest-Parkfoodfestival, September 2019
-esskultour (Restaurants aus Remscheid zeigen ihr Können), Juli 2019:
(36 Grad in der city :-) ). viel mehr Ware als erwartet, viel mehr Stände, die kurzfristig doch noch mit uns kooperieren wollten. Es war toll!
