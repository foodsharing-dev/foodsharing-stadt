---
title: Blieskastel
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 49.2364
        lng: 7.2613
    status: approved

content:
    items: @self.modular
---
