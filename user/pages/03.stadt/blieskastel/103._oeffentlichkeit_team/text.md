---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: right
---

## Öffentlichkeitsarbeit Team

Als Botschafterin hat Nina u.a. die Öffentlichkeitsarbeit als Aufgabengebiet übernommen. Sie pflegt sowohl facebook, als auch instagram. Darüber konnten schon einige foodsaver\*Innen, foodsharer\*Innen und Betriebe gefunden werden. Auch findet hierüber Vernetzung mit anderen Bezirken statt.
