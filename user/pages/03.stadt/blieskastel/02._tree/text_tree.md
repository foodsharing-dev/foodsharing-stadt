---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Blieskastel

- foodsharing Bezirk seit Oktober 2020
- Anzahl Kooperationen: 15
- Besonderes: Blieskastel ist Teil der Biosphäre bliesgau und eng mit dem Bezirk St. Ingbert verknüpft.


(Stand: 01.09.2021)
