---
title: Förderung von Alternativen
menu: alternativen
published: true
image_align: left
---

##Förderung von Alternativen

Da Blieskastel Teil der Biosphäre Bliesgau ist, werden regelmäßig spezielle Märkte veranstaltet, bei denen sich sowohl Bioläden als auch fairtrade und Weltladen beteiligen können. Der Verein "Bliesgau Genuss" vernetzt und unterstützt lokale, nachhaltig-produzierende Anbieter.