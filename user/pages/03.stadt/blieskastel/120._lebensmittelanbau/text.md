---
title: Lebensmittelanbau
menu: lebensmittelanbau
published: true
image_align: left
---

## Lebensmittelanbau

Es gibt in der Altstadt den sogenannten "Bürgergarten", der mit heimischen Gemüse-, Obst- und Kräutersorten bestückt und für jede\*n zugänglich ist. 
Die Aussaat und Pflege erfolgt durch Mitarbeiter\*Innen der Stadt in Zusammenarbeit mit einem Obst- und Gartenbauverein sowie Schulen und Kitas.



