---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: right
---

## foodsharing in Zeiten von Corona

Da die Tafel geschlossen wurde, haben wir einige Abholungen als Tafelersatzabholungen übernommen und durch eine öffentliche Fairteilergruppe weitergegeben. 
Bei der Tafel informierten Aushänge über diese Whatsapp-Gruppe.

Die monatlichen Treffen unserer foodsharing Gruppe  finden seit Beginn der Pandemie virtuell statt.

Zunächst hatten wir die Probeabholungen auf Eis gelegt. Dabei wird die Einführung am Fairteiler virtuell (mithilfe eines selbstgedrehtem Videos) und die anderen Abholungen Covidkonform durchgeführt. Die Slots bei den einzelnen Abholungen wurden auf höchstens zwei angepasst und die Betriebsveratnwortlichen weisen auf aktuelle Änderungen der Bestimmungen auf der Pinnwand hin.  
