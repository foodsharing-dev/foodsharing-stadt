---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver: 119
- AbholerInnen mind. einmal im Monat: 29
 
Unsere Gruppe besteht zur Zeit aus 29 Stammbezirklern – 119 foodsaver\*Innen sind engagiert in Blieskastel. „Geleitet“ wird dies von 3 Botschafter\*Innen und mehreren Betriebsverantwortlichen.
Mit 15 Betrieben stehen aktive Kooperationen. 
Seit Bestehen (10/2020) wurden bei 1218 Abholungen 12691 kg Lebensmittel gerettet.