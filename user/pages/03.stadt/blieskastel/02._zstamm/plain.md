---
title: Aktivität
menu: Baum
image_align: left            
    
---

Wir gratulieren zur **foodsharing Stadt**! Blieskastel hat bereits die Motivationserklärung mit der Stadt unterschrieben und ist Teil der foodsharing Region [Saarpfalz-Kreis](/stadt/kreis.saarpfalz).

