---
title: Info Stände
menu: info_staende
published: true
image_align: left
---

## Info Stände

Eine kleine Gruppe hat im Oktober 2020 auf dem Wochenmarkt in Blieskastel an einem Stand die Menschen über foodsharing und speziell unsere Gruppe in Blieskastel informiert.

Es fand außerdem bereits ein öffentlicher Infoabend in der Orangerie statt, bei dem über Lebensmittelverschwendung und die Arbeit von foodsharing referiert und Fragen der Teilnehmer\*Innen beantwortet wurden. Dies konnte leider coronabedingt bisher nicht wiederholt werden.