---
title: Gemeinschaftliche Events
menu: gemeinschaftliche_events
published: true
image_align: left
---

## Gemeinschaftliche Events

Das Gesundheitsamt ist beim Saarpfalzkreis angegliedert. Durch unsere Kooperation mit dem Saarpfalzkreis können alle Miglieder der foodsharing Gruppen kostenlose Hygieneschulungen vor Ort erhalten.
