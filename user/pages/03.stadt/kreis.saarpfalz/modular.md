---
title: Saarpfalz-Kreis
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
   coordinates: 
       lat: 49.2539
       lng: 7.2263
   status: approved

content:
    items: @self.modular
---
