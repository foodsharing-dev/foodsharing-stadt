---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung [Saarpfalz-Kreis](https://www.saarpfalz-kreis.de/)
- 4 foodsharing Bezirke: St. Ingbert (2018), Mandelbachtal (2020), Blieskastel (2020) und Homburg (2021)
- St. Ingbert und Blieskastel sind die 1. foodsharing Städte des Saarlandes
- Anzahl Kooperationen aller Bezirke: 78
-  Besonderes: Die Biosphäre Bliesgau liegt im Saarpfalz-Kreis. Nachhaltigkeit wird groß geschrieben. Seit 2015 ist der Saarpfalz-Kreis auch Fairtrade-Landkreis.

Titelbild: Saarpfalz-Touristik/Eike Dubois

(Stand: 22.01.2022)
