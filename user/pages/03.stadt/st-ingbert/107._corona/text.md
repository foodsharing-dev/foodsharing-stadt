---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: right
---

## foodsharing in Zeiten von Corona

Unser Fairteiler war während des ersten Lockdowns für 4 Wochen geschlossen. Davor und danach hatten wir die Hygienemaßnahmen am Fairteiler verdoppelt und Aushänge verfasst mit Infos bezüglich Übertragung und Maßnahmen zum Schutz vor COVID-19.
Da die örtliche Tafel geschlossen wurde, haben wir einige Abholungen von dieser übernommen als Tafelersatzabholungen und durch eine öffentliche Fairteilergruppe weitergegeben. 
Bei der Tafel informierten Aushänge über diese Gruppe.

Die monatlichen Treffen unserer foodsharing-Gruppe St. Ingbert finden seit Beginn der Pandemie virtuell statt. 
Zunächst hatten wir die Probeabholungen auf Eis gelegt und führen diese seit vier Wochen wieder durch. Dabei wird die Einführung am Fairteiler virtuell (mithilfe eines selbstgedrehtem Videos) und die anderen Abholungen covidkonform durchgeführt.
