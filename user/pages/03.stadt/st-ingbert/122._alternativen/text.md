---
title: Förderung von Alternativen
menu: alternativen
published: true
image_align: left
---

##Förderung von Alternativen

Da St. Ingbert teil der Biosphäre Bliesgau ist, wird regelmäßig ein Biosphärenmarkt und mehrere zusätzliche Märkte veranstaltet, bei denen sich sowohl Bioläden als auch fairtrade und Weltladen beteiligen können.
Die Abteilung Biosphäre unterstützt nachhaltige Aktionen und Ansiedlung aktiv. 
