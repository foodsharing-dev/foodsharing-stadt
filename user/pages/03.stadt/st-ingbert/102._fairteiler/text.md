---
title: Fairteiler
menu: Fairteiler
published: true
image_align: left
---

## Fairteiler

Unser Fairteiler befindet sich in der Videothek TAPE-O-MANIA, Rickertstraße 10A. Der Eingang zur Videothek befindet sich auf der Rückseite des Gebäudes, neben der Tanzschule Fess.

- <a href="https://foodsharing.de/?page=fairteiler&bid=1857&sub=ft&id=1325">Link zum Fairteiler</a>
