---
title: Integrierte Bildungsarbeit
menu: bildung
image_align: right
published: true
---

## Integrierte Bildungsarbeit

Wir führen regelmäßig, ca. 2 bis 4 mal pro Monat, Bildungsangebote im schulischen und außerschulischen Bereich durch. In der Regel kontaktieren uns die Bildungsträger und wir erarbeiten gemeinsam ein passendes Programm. Geplant und durchgeführt wird dies durch die ca. 20 Mitglieder\*Innen der Bildungs AG. Meistens starten wir solche Angebote mit einem Vortrag über Lebensmittelverschwendung und foodsharing. Nach einem hoffentlich regen Austausch mit den Teilnehmenden enden viele Aktionen mit einer gemeinsamen Verteilung und/oder anschließender Zubereitung von Lebensmitteln, wie z.B. einem gemeinsamen Frühstück. 
