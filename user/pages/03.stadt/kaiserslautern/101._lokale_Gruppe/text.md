---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
image_align: right
published: true
---

## Lokale foodsharing Gruppe

- FoodsaverInnen: 259
- Aktive FoodsaverInnen mit Abholungen mind. einmal im Monat: 164
- Kooperationen: 43
- Engagierte über Abholungen hinaus: 40
- BotschafterInnen: 2
- Fairteiler: 1 und 1 in Planung
- Kontakt: <kaiserslautern@foodsharing.network>