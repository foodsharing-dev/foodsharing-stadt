---
title: Kaiserslautern
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 49.4436 
        lng: 7.7687
    status: approved

content:
    items: @self.modular
---
