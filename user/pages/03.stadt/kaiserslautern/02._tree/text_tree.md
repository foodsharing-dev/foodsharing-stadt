---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Kaiserslautern

- foodsharing Bezirk seit 2012
- Anzahl Kooperationen: 43
- Besonderes: Unser Bezirk zeichnet sich durch die vielen öffentlichen Aktionen aus, die wir 
mit verschiedenen Partner zusammen durchführen.


(Stand: 14.11.2023)
