---
title: Öffentlichkeitsarbeit Team
menu: oeffentlichkeit_team
image_align: right
published: true
---

## Öffentlichkeitsarbeit Team

Für unsere Öffentlichkeitsarbeit haben wir eine eigene AG. Schreib uns gerne eine Mail an: 
<kaiserslautern@foodsharing.network>

Zu unseren Aktionen zählen u.a: 

- Feb. 22: Vortrag mit anschließendem Kochen an der BBS in Landstuhl
- 21.05.22: Verteilung und Infostand beim Fahrradmarkt des BUND
- 22.06.22: Infostand bei BNE-Infoveranstaltung im ZAK (Zentrale Abfallwirtschaft Kaiserslautern)
- 16.07.22: Verteilung und Infostand bei Stadtteilfest im Stadtpark von Kaiserslautern
- 14.12.22: Verteilung mit der „Fairtrade AG“ des Rittersberggymnasiums in Kaiserslautern
- 12.01.23: Vortrag mit Frühstück an der Berufsbildenden Schule I in Kaiserslautern
- 23.02.23: Vortrag mit Frühstück am Rittersberg Gymnasium in Kaiserslautern
- 25. + 26.03.23: Verteilung und Infostand bei der Nachhaltigkeitsmeile „Lautern blüht auf“ in Kaiserslautern
- 14.04.23: Verteilung an der Berufsbildenden Schule I in Kaiserslautern
- 06.05.23: Verteilung und Infostand an der Grundschule in Otterbach
- 13.05.23 Verteilung und Infostand beim Fahrradmarkt des BUND
- 14.06.23: Tütenaktion mit der „Fairtrade AG“ des Rittersberggymnasiums in Kaiserslautern -(foodsharing hat den Schülerinnen und Schülern Lebensmittel gebracht. Diese haben Tüten gepackt und es dann in der Stadt verteilt.)
- 21.06.23: Vortrag mit Frühstück in der Daniel Theysohn IGS - Integrierte Gesamtschule in Waldfischbach-Burgalben
- 13.07.23: Verteilung und Infostand in der Theodor Heuss Grundschule in Kaiserslautern
- 03.08.23: Ferienprogramm mit Jugendlichen zum Thema Lebensmittelverschwendung im JUZ Kaiserslautern
- 03.09.23: Verteilung von Lebensmitteltüten auf dem Kerweumzug in Rodenbach
- 17.09.23: Verteilung und Infostand beim Sommerfest des CVJM in Otterberg
- 19.09.23: Vortrag bei „Restlos Glücklich“ des Ateliers Belleville in Kaiserslautern
- 12.11.23: Kochen mit geretteten Lebensmittel mit dem Vielfalter e.V. in Kaiserslautern
- 22.11.23: Infostand bei einer BNE-Veranstaltung in Kaiserslautern
