---
title: Events
menu: events
image_align: left
published: true
---

## Events

Am 12.09.2022 organisierte Foodsharing Kaiserslautern in Zusammenarbeit mit Klima Lautern das erste eigene Event, den 1. Klimatag in Kaiserslautern. Im Rahmen des Events konnten wir den Kochbus Rheinland Pfalz willkommen heißen. Dort wurden aus geretteten Lebensmitteln gesunde Häppchen zubereitet und an Besucher\*Innen fairteilt. Darüber hinaus fanden sich auch verschiedene AkteurInnen vor Ort, wie u.a. das städtische Management für Klimaschutz, das Bildungsbüro, den BUND und die Stadtbildpflege, die allesamt auf die Klima- und Umweltfolgen unserer Welt aufmerksam machten.