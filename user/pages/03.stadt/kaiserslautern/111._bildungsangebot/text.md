---
title: Bildungsangebot
menu: bildungsangebot
image_align: right
published: true
---

## Bildungsangebot

Um unserem Bildungsauftrag gerecht zu werden, führen wir regelmäßig Projekttage und Veranstaltungen mit verschiedenen Bildungsträgern, u.a. in allen Schulformen sowie mit unterschiedlichen Institutionen und Vereinen, wie dem CVJM, BNE, BUND und dem ZAK durch. Seit 2023 kooperieren wir auch mit dem Bildungsbüro der Stadt KL, mit welchen wir ein Ferienprogramm für Jugendliche gestaltet und durchgeführt haben. 