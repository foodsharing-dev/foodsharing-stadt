---
title: Info Stände
menu: info_staende
image_align: left
published: true
---

## Info Stände

Wir bieten in regelmäßigen Abständen und bei verschiedenen Veranstaltungen und Projekten einen Bildungsstand an. Siehe Punkt 3. Öffentlichkeitsarbeit.