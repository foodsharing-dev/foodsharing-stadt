---
title: Nachhaltiger Umgang mit Geld
menu: geld
image_align: right
published: true
---

## Nachhaltiger Umgang mit Geld

Anschaffungen werden reichlich überlegt und es wird abgewogen, ob, was und wo angeschafft wird. So versuchen wird stets Anschaffungen aus bereits vorhandenen Ressourcen zu schöpfen und erst dann wird eine Neuanschaffung in Erwägung gezogen. Für diesen Fall nehmen wir gezielt Spenden an, die von unserem Kassenwart verwaltet werden.