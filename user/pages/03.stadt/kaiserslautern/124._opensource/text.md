---
title: Nutzung von Open-Source Software
menu: opensource
image_align: right
published: true
---

## Nutzung von Open-Source Software

Wir organisieren unsere bezirksinternen Texte, Regeln und Sammlungen in einer Cloud. Zudem finden unsere FoodsaverInnen über einen Forumsbeitrag eine Navigation zu weiterführenden Links der foodsharing Plattform. Zur Kommunikation, die sich schlecht über die Plattform verwirklichen lassen, wird BigBlueButton über die foodsharing Seite genutzt.