---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
image_align: right
published: true
---

## Öffentliche Lebensmittelrettung

Am Ende des Mittelaltermarktes am Gelterswoog haben wir die nicht mehr verkauften Lebensmittel wie Grillfleisch, Backwaren und Kuchen gerettet und weiter fairteilt. Zudem haben wir bei der Oktoberkerwe bei einigen Ständen die übrig gebliebenen Lebensmittel gerettet. Weitere Rettung sind bereits in Planung.
