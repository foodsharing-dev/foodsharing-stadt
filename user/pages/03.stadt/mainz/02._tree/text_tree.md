---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Mainz

- foodsharing Bezirk seit 2014
- foodsharing-Stadt seit 2022
- Anzahl Kooperationen: 109 laufenden Kooperationen
- Besonderes: Wir sind sehr aktiv im Bereich Öffentlichkeitsarbeit und haben eine eigene [Bezirkswebsite](https://www.foodsharing-mainz.de/).
- Mainz hat eine eigene [Resolution](https://www.foodsharing-mainz.de/die-foodsharing-stadt-mainz/#Resolution) mit der Stadt Mainz ausgearbeitet und unterzeichnet.


(Stand: Mai 2022)
