---
title: Info Stände
menu: info_staende
published: true
image_align: left
---

## Info Stände

Foodsharing Mainz tritt vor allem bei Demos und im Rahmen (umwelt-)politischer Veranstaltungen und Festivals auf. 
Es gab bereits Infostände und Lebensmittel-Fairteilungen bei Fridays for Future, dem OpenOhr Festival, dem Herzblick Festival, dem Mädchentag der Mainzer Jugendzentren sowie mehrere Infostände in Kooperation mit der BUNDjugend im Bereich des Projekts ,,Essen.Macht..." und viele mehr ;) 
