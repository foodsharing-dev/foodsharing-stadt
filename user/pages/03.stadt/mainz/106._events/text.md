---
title: Events
menu: events
published: true
image_align: left
---

## Events

Seit einigen Jahren organisiert Foodsharing Mainz den Foodsharing-Brunch in wechselnden Locations. 

Zusammen mit dem Kochbus Rheinland-Pfalz und der BUNDjugend organisierte foodsharing eine [Schnippeldisko auf dem Marktplatz](https://www.bundjugend-rlp.de/schnibbeldisco/). Gemeinsam mit Passanten wurden hier zuvor auf dem Markt gerettete Lebensmittel verkocht, verzerrt und Gespräche über Lebenmittelverschwendung geführt. 

Im September 2020 fand der Tag des Offenen Fairteilers im Rahmen der ersten bundesweiten Aktionswoche "Deutschland rettet Lebensmittel! - Zu gut für die Tonne" an sieben verschiedenen Standorten in Mainz zeitgleich statt. Es gab zum einen Stände mit Infos, Spielen, Aktionen und mehr zu Lebensmittelverschwendung und zum anderen Fairteil-Aktionen. 

Auf unserer [Instagram-Seite](https://www.instagram.com/foodsharingmainz/) könnt ihr euch noch mehr Fotos zu diesen und anderen Aktionen ansehen: 

! Vorankündigung: foodsharing Mainz wird am Rheinland-Pfalz Tag, der 2022 in der Landeshauptstadt stattfindet, mit einem eigenen Stand vertreten sein.
