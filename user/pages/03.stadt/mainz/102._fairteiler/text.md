---
title: Fairteiler
menu: Fairteiler
published: true
image_align: left
---

## Fairteiler

Es gibt mittlerweile für fast jeden Stadtteil in Mainz einen eigenen Fairteiler. 
- <a href="https://foodsharing.de/?page=fairteiler&bid=46&sub=ft&id=372">Altstadt</a>
- <a href="https://foodsharing.de/?page=fairteiler&bid=46&sub=ft&id=1301">Bretzenheim</a>
- <a href="https://foodsharing.de/?page=fairteiler&bid=46&sub=ft&id=1770">Hechtsheim</a>
- <a href="https://foodsharing.de/?page=fairteiler&bid=46&sub=ft&id=1322">Kostheim</a>
- <a href="https://foodsharing.de/?page=fairteiler&sub=ft&bid=46&id=2014">Mombach</a>
- <a href="https://foodsharing.de/?page=fairteiler&bid=46&sub=ft&id=1731">Neustadt</a>
- <a href="https://foodsharing.de/?page=fairteiler&bid=46&sub=ft&id=1641">Oberstadt</a>
- <a href="https://foodsharing.de/?page=fairteiler&bid=46&sub=ft&id=1732">Weisenau</a>

Außerdem gibt es WhatsApp- und/oder Telegramgruppen in den Stadtteilen für die Weiterverteilung der Lebensmittel. 
Zudem gibt es eine [Facebookgruppe für Mainz und Umgebung](https://www.facebook.com/groups/FoodsharingMainz)

