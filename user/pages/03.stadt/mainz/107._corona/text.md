---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: right
---

## foodsharing in Zeiten von Corona

Seit März 2020 veröffentlichen die Mainzer Botschafter\*innen interne Updates in Bezug auf den die Pandemie. Als die Mainzer Tafel schließen musste hat foodsharing die meisten Kooperation der Tafel kurzfristig übernommen. Dadurch konnten große Mengen an Lebensmitteln weiterverteilt werden. Es gibt eine enge Zusammenarbeit von foodsharing mit Mainzer Organisationen der Wohnungslosenhilfe. 
