---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: right
---

## Öffentlichkeitsarbeit Team

Die Öffentlichkeitsarbeit bildet eine der zentralen Säulen unserer Siegener Foodsharing-Gruppe. Über unsere Social-Media-Kanäle posten wir regelmäßig Bilder von Abholungen sowie Ankündigungen zu den monatlichen Foodsharing- und Neulingstreffen. Darüber hinaus hat das ÖA-Team im vergangenen Jahr zwei Projekte umgesetzt, um Foodsharing in Siegen bekannter und zugänglicher zu machen. Hierfür wurden einerseits zehn Sharepics erstellt, um auch für Außenstehende Begriffe wie „Foodsaver:in" oder „Fairteiler" und Unterschiede zu Organisationen, wie etwa der Tafel, zu erklären.
Darüber hinaus haben wir filmische Kurzporträts einiger kooperierender Betriebe erstellt, in denen diese die Möglichkeit hatten, die Beweggründe für ihre Zusammenarbeit mit Foodsharing darzustellen und ein Feedback über die laufende Kooperation abzugeben. Seit Anfang Mai findet man diese auf unserem Instagram-Profil [@foodsharing.siegen](https://www.instagram.com/foodsharing.siegen/).
Durch die Zusammenarbeit mit regionalen und überregionalen Print- und Filmmedien sind ebenfalls einige Artikel in Lokalzeitungen sowie Kurzbeiträge des WDR entstanden. 
