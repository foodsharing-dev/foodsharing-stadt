---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

Im Bezirk Siegen engagieren sich zurzeit 272 Foodsaver\*innen für die Rettung von Lebensmitteln (Stand April 2021). Fünf Botschafter*innen repräsentieren Foodsharing Siegen nach außen und übernehmen die wesentlichen organisatorischen Aufgaben. Darüber hinaus sind viele weitere Menschen in Siegen aktiv. Wir treffen uns jeden zweiten Samstag im Monat (zurzeit online). Außerdem findet ca. alle zwei Monate ein Neulings- und Orientierungstreffen statt. 
Kontakt: [siegen@foodsharing.network](mailto:siegen@foodsharing.network) 
