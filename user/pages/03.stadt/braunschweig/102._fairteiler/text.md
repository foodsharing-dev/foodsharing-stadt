---
title: Fairteiler
menu: Fairteiler
image_align: left
---

## Fairteiler

Wir haben einen <a href="https://foodsharing.de/?page=fairteiler&bid=40&sub=ft&id=1106">Fairteiler</a>, der im Altgebäude der Technischen Universität Braunschweig in zwei alten Telefonzellen zu finden ist. Geöffnet ist er von Mo-Fr um 8-22:00 Uhr und am Samstag von 9-15:30 Uhr. Es gibt ein Kistenregal und einen Kühlschrank.


