---
title: Beratungsteam für Betriebe
menu: betriebe_team
image_align: left
---

## Beratungsteam für Betriebe

Wir haben ein Team, das sich speziell um die Beratung von Betrieben kümmert, damit sie in Zukunft weniger Lebensmittel verschwenden.
Miri Heil ist Ansprechpartnerin für neue Kooperationen und Beratungsangebote und Jilly Latumena für Materialien. Schreibt uns gerne eine Mail an
<darmstadt@foodsharing.network>. 


