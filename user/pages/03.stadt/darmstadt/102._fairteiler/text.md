---
title: Fairteiler
menu: Fairteiler
image_align: left
---

## Fairteiler
In Darmstadt gibt es einen Fairteiler in der <a href="https://foodsharing.de/?page=fairteiler&bid=25&sub=ft&id=545">Hochschule</a> und
einen in der <a href="https://foodsharing.de/?page=fairteiler&bid=25&sub=ft&id=37">Uni</a>. 
