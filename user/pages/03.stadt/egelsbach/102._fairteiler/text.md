---
title: Fairteiler
menu: fairteiler
image_align: left
published: true
---

## Fairteiler

Wir haben derzeit einen Fairteiler in Egelsbach. Hier der entsprechende Link:

<a href="https://foodsharing.de/?page=fairteiler&bid=3869&sub=ft&id=2439">foodsharing | Fairteiler Egelsbach</a> 

Zudem hängt am Fairteiler ein QR-Code für die dazugehörige WhatsApp Gruppe.

Außerdem haben wir eine foodsharing Gruppe auf facebook welches es allen ermöglicht seine übrigen Lebensmittel zu fairteilen.

[Foodsharing - Langen Egelsbach Dreieich | Facebook](https://www.facebook.com/groups/FoodsharingLangenEgelsbachDreieich)
