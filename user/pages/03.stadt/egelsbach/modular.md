---
title: Egelsbach
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
       lat: 49.9656
       lng: 8.6644
    status: pending

content:
    items: @self.modular
---
