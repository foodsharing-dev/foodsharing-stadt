---
title: Weitere lokale Initiativen
menu: mehr_projekte
image_align: right
published: true
---

## Weitere lokale Initiativen

In Egelsbach ist die Tafel Langen verantwortlich. Diese holt regelmäßig noch genießbare Lebensmittel in den Betrieben ab und verteilt diese an Bedürftige aus Dreieich, Langen und Egelsbach. Hier besteht schon seit vielen Jahren eine freundschaftliche Kooperation zwischen der Tafel und foodsharing.

[Langener Tafel e.V. – Lebensmittel retten. Menschen helfen. (langener-tafel.de)](https://www.langener-tafel.de)

Der Birkenhof in Egelsbach ist ein Schulbauernhof und gibt Kindern und Jugendlichen die Möglichkeit Prozesse der Landwirtschaft kennenzulernen.

[Schulklassen/ Kindergeburtstage (birkenhof-egelsbach.de)](https://www.birkenhof-egelsbach.de/schulklassen-kindergeburtstage.html)
