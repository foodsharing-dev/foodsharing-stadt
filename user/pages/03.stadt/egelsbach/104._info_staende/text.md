---
title: Info Stände
menu: info_staende
image_align: left
published: true
---

## Info Stände

Wir haben bei der feierlichen Eröffnung unseres Fairteilers im Mai 2023 durch den Bürgermeister der Stadt Egelsbach die Möglichkeit gehabt einen Infostand aufzubauen und auf die Lebensmittelverschwendung aufmerksam zu machen.

[Schrank mit geretteten Lebensmitteln: Egelsbach hat ersten Fairteiler (op-online.de)](https://www.op-online.de/region/egelsbach/schrank-mit-geretteten-lebensmitteln-egelsbach-hat-ersten-fairteiler-92280035.html)

Wir hoffen hier in Zukunft noch stärker präsent zu sein. 