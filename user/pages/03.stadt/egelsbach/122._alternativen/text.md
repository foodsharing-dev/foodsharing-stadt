---
title: Förderung von Alternativen
menu: alternativen
image_align: left
published: true
---

## Förderung von Alternativen

Die Stadt Egelsbach fördert die Bewirtschaftung und den Erhalt der Streuobstwiesen. Dabei werden die Produkte unter dem Siegel Siebenschläfer verkauft.

[flyer_siebenschläferprodukte.pdf (neue-stadthalle-langen.de)](https://www.neue-stadthalle-langen.de/download/44741/flyer_siebenschl%C3%A4ferprodukte.pdf)
