---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
image_align: right
published: true
---

## Öffentliche Lebensmittelrettung

Wir konnten bisher bei zwei kleineren öffentlichen Veranstaltungen Lebensmittel retten.

Zum einen durften wir an Fastnacht die Reste vom Heringsessen der städtischen SPD retten. Hierzu waren unsere Foodsaver mit Eimern und Dosen vor Ort um alles was von der Veranstaltung übrig war zu retten. Das Ganze erfolgte recht kurzfristig auf Abruf und musste sehr schnell gehen. Anschließnd verteilten wir es über unsere örtliche WhatsApp Gruppe zügig weiter.

Auch beim örtlichen Handballturnier duften wir schon den restlichen Kuchen retten.