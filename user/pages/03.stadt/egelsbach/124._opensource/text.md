---
title: Nutzung von Open-Source Software
menu: opensource
image_align: right
published: true
---

## Nutzung von Open-Source Software

Uns wurde durch ein lokales Unternehmen ein entsprechender Server zur Verfügung gestellt auf dem unsere Homepage und damit unser öffentlicher Auftritt läuft.

[Foodsharing Landkreis Offenbach West (foodsharing-lkofw.de)](https://foodsharing-lkofw.de)

Mit unseren Vereinsmitgliedern kommunizieren wir über die Plattform foodsharing.de. Dort nutzen wir das Videokonferenzsystem zum Beispiel für Plenum oder interne Schulung unserer Foodsaver\*Innen und Betriebsveranwortlichen.