---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
image_align: right
published: true
---

## Lokale foodsharing Gruppe

Egelsbach mit seinen knapp 11.000 EW gehört zum foodsharing Bezirk Landkreis Offenbach West, welcher sich im September 2021 gegründet hat. 

- 2 aktive Kooperationen in Egelsbach
- 7 aktive Foodsaver in Egelsbach
- 5 Foodsaver in Neu-Isenburg engagieren sich über das Retten hinaus zum Beispiel bei Veranstaltungen, AGs oder der Betreuung von Fairteilern. 