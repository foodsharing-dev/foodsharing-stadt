---
title: Gemeinschaftlicher Fairteiler
menu: gemeinschaftlicher_fairteiler
image_align: left
published: true
---

## Gemeinschaftlicher Fairteiler

Unser Fairteiler in Egelsbach steht direkt auf dem Platz der Horst-Schmidt-Halle. Dabei hat foodsharing den Schrank zur Verfügung gestellt und dieser wurde durch die Dienstleistungsbetriebe der Stadt Egelsbach aufgestellt und verankert. Die Stadt stellt uns zudem die Fläche zu Verfügung.

Foodsharing übernimmt die regelmäßige Pflege und Betreuung.

Alle Fairteiler stellen sich auf unserer Homepage und auf foodsharing.de vor:

[Fairteiler – Foodsharing Landkreis Offenbach West (foodsharing-lkofw.de)](https://foodsharing-lkofw.de/index.php/fairteiler/)
