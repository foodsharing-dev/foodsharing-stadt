---
title: Lebensmittelanbau
menu: lebensmittelanbau
image_align: left
published: true
---

## Lebensmittelanbau

 Der Birkenhof in Egelsbach hat eine SoLaWi-Hofgemeinschaft und unterstütz weitere SoLaWi in Umkreis mit Pflanzen und Wissen.

[SoLawi -Hofgemeinschaft (birkenhof-egelsbach.de)](https://www.birkenhof-egelsbach.de/solawi.html)

Ein weiteres Projekt betrifft die Streuobstwiesen in Egelsbach. Die Stadt bewirtschaftet diese in Zusammenarbeit mit dem Obst- und Gartenbauverein. Hier finden regelmäßig Ernteaktionen statt und mit dem Verkauf der produzierten Produkten wird der Unterhalt des Lebensraums Streuobstwiese zu unterhalten.

[Streuobstwiesen in Egelsbach (ogv-egelsbach.de)](https://www.ogv-egelsbach.de/der-verein/vereinsleben/ereignisse-2016/32-streuobstwiesen-in-egelsbach)
