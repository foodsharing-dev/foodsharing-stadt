---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Egelsbach

Der foodsharing Bezirk Landkreis Offenbach West, zu welchem Neu-Isenburg gehört, hat sich im September 2021 vom Bezirk Offenbach abgespalten. 

- Anzahl der Kooperationen: 59 im gesamten Bezirk
- Anzahl der foodsaver*innen: 88
- Anzahl der aktiven Foodsaver*innen über das Retten hinaus: ca. 80%
- Besonderes: Gemeinnütziger Verein (foodsharing Landkreis Offenbach West e.V.


(Stand: Juni 2023)
