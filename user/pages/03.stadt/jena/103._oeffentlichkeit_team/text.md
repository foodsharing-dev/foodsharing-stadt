---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: left
---

## Öffentlichkeitsarbeit Team

Unsere Öffentlichkeitsarbeitsgruppe kümmert sich darum, Inhalte und Ziele von Foodsharing der Öffentlichkeit näherzubringen. Hier sind die Themen Lebensmittelwertschätzung und Lebensmittelverschwendung eng aneinandergekoppelt. Dabei ist es uns wichtig Aufklärungsarbeit zu leisten. Aus diesem Grund haben wir eine Bildungs-AG gegründet, damit wir auf Veranstaltungen verschiedene Zielgruppen mit Workshops und Informationsmaterialien erreichen können. 