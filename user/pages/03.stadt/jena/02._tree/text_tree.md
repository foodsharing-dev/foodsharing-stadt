---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Jena

- foodsharing Bezirk seit März 2014
- Anzahl Kooperationen: 18 Betriebe
- Besonderes: Jena ist eine klassische Studentenstadt, weshalb unsere Strukturen vorwiegend durch Studierende gepflegt werden. Aber auch in anderen Altersgruppen erfreuen wir uns über positive Resonanzerfahrungen. 

(Stand: 27.10.2020)
