---
title: Fairteiler
menu: Fairteiler
image_align: left
---

## Fairteiler

Wir haben mittlerweile [37 Fairteiler](https://foodsharing.de/?page=fairteiler&bid=13).
Alle Fairteiler sind mit Kühlschrank, ein Teil hat zusätzlich ein Regal. Die meisten sind in Lokalen und Volkshochschulen und nur zu deren Öffnungszeiten zugänglich. Wir haben auch 3 die im Freien stehen und 24/7 zugänglich sind. 2 davon sind in Lokalen, 1 auf dem Grund einer Pfarre.

 
Foto Boulderbar - links
