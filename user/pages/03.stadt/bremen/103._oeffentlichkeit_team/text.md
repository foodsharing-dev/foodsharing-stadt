---
title: Öffentlichkeitsarbeit Team
menu: oeffentlichkeit_team
image_align: right
published: true
---

## Öffentlichkeitsarbeit Team

Es gibt eine aktive Öffentlichkeitsarbeit AG, in der sich alle Foodsaver*innen engagieren können- bei Interesse gerne der AG eine Mail schreiben unter oeffentlichkeitsarbeit.bremen@foodsharing.network
