---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Bremen

- foodsharing Bezirk seit Februar 2014
- Anzahl Kooperationen: über 70 laufende Kooperationen
- Besonderes: Bremen (Stadt) gehört schon seit dem frühen Beginn von foodsharing zu einem sehr aktiven Bezirk, der jetzt auch über <a href="https://www.tiktok.com/@foodsharing_bremenfoodsharing">einen Tiktok Kanal</a> verfügt  


(Stand: Oktober 2023)
