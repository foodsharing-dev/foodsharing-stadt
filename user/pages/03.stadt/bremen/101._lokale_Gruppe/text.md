---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
image_align: right
published: true
---

## Lokale foodsharing Gruppe

Foodsharing Bremen gibt es seit dem Februar 2014 und bis heute engagieren sich knapp 300 Personen im Monat in über 70 Betrieben. Insgesamt konnten schon über 814396 kg an Lebensmittel gerettet in
mehr als 43384 Rettungseinsätzen. 