---
title: Bremen
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 53.07704
        lng: 8.787983
    status: pending

content:
    items: @self.modular
---
