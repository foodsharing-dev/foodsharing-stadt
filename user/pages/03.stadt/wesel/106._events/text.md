---
title: Events
menu: events
published: true
image_align: left
---

## Events

Wir haben am 10.02.2023 eine fairteilung am Rathaus Wesel durchgeführt. Nach offizieller Genehmigung des Ordnungsamtes, haben wir von 14-16 Uhr etwa 150 Kisten an etwa 250 Menschen fairteilt! Dazu hat das Radio und die Zeitung Beiträge gemacht. Es war ein voller Erfolg der geordnet und ohne Probleme abgelaufen ist!

Diese Aktion wird wiederholt. Das steht jetzt schon fest. Danke allen die da waren, sich engagiert haben, Danke allen fleißigen Helfern, Danke an die #foodsharingstadt Wesel für die Erlaubnis und Danke Allen für das Retten. Schön, dass ihr alle da wart.

