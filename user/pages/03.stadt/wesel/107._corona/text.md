---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: left
---

## foodsharing in Zeiten von Corona

Da wir in Zeiten von Corona angefangen haben, kennen wir foodsharing ohne Corona gar nicht;-) Die Anzahl der Slots wurde an die jeweiligen Corona-Regeln angepasst. Trotz Ausgangssperre und Quarantäne von mehreren Foodsaver\*innnen haben wir eine 100% Abholquote in allen Betrieben erreicht. Damit trotz verschiedener Ausgangsbeschränkungen alle Rettungseinsätze problemlos durchgeführt werden konnten, wurden mit den Betrieben teilweise ausgeklügelte Rettungen abgesprochen um just in time mit den Ausgangssperren zu retten.  Auch die anschließenden Fairteilungen mussten hier mit eingeplant werden. 
Wir haben sowohl alle Abholungen als auch Einführungsabholungen von neuen Foodsaver\*innen unter den jeweils geltenden Hygienemaßnahmen durchgeführt.
Aufgrund der für die Betriebe noch schlechter zu kalkulierenden Einkaufsmengen als außerhalb der Corona-Zeiten wurden hier teilweise immense Mengen an Lebensmitteln gerettet. 

Besonders schön war die Solidarität der Foodsaver\*innen untereinander. Musste z.B. eine Familie aus dem Kreis der aktiven Foodsaver\*innen in Quarantäne, wurden diese durch andere Foodsaver\*innen nach Rettungen mit Lebensmitteln versorgt. Genauso schnell sind andere Foodsaver\*innen für andere eingesprungen, wenn es um quarantänebedingt ausgefallene Rettungen ging. 
  
