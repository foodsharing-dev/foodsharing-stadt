---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: left
---

## Öffentlichkeitsarbeit Team

Unsere Öffentlichkeitsarbeits-Gruppe besteht zwar nur aus einer Foodsaverin und einem Foodsaver, aber wir haben schon einige Zeitungsartikel und sogar einen Radiobericht initiieren können. Ganz am Anfang wurde über die Entstehung des Bezirks Wesel berichtet (NRZ am 17.10.2020, Radio KW am 26.10.2020). In der Weihnachtszeit hat die NRZ über unsere Tafelersatzabholungen berichtet (NRZ 30.12. 2020). Am 20.7.2021 erschien ein ausführlicher Artikel  über unsere Tätigkeit in der Rheinischen Post. Außerdem erschien ein kurzer Bericht über die Unterstützung des Umweltausschusses in Wesel (20.01. hat Radio KW berichtet, 20.05. ein Bericht in der NRZ), in dem betont wurde, dass ein öffentlicher Fairteiler dringend gesucht wird. Darüber hinaus wurde die Zusammenarbeit mit der Tafel Hamminkeln mehrfach in kürzeren Artikeln erwähnt (z.B. NRZ 21.12.2020 oder 21.04.2021).
Außerdem sind wir aktiv auf Facebook (Foodsharing Wesel und Hamminkeln), Instagram (foodsharingwesel) und TikTok (foodsharing_wesel).
Geplant ist ein Video-Interview  mit dem Youtube-Kanal „Wir Machen Zukunft TV“  - das Interview findet am 22.07.2021 statt. 
