---
title: Wesel und Hamminkeln
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 51.6572
        lng: 6.6175
    status: approved

content:
    items: @self.modular
---
