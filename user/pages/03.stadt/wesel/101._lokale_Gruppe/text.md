---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver\*innen:  130
- Abholer\*innen mind. einmal im Monat:  ca. 50

foodsharing gibt es in Wesel und Hamminkeln seit Oktober 2020. Wir sind also ein sehr junger Bezirk,  aber die Anzahl sowohl der Foodsaver\*innen als auch der Kooperationen steigt stetig. Es gibt aktuell circa 130 Personen, die in den Bezirken Wesel und Hamminkeln registriert sind. Aktiv retten im Moment ca. 50 Foodsaver\*innen in 33 Betrieben. 
Bis Juli 2021 haben wir  insgesamt fast 50.000kg Lebensmittel gerettet.
Wir arbeiten eng zusammen mit 3 Tafeln in Wesel und Umgebung. In allen Betrieben handeln wir natürlich nach dem Motto „Tafel first“. In einigen Betrieben führen wir Tafelersatzabholungen durch. In den Weihnachtsferien haben wir die kompletten Tafelabholungen in Wesel und teilweise in Hamminkeln übernommen. 
Was mit der Suche nach einem Fairteiler über den Umweltausschuss und die Bürgermeisterin in Wesel angefangen hat, hat fürs Erste mit einer kostenlosen 240Liter Mülltonne, die foodsharing Wesel/Hamminkeln  zur Verfügung gestellt wird, aufgehört. Die Stadt bezahlt diese Mülltonne, damit zumindest beim 2. Aussortieren keine Kosten für foodsharing Wesel/Hamminkeln entstehen. Die Suche nach einem Fairteiler geht jedoch weiter, auch seitens der öffentlichen Hand. 
