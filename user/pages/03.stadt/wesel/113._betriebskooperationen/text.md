---
title: Herausragende Betriebskooperationen
menu: betriebskooperationen
published: true
image_align: left
---

## Herausragende Betriebskooperationen
Eine sehr besondere Kooperation haben wir mit dem Biomarkt Wesel/Bocholt. Zweimal pro Woche holen unsere Foodsaver\*innen Obst, Gemüse und Backwaren in bio Qualität auf dem Betriebshof in Hamminkeln-Mehrhoog ab. Bei unserem WDR-Fernsehdreh am 14.09.2021 duften wir das Fernsehteam mit zu einer Lebensmittelrettung auf dem Betriebshof mitnehmen. Die Betreiber des Biohofs haben stolz von der Zusammenarbeit mit foodsharing erzählt. 
Am 12.04.2022 haben wir auf Wunsch des Biomarktes ein kleines Fotoshooting gemacht. Unter dem Motto „Sharing ist Caring“ haben die Betreiber des Biomarktes die Fotos auf deren Instagram Account sowie deren Facebook Seite veröffentlicht. Der Text unter und neben den Fotos zeigt deutlich, dass das Biohof-Team von foodsharing überzeugt ist und gerne mit uns zusammenarbeitet. Außerdem achtet auch der Biohof selber auf die Nachhaltigkeit. Unsere aussortierten Lebensmittel werden nicht entsorgt, sondern an die eigenen Hühner verfüttert. Übriggebliebenes oder Obst und Gemüse, das nicht mehr so schön ist, wird in der Betriebsküche verarbeitet oder von der Inhaberfamilie selber verbraucht. Außerdem gibt es einen kleinen Hofladen, der komplett auf Vertrauensbasis läuft. Sehr sympathisch ist, dass die Inhaber sich freuen, wenn unsere Foodsaver\*innen Selbstgemachtes aus geretteten Lebensmitteln zum Probieren mit zur Rettung bringen. Solche Kooperationen wünscht man sich!

Wir haben seit Januar 2023 eine weitere herausragende Kooperation. Wir retten im Evangelischen Krankenhaus Wesel (EVK) übrig gebliebene verschweißte Essensportionen und übrig Gebliebenes aus der Kantine, also auch offene Lebensmittel.




