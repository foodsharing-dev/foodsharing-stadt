---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Wesel und Hamminkeln

- foodsharing Bezirk seit Oktober 2020
- Anzahl Kooperationen: 33
- Besonderes: Wir haben angefangen als Wesel und Umgebung, im Frühjahr 2021 haben wir aufgrund der Anzahl der dortigen Kooperationen und der örtlichen Verbundenheit in der ländlichen Gegend  den Bezirk aufgeteilt und seit April 2021 gibt es zusätzlich den Bezirk Hamminkeln (Kreis Wesel). Die Grenzen sind jedoch fließend und die Botschafterin und der Botschafter  kümmern sich um beide Bezirke. Wir verstehen uns trotz der zwei Bezirke als eine Gemeinschaft.
Eine große Besonderheit ist die Kooperation mit den drei Tafeln. Zum einem haben wir als foodsharing die zuvor nicht vorhandene Kooperation der regionalen Tafeln untereinander angeregt und zur Vermittlung beigetragen. Zum anderen basiert die Kooperation auf beidseitigem Geben und Nehmen. Wir retten die Tafelreste nach deren Verteilungen und die Tafel bekommt von uns Lebensmittel nach Großrettungen für deren Verteilung. Mittlerweile ist das Vertrauen seitens der Tafel so groß, dass foodsharing Wesel und Hamminkeln im Namen der Tafeln Ersatzabholungen durchführen darf.  

(Stand: 20.07.2021)
