---
title: Bildungsangebot
menu: bildungsangebot
published: true
image_align: right
---

## Bildungsangebot
Im Sommer 2021 durften wir das Sommerlager einer Pfadfindergruppe mit geretteten Lebensmitteln versorgen und gleichzeitig die Pfadfinder spielerisch über foodsharing  und Lebensmittelverschwendung informieren. Wir haben eine Foodsharing-Rallye mit Fragen und  Aufgaben gestaltet. Diese wurde sowohl von den Kindern und Jugendlichen als auch von den Leiter\*innen sehr positiv aufgenommen. Wir haben einen schönen Nachmittag mit den Pfadfindern verbracht und sind uns sicher, dass sie mit dem Thema Lebensmittel in der Zukunft sensibilisiert umgehen werden. Für ein paar Kinder war foodsharing nicht neu, sie kannten bereits aus der Familie und aus dem Bekanntenkreis Foodsaver\*innen bzw.  Retter\*innen, die sich aktiv gegen Lebensmittelverschwendung engagieren.

Im November 2022 haben wir in Zusammenarbeit mit der VHS Wesel mit einem Vortrag zum Thema foodsharing und die foodsharing-Stadt Wesel informiert. Eindrücke dazu könnt ihr aus dem veröffentlichten [Zeitungsartikel](https://www.nrz.de/staedte/wesel-hamminkeln-schermbeck/lebensmittel-retten-wie-geht-das-eigentlich-in-wesel-id236932699.html?service=amp) entnehmen.

