---
title: Info Stände
menu: info_staende
published: true
image_align: left
---

## Info Stände
Unser Bezirk hatte Infostände auf bereits zwei Veranstaltungen der Stadt Wesel. Die erste Veranstaltung fand am 04.09.2021 statt. Auf dem Markt der Möglichkeiten im Rahmen der Interkulturellen Tage haben unsere Foodsaver\*innen viele gerettete Lebensmittel  fairteilt. Auch wurden viele Fragen zu unserem Engagement beantwortet. Wir haben dank unseres Infostandes eine neue Foodsaverin sowie über hundert neue Retter\*innen gewinnen können. Am Ende des Marktes haben wir den Nachbarstand mit leckerem Bigos berettet. 

Den zweiten Infostand hatten wir auf dem Feierabend Markt am 02.06.2022. Auch hier fanden viele interessante Gespräche statt und wir haben ein breites Publikum zum Thema foodsharing und Lebensmittelverschwendung erreicht.

Der nächste Infostand ist für den 11.06.2022 auf dem Second Hand Umweltmarkt geplant.
Am 27.08.2022 sind wir wieder für den Markt der Möglichkeiten eingeladen. Somit sieht man, dass der Infostand ein fester Bestandteil der foodsharing Gruppe Wesel und Hamminkeln auf verschiedensten Veranstaltungen der Stadt geworden ist. 

