---
title: Integrierte Bildungsarbeit
menu: bildung
published: true
image_align: right
---

## Integrierte Bildungsarbeit
Seit Anfang des Jahres hat foodsharing Wesel eine interessante „Abgabestelle“. Der Kindergarten Abenteuerland engagiert sich zusammen mit uns gegen die Lebensmittelverschwendung.
Obst und Gemüse von foodsharing sind jeden Tag ein Teil des Essensplans. Die Kinder beteiligen sich aktiv an der Verarbeitung und  entscheiden, ob ein Apfel so gegessen werden soll, oder lieber schon zum Apfelmuss verarbeitet werden soll. Sie tragen den Gedanken, dass keine guten Lebensmittel in die Tonne gehören, mit nach Hause zu ihren Eltern und tragen somit zur Aufklärung von möglichst vielen Menschen bei.
Freitags stehen Kisten mit übrig gebliebenem Obst und Gemüse vor der Kita – hier dürfen sich alle bedienen. Gegen 16 Uhr ist die letzte Kiste leer. Es freut uns sehr, dass so eine tolle Sache dank des Engagements des Kitaleiters, der selber Foodsaver ist, entstanden ist.

