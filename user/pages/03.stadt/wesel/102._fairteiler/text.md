---
title: Fairteiler
menu: Fairteiler
published: true
image_align: right
---

## Fairteiler

Einen Fairteiler im herkömmlichen Sinne gibt es in Wesel noch nicht. Ein Foodsaver betreibt jedoch samstags einen „mobilen Fairteiler“. Er rettet nacheinander in mehreren Betrieben und fährt dann mit seinem umgebauten Sprinter in ein ländlich gelegenes Nachbarsdorf, wo er eine Fairteilung macht. Im Sprinter befinden sich auch ein Kühlschrank und sogar ein Gefrierfach, sodass die Kühlkette definitiv eingehalten bleibt.  Zur Gewährleistung der Rückverfolgbarkeit wird bei diesen Fairteilungen eine Anwesenheitsliste geführt. 
Die Suche nach einem öffentlichen Fairteiler geht aber natürlich weiter und hoffentlich wird es bald auch in Wesel oder Hamminkeln einen Fairteiler geben. 
