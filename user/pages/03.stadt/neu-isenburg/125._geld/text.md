---
title: Nachhaltiger Umgang mit Geld
menu: geld
image_align: right
published: true
---

## Nachhaltiger Umgang mit Geld

Unser gemeinnütziger Verein finanziert sich rein aus Spenden. Diese kommen von Privatpersonen oder auch von unseren Mitgliedern.

Zudem wurden wir 2022 mit dem Rotary Preis für Soziales und Nachhaltigkeit ausgezeichnet.

[Rotary Magazin Artikel: Dreieich - Rotary-Sozialpreis der beiden Dreieicher Clubs zum zwanzigsten Mal verliehen](https://rotary.de/clubs/distriktberichte/rotary-sozialpreis-der-beiden-dreieicher-clubs-zum-zwanzigsten-mal-verliehen-a-20863.html)

Diese Gelder nutzen wir vor allem für unsere Versicherung, um unseren Foodsavern\*Innen möglichst wenigen Risiken auszusetzen und unsere Kooperationspartner entsprechend abzusichern.

Dazu kommen Ausgaben für Werbematerialien (Aufsteller, Buttons) oder auch für Materialien für unsere Infostände (Schürzen).