---
title: Öffentlichkeitsarbeit der Stadt
menu: oeffentliche_kommunikation
image_align: left
published: true
---

## Öffentlichkeitsarbeit der Stadt

Neu-Isenburg ist Fair-Trade-Stadt und setzt sich seit vielen Jahren für mehr Nachhaltigkeit ein.

[Fair Trade Stadt Neu-Isenburg](https://neu-isenburg.de/buergerservice/rathauspresse/pressemitteilungen/pressemitteilung/fair-trade-stadt-neu-isenburg-4/)

Zudem hat Neu-Isenburg einen Nachhaltigkeits-Stadtplan. Bei der nächsten Neuauflage wird hier auch unser Fairteiler mit aufgenommen.

[Nachhaltiger Stadtplan für Neu-Isenburg neu aufgelegt](https://neu-isenburg.de/buergerservice/rathauspresse/pressemitteilungen/pressemitteilung/nachhaltiger-stadtplan-fuer-neu-isenburg-neu-aufgelegt/)