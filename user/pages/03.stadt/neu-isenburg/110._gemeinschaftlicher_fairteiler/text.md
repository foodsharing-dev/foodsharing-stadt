---
title: Gemeinschaftlicher Fairteiler
menu: gemeinschaftlicher_fairteiler
image_align: left
published: true
---

## Gemeinschaftlicher Fairteiler

Unser Fairteiler steht an der Martin-Luther-Gemeinde in Neu-Isenburg. Dieser Platz wird uns von der Stadt in Zusammenarbeit mit dem Ordnungsamt und der Kirche zur Verfügung gestellt. Derzeit ist geplant den Fairteiler mit der Kirchenjugend zusammen zu gestalten. Die tägliche Reinigung und Betreuung erfolgt durch unser Vereinsmitglieder.

[Lebensmittel in Neu-Isenburg vor dem Wegwerfen retten (op-online.de)](https://www.op-online.de/region/neu-isenburg/lebensmittel-in-neu-isenburg-vor-dem-wegwerfen-retten-92056993.html?utm_medium=Social&utm_source=Facebook&fbclid=IwAR3HDJlT5H0f3rBiiFK2UoLLM9Ny4ID31DE3gLjls3xd8LSRGgU3OEF59Zs#Echobox=1675075099)

Alle Fairteiler stellen sich auf unserer Homepage und auf foodsharing.de vor.

[Fairteiler – Foodsharing Landkreis Offenbach West (foodsharing-lkofw.de)](https://foodsharing-lkofw.de/index.php/fairteiler/)