---
title: Lebensmittelanbau
menu: lebensmittelanbau
image_align: left
published: true
---

## Lebensmittelanbau

Es gibt in Neu-Isenburg mehrere Gemeinschaftgärten so zum Beispiel im Stadtteilzentrum West und am Dreiherrnsteinplatz. Letzterer wird durch den Kindergarten vor Ort betreut.

[Garten zum Mitmachen • Stadtpost Neu-Isenburg](https://www.stadtpost.de/stadtpost-neu-isenburg/garten-mitmachen-id14225.html)

[Zaghafter Start am Hochbeet • Stadtpost Neu-Isenburg](https://www.stadtpost.de/stadtpost-neu-isenburg/zaghafter-start-hochbeet-id75556.html)

Zudem wurde durch die städtische Wohnungsbaugesellschaft in Zusammenarbeit mit den Bewohnern in den unterschiedlichen Liegenschaften Gemeinschaftsgärten angelegt.

[Gemeinschaftsgarten in der Richard-Wagner-Straße](https://neu-isenburg.de/buergerservice/rathauspresse/pressemitteilungen/pressemitteilung/gemeinschaftsgarten-in-der-richard-wagner-strasse/)
