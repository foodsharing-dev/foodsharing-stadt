---
title: Info Stände
menu: info_staende
image_align: left
published: true
---

## Info Stände

Wir haben 2022 am Cleanup Day teilgenommen und konnten hier neben dem sammeln von Müll auch über Lebensmittelverschwendung aufklären.

Weiterhin planen wir derzeit für Herbst eine Veranstaltung speziell für Senioren in Zusammenarbeit mit dem örtlichen VdK (der Termin im Mai musste leider seitens des VdK verschoben werden).

