---
title: Fairteiler
menu: fairteiler
image_align: left
published: true 
---

## Fairteiler

Wir haben derzeit einen Fairteiler in Neu-Isenburg. Hier der entsprechende Link:
<a href="https://foodsharing.de/?page=fairteiler&bid=3869&sub=ft&id=2438">Fairteiler</a>


Zudem hängt am Fairteiler ein QR-Code für die dazugehörige WhatsApp Gruppe.

Außerdem haben wir eine foodsharing Gruppe auf facebook welches es allen ermöglicht seine übrigen Lebensmittel zu fairteilen.

Foodsharing Neu-Isenburg | Facebook
https://www.facebook.com/groups/2041006006138463
