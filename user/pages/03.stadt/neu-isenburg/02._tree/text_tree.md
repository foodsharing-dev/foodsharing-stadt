---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Neu Isenburg

Der foodsharing Bezirk Landkreis Offenbach West, zu welchem Neu-Isenburg gehört, hat sich im September 2021 vom Bezirk Offenbach abgespalten. 
- Anzahl der Kooperationen: 59 im gesamten Bezirk
- Anzahl der foodsaver\*Innen: 88
- Anzahl der aktiven Foodsaver\*Innen über das Retten hinaus: ca. 80%
- Besonderes: Gemeinnütziger Verein (foodsharing Landkreis Offenbach West e.V.)


(Stand: Juni 2023 )
