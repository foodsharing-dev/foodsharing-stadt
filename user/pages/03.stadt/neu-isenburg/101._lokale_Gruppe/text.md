---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
image_align: right
published: true
---

## Lokale foodsharing Gruppe

Neu-Isenburg mit seinen knapp 40.000 EW gehört zum foodsharing Bezirk Landkreis Offenbach West, welcher sich im September 2021 gegründet hat. 

- 21 aktive Kooperationen in Neu-Isenburg
- 36 aktive Foodsaver\*Innen in Neu-Isenburg
- 30 Foodsaver\*Innen in Neu-Isenburg engagieren sich über das Retten hinaus zum Beispiel bei Veranstaltungen, AGs oder der Betreuung von Fairteilern.
