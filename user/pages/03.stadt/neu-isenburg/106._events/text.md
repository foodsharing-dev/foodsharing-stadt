---
title: Events
menu: events
image_align: left
published: true
---

## Events

Derzeit befinden wir uns den letzten Zügen der Vorbereitung einer Infoveranstaltung für Senioren in Zusammenarbeit mit dem örtlichen VdK (der Termin im Mai musste leider seitens des VdK verschoben werden).

Außerdem haben wir uns im Rahmen des World cleanup days mit einer Aktion im Westen Neu-Isenburgs beteiligt. Hierfür haben wir mit den örtlichen Dienstleistungsbetrieben der Stadt organisiert, dass wir Zangen, Handschuhe und Müllsäcke gestellt bekommen und die Stadt die Entsorgung übernimmt. Wir haben die Werbung und den Infostand und alles rund um die Organisation übernommen (September 2022).