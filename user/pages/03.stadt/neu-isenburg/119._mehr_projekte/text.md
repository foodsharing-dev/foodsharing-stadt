---
title: Weitere lokale Initiativen
menu: mehr_projekte
image_align: right
published: true
---

## Weitere lokale Initiativen

Die örtliche Speisekammer rettet als Pendant zur Tafel Lebensmittel und verteilt diese an Bedürftige. Wir arbeiten mit dieser in enger Abstimmung zusammen und  lassen ihr immer den Vortritt und unterstützen sie wo wir können.

[Die Speisekammer -Speisekammer Neu Isenburg](https://www.diespeisekammer.com/)

Ein weiteres Herzensprojekt ist Give life a hope mit dem wir wann immer möglich zusammenarbeiten. Dieses Projekt unterstützt mittellose Familien in Gambia mit Lebensmitteln. Hier konnten wir schon oft mit Trockenware und vor allem Babyprodukten aus unseren Rettungen unterstützen.

[Give Life a Hope](https://givelifeahope.com/)
