---
title: Bildungsangebot
menu: bildungsangebot
image_align: right
published: true
---

## Bildungsangebot

Eine Veranstaltung für Senioren ist für Herbst in Zusammenarbeit mit dem örtlichen VdK geplant.

Weiterhin stehen wir mit der Kirche in Verbindung um hier gemeinsame Kochveranstaltungen und Workshops für verschiedene Altersgruppen anzubieten.

Mitte Mai 2023 fand zudem ein Treffen mit dem Bürgermeister statt, um auch das Angebot auf Schulen im Rahmen von Projektwochen anzubieten.

Wir hoffen zeitnah aktiv zu werden, da viele Themen bereits in den letzten Phase der Vorbereitung sind.

Kontakt:

<Bildung.LK.Offenbach-West@foodsharing.network>
