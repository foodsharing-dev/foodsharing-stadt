---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
image_align: right
published: true
---

## Öffentliche Lebensmittelrettung

An Fastnacht wird traditionell in Neu-Isenburg am Lumpenmontag (Rosenmontag) Linsensuppe gekocht und kostenlos an die Bürger der Stadt verteilt. Im Februar 2023 blieben bei dieser traditionellen Veranstaltung rund 150l Linsensuppe übrig, welche durch foodsharing gerettet werden konnten.

Die größte Herausforderung war, dass die Suppe in großen Thermophoren von uns abgeholt wurden und wir diese anschließend portionieren mussten. Unsere Abholer\*Innnen brachten hierfür Töpfe, Schüsseln und Tupper mit und so schafften wir es, die gesamte Suppe zu retten und zu verteilen.
