---
title: Info Stände
menu: info_staende
published: true
image_align: left
---

## Info Stände

In Diez gibt es regelmäßig Infostände. Egal ob bei Fairteilungen, auf Märkten oder bei Fridays for Future. Wir freuen uns über tolle Gespräche und interessierte Bürger. 
Wir verteilen gemeinsam mit der Flüchtlingsinitiative Willkommenskreis Diez wöchentlich gerettete Lebensmittel und betreiben dabei natürlich Aufklärungsarbeit. 
Hier im Gespräch Rebecca Lefèvre, Anne Olschewski (foodsharing.de), Ramona Wiese (Stadtrat Diez) und ein Vertreter der Presse am Tag der offenen Tür im Willkommenskreis Diez. 
Außerdem unterstützen wir mit foodsharing.de regelmäßig Fridays for Future Diez und haben einen Stand am Ziel bzw. Ende der Demo. (Dies ist während der Corona-Krise leider nicht erlaubt.) 
Auch auf dem Diezer Frühlingsmarkt hatten wir einen Infostand und haben die Besucher über Lebensmittelverschwendung aufgeklärt. 
