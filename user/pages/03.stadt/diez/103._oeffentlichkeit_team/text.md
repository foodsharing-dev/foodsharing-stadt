---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: right
---

## Öffentlichkeitsarbeit Team

Durch die sozialen Medien und auch durch die Öffentlichkeitsarbeit des Willkommenskreises hat foodsharing in Diez einen sehr hohen Bekanntheitsgrad. 

Hier eine Reportage von TV Mittelrhein über die Aktivitäten des Willkommenskreises in Verbindung mit foodsharing: <a href="https://www.youtube.com/watch?=9&v=pRoJlZ3QGjw">Video</a>

Die Ehrenamtlichen wurden von Landrat Puchtler und der rheinland-pfälzischen Ministerpräsidentin Malu Dreyer in Diez im Willkommenskreis ausgezeichnet. Malu Dreyer berichtete uns, dass sie selbst foodsaverin ist und regelmäßig Lebensmittel aus ihrem örtlichen Fairteiler holt. Sie befürwortet die Aktivitäten in Diez sehr. 

