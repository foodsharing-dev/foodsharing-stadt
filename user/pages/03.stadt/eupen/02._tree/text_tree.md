---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_1_4.png        


          
    
---

Kurzbeschreibung von Eupen
- foodsharing Bezirk seit Juni 2018
- Anzahl Kooperationen: 4
- Art der Kooperation: Wir bauen erste Kooperationen auf und holen noch sehr unregelmäßig bei 4 Betrieben ab. 2 Bäckereien und 2  Lebensmittelgeschäften (Night-Shops).
- Besonderheiten: Bisher laufen fast alle Interaktionen über die Facebook Gruppe. Es ist uns noch nicht gelungen, Menschen einzubeziehen, die nicht auf sozialen Netzwerken unterwegs sind. Wir haben super Beziehungen zur Presse und haben regelmäßig eine gute Berichterstattung (TV, Radio, Zeitung). Die Initiative ist sehr schnell bekannt geworden und super positiv aufgefasst worden.

Besucht die eigene [Website von foodsharing Ostbelgien](https://foodsharing-ostbelgien.jimdosite.com)


(Stand: Dezember 2019)
