---
title: Öffentlichkeitsarbeit Team
menu: oeffentlichkeit_team
image_align: right
published: true
---

## Öffentlichkeitsarbeit Team


Kontakt: [pr-kiel@foodsharing.network](mailto:pr-kiel@foodsharing.network)

Ziele:
- mehr Sichtbarkeit von foodsharing Kiel in der Öffentlichkeit
- mehr teilnehmende Betriebe
- mehr regelmäßig rettende Foodsaver\*innen


Wir nehmen an unterschiedlichen Veranstaltungen im Kieler Raum teil,
bei denen Nachhaltigkeit ein Thema ist. Wir organisieren selbst „Schnippelparties“, bei denen über nachhaltige Lebensmittelnutzung und Kochen mit „abgelaufenen“ oder nicht mehr „schönen“ Lebensmitteln informiert und dies auch gleich praktisch umgesetzt wird.