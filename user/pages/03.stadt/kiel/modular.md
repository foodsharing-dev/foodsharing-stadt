---
title: Kiel
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 54.322869
        lng: 10.135531
    status: approved

content:
    items: @self.modular
---
