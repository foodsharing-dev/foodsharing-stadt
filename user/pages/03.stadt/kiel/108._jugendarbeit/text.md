---
title: Jugendarbeit
menu: jugendarbeit
image_align: right
published: true
---

## Jugendarbeit

- Alte Mu Start Ups
- Christian-Albrechts-Universität Studien Gruppen: @\_rettich\_ und @schmausundplausch (Instagram), siehe  [Frucht Weitere lokale Initiativen](#mehr_projekte)
- AGs / Greenteams an verschiedenen Schulen

Zuletzt wurde „We've got the chance to change“ der Max Planck Schule
mit dem Umweltpreis der Stadtwerke ausgezeichnet.