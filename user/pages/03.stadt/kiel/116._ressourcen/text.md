---
title: Ressourcen der öffentlichen Hand
menu: ressourcen
image_align: right
published: true
---

## Ressourcen der öffentlichen Hand

Zusammenarbeit mit den AnNas (Anlaufstelle Nachbarschaftstreff)
**Die AnNas sind teils städtisch.**

Kirchengemeinden:
- Jakobi-Gemeinde
- Matthias-Claudius-Gemeinde Suchsdorf


Im Rahmen der Zusammenarbeit mit foodsharing werden Informationen über die Medien der Landeshauptstadt geteilt (Webseite, Social-Media, Newsletter), aber auch Räume können in Kooperation
genutzt werden (z.B. das Nachhaltigkeitszentrum).