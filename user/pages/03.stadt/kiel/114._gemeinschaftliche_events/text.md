---
title: Gemeinschaftliche Events
menu: gemeinschaftliche_events
image_align: left
published: true
---

## Gemeinschaftliche Events

- Beteiligung am Arbeitskreis Zero Waste
- guter und regelmäßiger Austausch mit dem Umweltschutzamt, Nachhaltigkeitszentrum Stadt Kiel
- Mehrfache Teilnahme an der jährlichen Kieler Klimawoche

Die Stadt Kiel ist bereits Zero.Waste.City und hat ein Konzept geschrieben. Im Zuge dessen wurden einerseits [Flyer](https://www.kiel.de/de/umwelt_verkehr/zerowaste/_dokumente_zerowaste/LHK_ZW_Flyer_Lebensmittelverschwendung_DIN_L_Wickelfalz_webUA_v01f.pdf) und andererseits [Internetseiten zum Thema Lebensmittel-Wertschätzung](https://www.kiel.de/de/umwelt_verkehr/zerowaste/haushalte/lebensmittel_retten.php) erstellt.

Die Landeshauptstadt Kiel bietet die Teilnahme an Nachhaltigkeitsveranstaltungen wie z.B. dem jährlichen Mobilitätsfest an, stellt dabei Zelt und Bänke als Ausrüstung und bewirbt die Veranstaltung.