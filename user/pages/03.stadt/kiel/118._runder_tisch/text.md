---
title: Runder Tisch
menu: runder_tisch
image_align: left
published: true
---

## Runder Tisch

- Beteiligung am Arbeitskreis Zero Waste guter und regelmäßiger Austausch mit dem Umweltschutzamt
- Der Ernährungsrat Kiel wurde 2018 gegründet, im Moment gibt es eine personelle Umstrukturierung, so dass die Treffen unregelmäßig stattfinden.