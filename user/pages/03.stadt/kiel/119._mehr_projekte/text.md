---
title: Weitere lokale Initiativen
menu: mehr_projekte
image_align: right
published: true
---

## Weitere lokale Initiativen

- [„gemeinsam Auftischen“](https://www.boell-sh.de/de/gemeinsam-auftischen) hatte im Juni 2024 die Kickoff Veranstaltung. Mit 30 Teilnehmenden aus ganz Schleswif-Holstein und Hamburg (Böllstiftung)
- Studentische Projekte, die mit Foodsharing Kiel e.V. zusammenarbeiten:
  - BUND Jugendgruppe
  - „Schmaus und Plausch“, siehe [Artikel im Hempels](https://www.hempels-sh.de/fileadmin/Fileuploads/user_upload/magazin%20pdfs/2023/Hempels_328.pdf) und [Instagram @schmausundplausch](https://www.instagram.com/schmausundplausch/) 
  - „RettIch“, siehe Instagram @\_rettich\_
- [Cocina - Food Coworking Kitchen](https://www.cocina-kiel.de/)
- [Kieler Tafel](https://www.tafelkiel.de/)
- [Zero Waste Kiel e.V.](https://www.tafelkiel.de/)
- [Resteritter](https://resteritter.de/)



