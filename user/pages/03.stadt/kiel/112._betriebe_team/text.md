---
title: Beratungsteam für Betriebe
menu: betriebe_team
image_align: left
published: true
---

## Beratungsteam für Betriebe

Es gibt einerseits eine Akquise-AG, die neue potentielle Betriebe anspricht, um weitere Kooperationen aufzubauen. Dafür wurde eine umfangreiche Akquisemappe erstellt. Das PR-Team und der Gesamtvorstand haben eine eigene Internetseite für den
Verein erstellt: [foodsharing-kiel.org](https://foodsharing-kiel.org/)

Im Moment sind wir dabei einen neuen Flyer zu gestalten, der sich einerseits an zukünftige Kooperationspartner und andererseits an neue Foodsaver*innen richtet.

Kontakt: [Betriebsakquise.Kiel@foodsharing.network](mailto:Betriebsakquise.Kiel@foodsharing.network)

Fortlaufend finden Verhandlungen mit weiteren Betrieben statt, um sie für Kooperationsvereinbarungen zu
gewinnen.


