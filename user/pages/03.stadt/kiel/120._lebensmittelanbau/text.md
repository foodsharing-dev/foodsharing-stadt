---
title: Lebensmittelanbau
menu: lebensmittelanbau
image_align: left
published: true
---

## Lebensmittelanbau

- Hochbeete an der Uni-Bibliothek
- Interkultureller Garten in Gaarden
- Permakultur in der Alten Mu
- Wohnungsgenossenschaft (WBG Kiel-Ost) hat seit diesem Jahr einen Acker im Viertel (Ellerbek/Wellingdorf) angelegt, welcher gemeinschaftlich unter professioneller Anleitung bewirtschaftet wird
- Gemeinschaftsgärten an verschiedenen Schulen (s. Nachhaltigkeitspreis 2022)
- Marktschwärmer 2 Standorte


