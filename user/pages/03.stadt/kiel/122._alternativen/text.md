---
title: Förderung von Alternativen
menu: alternativen
image_align: left
published: true
---

## Förderung von Alternativen

- [Wochenmärkte](https://www.kiel.de/de/kultur_freizeit/maerkte/wochenmarkt.php) werden durch Kochaktionen unterstützt, ausgerichtet
von der Landeshauptstadt Kiel
- ggf. auch Ausrichtung von [Bauernmärkten](https://kiel-sailing-city.de/veranstaltungen/sonntagsoeffnungen/bauern-und-regionalmarkt) in der Innenstadt unter Beteiligung regionaler Vertreiber\*innen: 
- Unterstützung bei der Öffentlichkeitsarbeit und Sichtbarmachen der Angebote (z.B. der Flyer zu fair gehandelten Lebensmitteln, die Zero Waste-Orte)