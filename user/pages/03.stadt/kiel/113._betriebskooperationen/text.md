---
title: Herausragende Betriebskooperationen
menu: betriebskooperationen
image_align: right
published: true
---

## Herausragende Betriebskooperationen


**Örtlicher inhabergeführter Supermarkt**: Salatbar mit aussortierten Salaten und Gemüse; Reduktion der grünen RE-FOOD-
Tonnen: von 9 auf nur noch 4 Tonnen pro Woche innerhalb von 6 Monaten


**Patrizias Backshop**: Vortagsbäckerei. Mit großem Erfolg werden Backwaren aller Art verkauft. Die Reste werden wiederrum von foodsharing Kiel gerettet.

**Café Vielfalt**: In einem sozialen Projekt werden Knödel aus gerettetem Brot zubereitet. Außerdem lernen die Mitarbeitenden dort das Einkochen und Marmeladekochen.

**Resteritter.de**: Aus einem studentischen Projekt an der CAU Kiel entstand das Projekt, der Herstellung von Chutneys und Marmeladen aus übriggebliebenen Lebensmitteln.

**Lille Brauerei**: In einer Brauerei werden [Brotchips aus Malzresten der Bierproduktion](https://brauereichips.de/chips/) hergestellt.


**Studentenwerk**: Das Studentenwerk der Christian Albrechts-Universität Kiel hat den sogenannten [„Zero Waste-Teller“](https://studentenwerk.sh/de/studentenwerk-testet-zero-waste-teller)
entwickelt.