---
title: Fairteiler
menu: fairteiler
image_align: left
published: true
---

## Fairteiler

Die Lebensmittel werden dezentral fairteilt. Es gibt es sehr viele
verschiedene private Abgabestellen. Außerdem verwalten wir eine
WhatsApp-Community mit verschiedenen Stadtteilgruppen und seit Kurzem eine Signalgruppe.
