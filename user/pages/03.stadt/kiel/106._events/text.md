---
title: Events
menu: events
image_align: left
published: true
---

## Events

**Generationsübergreifendes Kochen in der Anna**: Monatlich (mit Sommerpause) findet ein generationenübergreifendes „Mittwochskochen“ in einem Gemeinschaftsraum einer Baugenossenschaft statt, der für diesen Zweck kostenlos genutzt werden kann.

**Schnippelparties**: Etwa alle sechs Wochen wird in wechselnden Stadtteilen eine Schnippelparty durchgeführt, bei der wechselnde Teilnehmende ebenfalls gemeinsam die geretteten Lebensmittel
verarbeiten und Informationen über foodsharing erhalten.