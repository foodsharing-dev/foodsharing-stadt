---
title: Anstellung bei der Stadt
menu: mitarbeit_stadt
image_align: right
published: true
---

## Anstellung bei der Stadt

**Zero.Waste.City.**   
Landeshauptstadt Kiel,  
Umweltschutzamt  
Projekt Zero.Waste: [zerowaste@kiel.de](zerowaste@kiel.de)

**Nachhaltigkeitszentrum Landeshauptstadt Kiel**  
Landeshauptstadt Kiel,  
Nachhaltigkeitszentrum:  
Umweltberatung: [umweltberatung@kiel.de](umweltberatung@kiel.de)
