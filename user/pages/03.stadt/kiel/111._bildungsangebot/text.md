---
title: Bildungsangebot
menu: bildungsangebot
image_align: right
published: true
---

## Bildungsangebot

Wir bieten Schnippelparties an, auf denen mit geretteten Lebensmitteln gekocht wird. In Planung sind spezielle reduzierte
(Salat, Obstsalat) Schnippelparties in Schulen (3./4. Klasse)

So fand im September 2024 eine Schnippelparty in Kronshagen an der Gemeinschaftsschule statt. Angedacht ist eine ähnliche Veranstaltung am Regionalen Bildungszentrum.

Im Rahmen der Europäischen Woche der Abfallvermeidung 2024 findet eine Schnippelparty im Nachhaltigkeitszentrum der Stadt Kiel statt.