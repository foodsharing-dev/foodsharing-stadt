---
title: Öffentlichkeitsarbeit der Stadt
menu: oeffentliche_kommunikation
image_align: left
published: true
---

## Öffentlichkeitsarbeit der Stadt

- Zero Waste-Haushalts-Challenge 2022: Es haben 117 Haushalte teilgenommen mit über 400 Kieler\*innen. Ein thematisierter Bereich war „Lebensmittel & Küche“, bei dem die Teilnehmenden in einer Selbsteinschätzung nach der Challenge berichteten weniger weggeschmissen zu haben. Bericht auf der [Seite der Stadt Kiel](https://www.kiel.de/de/umwelt_verkehr/zerowaste/haushalte/gemeinsam_fuer_ressourcenschutz.php)
- Kampagne und Teilnahme an der Initiative „Städte gegen Food Waste"
- Social Media Kampagnen z.B. zu der Aktionswoche „Zu gut für die Tonne“
- Veranstaltungen und Workshopangebote für Schulen im Rahmen der kommenden Europäischen Woche der Abfallvermeidung 2024. Darunter auch eine Schnippelparty im Nachhaltigkeitszentrum.