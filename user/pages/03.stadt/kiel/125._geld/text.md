---
title: Nachhaltiger Umgang mit Geld
menu: geld
image_align: right
published: true
---

## Nachhaltiger Umgang mit Geld

Die Mitgliedschaft im gemeinnützigen Verein ist kostenlos, obwohl durch die Unfall- und Haftpflicht-Versicherung Kosten entstehen.

Wir arbeiten ausschließlich spendenfinanziert; inklusive Preisgelder. So sind wir auf die freiwillige Unterstützung angewiesen und sehr dankbar dafür, dass wir es bis jetzt geschafft haben.

Durch zwei Kooperationspartner bekommen wir regelmäßig Spenden,
wenn Kunden ihre Pfand-Bons spenden. Wir haben uns für die nachhaltige GLS-Bank entschieden, weil dort unsere Einlagen für den nachhaltigen Lebensmittelanbau verwendet werden. Das passt perfekt zu unseren Prinzipien.

Erfolgreiche Teilnahme an verschiedenen Umweltwettbewerben:
- Umweltpreis der Stadtwerke Kiel 24/7
- 1\. Preis (Publikum) 2019: 3000EUR
- Jurypreis und Impulsförderung 2022: 250EUR
- Impulsförderung 2024: 250EUR

Da wir ein e.V. sind, wird ausgegebenes und eingenommenes Geld mit Belegen dokumentiert und es gibt einen jährlichen Kassenbericht, der auf der Vereinsseite ([foodsharing-kiel.org](https://foodsharing-kiel.org/spenden/)) einsehbar ist.

Die Gründung eines Vereins ermöglichte uns erst viele Kooperationen mit Betriebspartnern, da viele zu Recht nur mit gemeinnützigen Organisationen zusammenarbeiten.


