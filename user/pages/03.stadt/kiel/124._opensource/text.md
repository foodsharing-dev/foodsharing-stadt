---
title: Nutzung von Open-Source Software
menu: opensource
image_align: right
published: true
---

## Nutzung von Open-Source Software

- Nutzung von OpenOffice und LibreOffice, Cloud der Foodsharing.de-
Plattform
- Kommunikation über Plattform und Messenger-Dienst Signal
- Videokonferenzen über BigBlueButton im Wechsel
mit persönlichen Treffen
- Manitu als Server für unsere Vereinsseite foodsharing-kiel.org