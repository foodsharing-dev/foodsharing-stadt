---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png
          
    
---

Kurzbeschreibung von Kiel

- foodsharing Bezirk seit Oktober 2013
- Anzahl Kooperationen: 83 aktive Kooperationen
- gemeinnütziger Verein seit: 19.09.2019. Alle Mitglieder sind unfall- und haftpflichtversichert
- Besonderes: Kiel ist die Landeshauptstadt des nördlichsten Bundeslandes Schleswig- Holstein und hat etwa 250.000 Einwohner. Der Foodsharing Bezirk geht zum Teil ein bisschen über die Stadtgrenze hinaus. Kiel umschließt die Förde, einen Arm der Ostsee, und ist dadurch stark unterteilt. Lebensmittel werden aus Supermärkten, Cafés, Imbissen, Mensen, Bäckereien, Discountern und teilweise bei städtischen Veranstaltungen gerettet.


(Stand: November 2024)