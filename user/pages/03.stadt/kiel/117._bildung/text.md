---
title: Integrierte Bildungsarbeit
menu: bildung
image_align: right
published: true
---

## Integrierte Bildungsarbeit

Die Landeshauptstadt Kiel engagiert sich für die Wertschätzung von Lebensmittel, indem Veranstaltungen angeboten werden. Zum Beispiel im Rahmen des nachhaltigen Mittwochs im Nachhaltigkeitszentrum, bei der Klimawoche 2023 oder auch durch ein Angebot von Workshops für Schulen (Zero Waste Walk). Angebote finden mindestens einmal im Jahr statt.

- Teilnahme an den Nachhaltigkeits_Messen im Regionalen Bildungszentrum (RBZ)
- Zero Waste Teller / Christian-Albrechts-Universität Kiel.
- Rettungen an den Schulzentren BZM und RBZ
- Kita Einstein
- Kita Lollipop




