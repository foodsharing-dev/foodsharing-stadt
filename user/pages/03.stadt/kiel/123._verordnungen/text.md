---
title: Verordungen der öffentlichen Hand
menu: verordnungen
image_align: right
published: true
---

## Verordungen der öffentlichen Hand

[Zero Waste-Konzept](https://www.kiel.de/de/umwelt_verkehr/zerowaste/_dokumente_zerowaste/zerowaste_kiel_konzept.pdf) mit den Maßnahmen:

- HA-003: Förderung unverpackter, regionaler Lebensmittel
- ÖV-009: Lebensmittelabfälle in städtischen Organisationseinheiten messen
- BE-002: Abfallfreie Mensa
- EV-002: Verpflichtung von Foodsharing Angeboten auf Events

Seit 2016 gibt es in der Landeshauptstadtden lokalen Verein Zero Waste Kiel e.V., der Mitglied im internationalen Netzwerk Zero Waste Europe ist und angeregt hat,dass die Landeshauptstadt eine
Zero.Waste.City werden könnte. Zudem verfolgt unsere Partnerstadt San Francisco bereits seit vielen Jahren eine Zero Waste-Strategie und hat damit schon große Erfolge in der Abfallwirtschaft erzielt. Aus diesen Gründen lag der Gedanke, Zero.Waste.City zu werden, nicht fern,und als aktive Klimaschutzstadt, die sich seit 2016 auch zu den 17 globalen Zielen für nachhaltige Entwicklung der Agenda
2030 (Sustainable Development Goals, SDGs) bekannt hat, ergänzt das Zero Waste-Vorhaben nun auch ideal die bereits bestehenden Klima- und Umweltaktivitäten der Landeshauptstadt Kiel. Das Zero Waste-Konzept, welches 2020 von der Ratsversammlung
beschlossen wurde und in einem partizipativen Prozess mit Kieler\*innen entwickelt wurde, gilt seitdem als Grundlage für die Umsetzungverschiedener Maßnahmen und Projekte, die zur Abfallvermeidung und zum Ressourcenschutz beitragen. 