---
title: Info Stände
menu: info_staende
image_align: left
published: true
---

## Info Stände

Im vergangenen und in diesem Jahr eine Vielzahl (jeweils mehr als 50) u.a.
- Messe Stadtwerke Kiel
- Kieler Woche (KIWO) Schlosspark 2023,
- Alte MU Winterfest,
- Muddimarkt (auf der KiWo) 2024
- Zero Waste Picknick 2024
- Nette Kieler Ehrenamtmesse 2024

Zielgruppe sind insbesondere Menschen, die bisher unreflektiert Lebensmittel wegwerfen.