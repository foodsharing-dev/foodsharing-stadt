---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
image_align: right
published: right
---

## Lokale foodsharing Gruppe

- aktive Foodsaver\*innen: 612
- gemeinnütziger Verein seit: 19.09.2019. Alle Mitglieder sind unfall- und haftpflichtversichert
- [Website foodsharing Kiel e.V.](https://foodsharing-kiel.org/)