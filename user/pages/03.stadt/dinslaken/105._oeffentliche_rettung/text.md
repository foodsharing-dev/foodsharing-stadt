---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
image_align: left
published: true
---

## Öffentliche Lebensmittelrettung

2021 konnten wir zum Abschluss des Weihnachtsmarktes in der Stadt Lebensmittel an einigen Ständen retten.
