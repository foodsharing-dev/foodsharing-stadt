---
title: Info Stände
menu: info_staende
image_align: left
published: true
---

## Info Stände
Auf den DIN-Tagen, dem alljährlichen Stadtfest, waren wir im Sommer 2022 vertreten. Im Burginnenhof im fairen KulturCafé haben wir an unserem Infostand mit vielen Menschen gesprochen und gerettete Lebensmittel fairteilt.

Auch am Tag der Möglichkeiten (organisiert vom Verein ach so) waren wir eine „Station“, konnten viele Gespräche über Lebensmittelverschwendung führen und haben Lebensmittel und auch Blumen fairteilt.

Weitere Stände hatten wir bisher zum Beispiel beim CleanUp, RhineCleanUp, Tischtennisturnier und beim Tag der offenen Tür in der Waldorfschule in Dinslaken.

