---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
image_align: right
published: true
---

## Lokale foodsharing Gruppe

Wir haben zwei Botschafter\*Innen, die foodsharing in Dinslaken repräsentieren und die organisatorischen Aufgaben unter sich aufteilen. Darüber hinaus engagieren sich weitere Personen als Betriebsverantwortliche, als Kontaktpersonen für Abgabestellen oder für die Öffentlichkeitsarbeit. Wir sind mit „foodsharing Dinslaken“ präsent in den sozialen Medien, den ein oder anderen Pressebericht konnten wir ebenfalls platzieren. Wir kooperieren mit den Wunderfindern, einem Verein, der Wohnungslose und Alleinerziehende unterstützt. Weitere Abgabestellen für Lebensmittel haben wir bei den Wegfindern und einer Institution für betreutes Wohnen in Dinslaken. Auch bei uns gilt natürlich „Tafel first“. Wir sind gemeinsam in einigen Betrieben aktiv und helfen gerne aus, wenn die Tafel aufgrund von Urlaub oder Krankheit nicht abholen kann.

Seit Januar 2023 haben wir auch ein Meldeteam, das bei Bedarf dabei helfen soll, Missverständnisse untereinander fair aufzuklären.

Unsere im Dezember 2022 gewählten Botschafter*innen sind: Ruth Gruner r.gruner@foodsharing.network Sabine Schrade s.schrade@foodsharing.network 
 