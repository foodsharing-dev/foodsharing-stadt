---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---
Dinslaken ist eine Stadt mit rund 70.000 Einwohner\*innen am unteren Niederrhein im Nordwesten des Ruhrgebietes. Foodsharing gibt es hier seit dem Frühjahr 2015. Mittlerweile haben wir 107 Saver\*innen mit ihrem Stammbezirk in Dinslaken. Insgesamt engagieren sich in unserem Bezirk 294 Personen. Bis Januar 2023 wurden rund 42.000 kg Lebensmittel gerettet, die bisherigen 4.036 Rettungseinsätze verteilen sich auf 19 laufende Kooperationen.
Dinslaken ist seit 2009 Fairtrade-Town und arbeitet vernetzt im Rahmen der Lokalen Agenda 21.
Ansprechpartnerin bei der Stadt ist Lucie-Maria Rodemann.
Kontaktiert sie gerne per Mail an: <lucie-maria.rodemann@dinslaken.de> 

[Pressebericht über foodsharing Dinslaken](https://www.nrz.de/region/niederrhein/foodsharing-dinslakener-retten-lebensmittel-vor-der-tonne-id241585722.html)


(Stand: Februar 2024)
