---
title: Dinslaken
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 51.57328406615913
        lng: 6.734467185276564
    status: approved

content:
    items: @self.modular
---
