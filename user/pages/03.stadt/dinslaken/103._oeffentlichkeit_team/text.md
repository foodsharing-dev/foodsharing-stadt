---
title: Öffentlichkeitsarbeit Team
menu: oeffentlichkeit_team
image_align: left
published: true
---

## Öffentlichkeitsarbeit Team

Gemeinsame Öffentlichkeitsarbeit stellen wir regelmäßig mit dem Verein „ach so“ auf die Beine. Der Verein trifft sich zum einen quartalsweise zu einem Stammtisch in der Zechenwerkstatt in Dinslaken. An den Abenden sind wir immer mit einem Stand und geretteten Lebensmitteln vertreten.
Auch bei regelmäßigen Müllsammel-Aktionen des Vereins, den Clean-Ups, sind wir mit geretteten Lebensmitteln vor Ort, informieren die Teilnehmenden und fairteilen Lebensmittel.

Ansprechpartnerin ist Anne Doemen
a.doemen@foodsharing.network

Unsere Kanäle sind facebook und Instagram. Schaut doch mal vorbei:
[facebook](https://www.facebook.com/foodsharing.dinslaken/)
[Instagram](https://instagram.com/foodsharing_dinslaken?igshid=YmMyMTA2M2Y=) 


Verein ach so
ach so e.V. – Initiative für bewusstes Leben in Dinslaken erreicht ihr unter dieser [Webseite](https://www.achso-dinslaken.de)

