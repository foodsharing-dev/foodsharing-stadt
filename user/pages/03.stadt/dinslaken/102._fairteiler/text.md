---
title: Fairteiler
menu: fairteiler
image_align: left
published: true
---

## Fairteiler

Der Fairteiler ist öffentlich zugänglich, immer zu den Öffnungszeiten des Museums Voswinckelshof: dienstags bis sonntags, 14.00 bis 18.00 Uhr. Während der Öffnungszeiten können alle Gäste die Regale füllen und natürlich selber Lebensmittel kostenlos mitnehmen. Sabine Schrade und Ruth Gruner, Botschafterinnen von foodsharing in Dinslaken, fassen es so zusammen: „Sie haben zu viele Äpfel im Garten, die Sie gar nicht verbrauchen können? Sie fahren in den Urlaub und der Kühlschrank zu Hause ist noch voll? Dann bestücken Sie doch den ‚Fairteiler‘ damit!“

Den Pressebericht zur Eröffnung des Fairteilers findet ihr hier: [Pressebreicht](https://www.dinslaken.de/stadt-buergerservice/aktuelles/dinslaken-auf-dem-weg-zur-foodsharing-stadt#)


Bildunterschrift: Bürgermeisterin Michaela Eislöffel, Cordula Hamelmann (Museum), Christoph Hügel (Nachhaltige Entwicklung, Stadtverwaltung), Museumsleiter Danny Könnicke, Lucie-Maria Rodemann (Nachhaltige Entwicklung, Stadtverwaltung), Ruth Gruner (Foodsharing)