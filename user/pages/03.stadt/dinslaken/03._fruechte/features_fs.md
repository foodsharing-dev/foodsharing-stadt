---
title: Fruechte
menu: Fruechte
class: small

features:
     - header: lokale foodsharing Gruppe
       url: "#lokale_gruppe"
       icon: /user/images/icons/lokale_gruppe.jpg

     - header: Fairteiler
       url: "#fairteiler"
       icon: /user/images/icons/fairteiler.jpg

     - header: Öffentlichkeitsarbeit Team
       url: "#oeffentlichkeit_team"
       icon: /user/images/icons/oeffentlichkeit_team.jpg

     - header: Info Stände
       url: "#info_staende"
       icon:  /user/images/icons/info_staende.jpg      

     - header: Öffentliche Lebensmittelrettung
       url: "#oeffentliche_rettung"
       icon: /user/images/icons/oeffentliche_rettung.jpg

    # - header: Events
    #   url: "#events"
    #   icon: /user/images/icons/events.jpg
    
    # - header: foodsharing in Zeiten von Corona
    #   url: "#corona"
    #   icon: /user/images/icons/corona.jpg 

    # - header: Jugendarbeit
    #   url: "#jugendarbeit"
    #   icon: /user/images/icons/jugendarbeit.jpg

    # - header: Politische Bündnissen und Demos
    #   url: "#politisches"
    #   icon: /user/images/icons/politisches.jpg
      
    # - header: Gemeinschaftlicher Fairteiler
    #   url: "#gemeinschaftlicher_fairteiler"
    #   icon: /user/images/icons/gemeinschaftlicher_fairteiler.jpg

    # - header: Bildungsangebot
    #   url: "#bildungsangebot"
    #   icon: /user/images/icons/bildungsangebot.jpg

    # - header: Beratungsteam für Betriebe
    #   url: "#betriebe_team"
    #   icon: /user/images/icons/betriebe_team.jpg

    # - header: Herausragende Betriebskooperationen
    #   url: "#betriebskooperationen"
    #   icon: /user/images/icons/betriebskooperationen.jpg

    # - header: Gemeinschaftliche Events
    #   url: "#gemeinschaftliche_events"
    #   icon: /user/images/icons/gemeinschaftliche_events.jpg

    # - header: Öffentlichkeitsarbeit der Stadt
    #   url: "#oeffentliche_kommunikation"
    #   icon: /user/images/icons/oeffentliche_kommunikation.jpg

    # - header: Ressourcen der öffentlichen Hand
    #   url: "#ressourcen"
    #   icon: /user/images/icons/ressourcen.jpg

    # - header: Integrierte Bildungsarbeit
    #   url: "#bildung"
    #   icon: /user/images/icons/bildung.jpg

    # - header: Runder Tisch
    #   url: "#runder_tisch"
    #   icon: /user/images/icons/runder_tisch.jpg

     - header: Weitere lokale Initiativen
       url: "#mehr_projekte"
       icon: /user/images/icons/mehr_projekte.jpg

    # - header: Lebensmittelanbau
    #   url: "#lebensmittelanbau"
    #   icon: /user/images/icons/lebensmittelanbau.jpg

    # - header: Anstellung bei der Stadt
    #   url: "#mitarbeit_stadt"
    #   icon: /user/images/icons/mitarbeit_stadt.jpg

    # - header: Förderung von Alternativen
    #   url: "#alternativen"
    #   icon: /user/images/icons/alternativen.jpg

    # - header: Verordungen der öffentlichen Hand
    #   url: "#verordnungen"
    #   icon: /user/images/icons/verordnungen.jpg

    # - header: Nutzung von Open-Source Software
    #   url: "#opensource"
    #   icon: /user/images/icons/opensource.jpg

    # - header: Nachhaltiger Umgang mit Geld
    #   url: "#geld"
    #   icon: /user/images/icons/geld.jpg         
    
---

