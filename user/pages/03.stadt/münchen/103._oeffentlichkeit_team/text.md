---
title: Öffentlichkeitsarbeit Team
menu: oeffentlichkeit_team
image_align: right
published: true
---

## Öffentlichkeitsarbeit Team

"Ressort Promotion München" als Arbeitsgruppe auf foodsharing.de mit insgesamt 51 Mitgliedern.

Auswahl an Events in 2023
- DEHOGA München Jahreshauptversammlung: 10.05.2023
- Junge Klimakonferenz (LMU): 08.10.2023
- [Münchner Umweltpreis](https://ru.muenchen.de/2023/212/Muenchner-Umweltpreis-2023-verliehen-109930): 06.11.2023