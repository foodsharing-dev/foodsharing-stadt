---
title: Nutzung von Open-Source Software
menu: opensource
image_align: right
published: true
---

## Nutzung von Open-Source Software

- Nutzung der Videokonferenzplattform „BigBlueButton“ z.B. für die Mitgliederversammlung
- Nutzung einer Cloud zur Ablage von Informationsmaterialien, z.B. für Botschafter*innen