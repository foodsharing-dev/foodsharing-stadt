---
title: Events
menu: events
image_align: left
published: true
---

## Events

Auswahl an öffentlichen Events in 2023

- 5x Foodsharing Dinner in 2023 (25.02.2023 / 22.04.2023 / 24.06.2023 / 26.08.2023 / 28.10.2023)
- 1x Foosharing Winterparty (16.12.2023)