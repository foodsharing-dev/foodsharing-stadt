---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
image_align: right
published: true
---

## Lokale foodsharing Gruppe

- Kontakt: [muenchen@foodsharing.network](mailto:muenchen@foodsharing.network)
Alle weiteren Infos auf der [Website](https://www.foodsharing-muenchen.de/)