---
title: Nachhaltiger Umgang mit Geld
menu: geld
image_align: right
published: true
---

## Nachhaltiger Umgang mit Geld


Vereinskonto bei „GLS Bank“
- Bewusste Wahl dieser Bank aufgrund von:
Investition in nachhaltige Unternehmen, Windkraftanlagen, nachhaltige Wohnbauprojekte,
Hersteller von Naturkosmetik, Öko-Textilien, Öko-Landwirtschaft


Trotz einer günstigeren Alternative hat sich die Mitgliederversammlung im Jahr 2023 dazu
entschieden, bei der GLS Bank aufgrund ihrer nachhaltigen Investitionen zu bleiben.