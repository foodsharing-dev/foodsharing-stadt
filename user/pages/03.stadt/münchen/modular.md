---
title: München
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 48.1373
        lng: 11.5759
    status: pending

content:
    items: @self.modular
---
