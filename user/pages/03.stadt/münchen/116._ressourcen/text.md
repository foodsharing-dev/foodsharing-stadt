---
title: Ressourcen der öffentlichen Hand
menu: ressourcen
image_align: right
published: true
---

## Ressourcen der öffentlichen Hand

**Gewinn des Münchner Umweltpreis in 2023**
Mit dem Münchner Umweltpreis wird außerordentliches Engagement für
Umweltschutz in der Stadt München gewürdigt. Klima- und
Umweltschutzreferentin Christine Kugler hat im Alten Rathaus die Preise 2023
überreicht. Alle Preisträger\*innen wurden mit einer Urkunde und einer
Siegertrophäe ausgezeichnet. Insgesamt wurden Preisgelder in Höhe von 10.000
Euro vergeben. Eine zehnköpfige Jury hatte im Vorfeld aus 22 Bewerbungen die
Preisträger\*innen ausgewählt. Den ersten Platz hat der Foodsharing München
e.V. mit seinem Projekt „Lebensmittel retten“ belegt. Foodsharing München rettet
Lebensmittel aus privaten Haushalten und Betrieben und sagt damit der
Lebensmittelverschwendung den Kampf an.