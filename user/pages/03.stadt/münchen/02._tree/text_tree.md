---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von München

- 02. Juli 2017: Gründung als „Foodsharing München mit angrenzenden Kreisen e.V.“ mit den Bezirken München, Dachau, Fürstenfeldbruck und Starnberg
- 25. Juli 2021: Umbenennung in „Foodsharing München e.V.“, aufgrund fortschreitenden Wachstums wurden die angrenzenden Kreise entkoppelt
- Zahl der Vereinsmitglieder: 2.000 (Stand: 01.10.2022)
- Zahl aktiver Kooperationen: 140 (Stand: 01.09.2022)
- Anzahl der aktiven Foodsavenden: 487 (Stand: 31.12.2022)
- Anzahl der angebotenen Essenskörbe: 10.596 (Stand: 31.12.2022)
- Aktive Fairteiler/Abgabestellen: 5 (Stand: 09.12.2023)
- Website: [https://www.foodsharing-muenchen.de/](https://www.foodsharing-muenchen.de/)

(Stand: Februar 2025 )
