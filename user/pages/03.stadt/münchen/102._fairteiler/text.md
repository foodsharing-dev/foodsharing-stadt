---
title: Fairteiler
menu: fairteiler
image_align: left
published: true
---

## Fairteiler

5 Abgabestellen im Bezirk München

Standorte der Abgabestellen
- Abgabestelle Bavariapark | IG Feuerwache (Ganghoferstraße 41, 80339 München)
- Abgabestelle Haidhausen | HEI (Wörthstraße 42 , 81667 München)
- Abgabestelle Leonrodplatz | Brauchbar (Dachauer Straße 114, 80636 München)
- Abgabestelle Moosach | diakonia inhouse (Dachauer Straße 192, 80992 München)
- Abgabestelle Schwanthalerhöhe | EineWeltHaus (Schwanthalerstraße 80 RGB, 80336 München)
