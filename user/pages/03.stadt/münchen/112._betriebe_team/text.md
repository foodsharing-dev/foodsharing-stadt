---
title: Beratungsteam für Betriebe
menu: betriebe_team
image_align: left
published: false
---

## Beratungsteam für Betriebe

"Ressort Kooperationen München" als Arbeitsgruppe auf foodsharing.de mit insgesamt 9 Mitgliedern.

Kooperationstreffen: monatlich am 20.
In diesem Treffen wird über die mögliche Ansprache neuer Betriebe gesprochen. Ansprachen erfolgen im Bezirk München ausschließlich über dieses Ressort.