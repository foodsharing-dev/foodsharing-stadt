---
title: Info Stände
menu: info_staende
image_align: left
published: true
---

## Info Stände

Auswahl an Infoständen in 2023
- Rohvolution & Veggienale Messe: 29. & 30.04.2023
- Zamanand Festival: 13. & 14.05.2023
- Nachtbiomarkt Neubiberg: 20.07.2023
- Tierheimfest: 22.07.2023
- Oben Ohne Festival: 22.07.2023
- Klima Dult: 23.07.2023
- Veggie World: 11. & 12.11.2023

