---
title: Runder Tisch
menu: runder_tisch
image_align: left
published: true
---

## Runder Tisch

Teilnahme an „Runder Tisch - Vermeidung von Lebensmittelabfällen“ am 06.07.2023 (Community Kitchen & Landeshauptstadt München)