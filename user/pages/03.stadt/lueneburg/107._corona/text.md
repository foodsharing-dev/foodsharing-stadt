---
title: foodsharing in Zeiten von Corona
menu: corona
published: false
image_align: left
---

## foodsharing in Zeiten von Corona
Die COVID-19-Pandemie beeinflusste auch die Tätigkeiten von foodsharing in Lüneburg, traf uns aber weniger stark als andere. So fanden Abholungen bei kooperierenden Betrieben weiterhin statt – allerdings mit geringerer Personenzahl. Die Fairteiler waren weiterhin geöffnet – allerdings durften diese nur noch einzeln und mit Maske genutzt werden. Unsere Plena hatten sich in den virtuellen Raum verschoben. In der Pandemie zeigte sich jedoch auch die Stärke der foodsharing-Organisation und des foodsharing-Netzwerks. So gab es teilweise anhaltende private Unterstützung von besonders stark von der Krise getroffenen Personen mit Lebensmitteln; teilweise konnten wir auch für die Tafel einspringen. Die ohnehin angelegte online-Kommunikation und -Organisation erwies sich in der Pandemie auf jeden Fall als Stärke.   
