---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Lüneburg

- foodsharing Bezirk seit Januar 2014
- Aktive Foodsaver\*innen mit mindesten 1 Abholung pro Monat: 130
- Anzahl der Kooperationen: 36 Betriebe (zzgl. vieler Tafelersatzabholungen in den Tafelferien)
- Art der Kooperationen: Restaurants/Cafés, Kindergärten, Marktstände, Bäckereien, Supermärkte, Tankstellen...
- 7 Lebensmittel und 1 Pflanzenfairteiler (davon 1 in Adendorf, 1 in Bardowick)
- foodsharing-Stadt seit März 2023
- Besonderes: Für eine mittelgroße Stadt wie Lüneburg ist foodsharing mit 130 aktiven Abholenden pro Monat recht groß. Wir sind eine bunt-gemischte Gruppe, die sich gut ergänzt. Außerdem freuen wir uns 7 Fairteiler zu haben und mehrere Lastenräder und Fahrradanhänger.
- Kontakt: [lueneburg@foodsharing.network](mailto:lueneburg@foodsharing.network)

(Stand von August 2024)





