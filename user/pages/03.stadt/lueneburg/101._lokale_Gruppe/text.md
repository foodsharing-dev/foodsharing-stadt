---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: left
---

## Lokale foodsharing Gruppe

foodsharing Lüneburg gibt es seit Januar 2014 und es wurden Insgesamt schon 436.887  kg Lebensmittel gerettet. 2023 haben jeden Monat mehr 130 Foodsaver*innen in 42 Betrieben abgeholt (Tendenz steigend). Der Bezirk spricht sich mit der Tafel ab, um den Vorrang dieser zu gewährleisten. Monatliches Plenum: Wir treffen uns immer um den 15. des Monats um 19.30 Uhr an wechselnden Orten. Im Sommer oft im Park, im Winter wechselnd im PONS, mosaique oder online. Meist sind wir 5-10 Personen.
