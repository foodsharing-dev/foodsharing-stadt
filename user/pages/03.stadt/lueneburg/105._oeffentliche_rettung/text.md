---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
published: true
image_align: right
---

## Öffentliche Lebensmittelrettung

Jedes Jahr holt die foodsharing Gruppe bei städtischen Veranstaltungen, wie z. B. Weihnachtsmarkt oder Sülfmeistertagen, ab.
