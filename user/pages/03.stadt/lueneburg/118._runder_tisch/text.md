---
title: Runder Tisch
menu: runder_tisch
published: true
image_align: left
---

## Runder Tisch

Der Lüneburger Ernährungsrat hatte 2023 eine Neuorientierungsphase: Es gab offene Treffen, zu denen breit eingeladen wurde, und dabei haben sich mehrere neue Personen dem Aktivenkreis des Ernährungsrats angeschlossen. Im Juni 2023 hat gab es im Rahmen der Mitmach-Region einen Mitmachmonat zum Thema Ernährung, der vom Ernährungsrat koordiniert wurde. Zu den Veranstaltungen gehörten Hof- und Gartenbesuche, gemeinsame Kochevents, ein Vortrag zu Lebensmittelverschwendung, eine Wildkräuterführung, eine Filmvorführung zu solidarischer Landwirtschaft und ein Besuch bei einer Agroforst-Landwirtschaft.