---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: left
---

## Öffentlichkeitsarbeit Team

Das Team der Öffentlichkeitsarbeit in Lüneburg organisiert sich in einem dafür angelegten <a href="https://foodsharing.de/store/47800">Betrieb</a>. So können wir unkompliziert Zuständigkeiten für Infostände, Vorträge und Co. zuteilen. 

Unsere Ziele

- Bewusstsein der Lüneburger*innen für Lebensmittelwertschätzung schaffen
- Breiteres Bewusstsein für Möglichkeiten des Teilens von überschüssigen Lebensmitteln
- Teilnahme an foodsharing
- Private Nutzung des Fairteilers
- Privates Teilen von überschüssigen Lebensmitteln
- Öffentliche Lebensmittelrettungen bei städtisch organisierten Festen
- Öffentliche Bewerbung von foodsharing durch die Hansestadt Lüneburg

Weitere Ideen und Anregungen können gerne an die allgemeine Kontaktadresse geschickt werden: [lueneburg@foodsharing.network](mailto:lueneburg@foodsharing.network)
