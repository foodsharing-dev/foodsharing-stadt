---
title: Anstellung bei der Stadt
menu: mitarbeit_stadt
published: false
image_align: left
---

## Anstellung bei der Stadt
Lüneburg gehört zu dem Projekt Zukunftsstadt. Eines der in diesem Projekt durchgeführten Realexperimente beschäftigt sich mit dem Aufbau eines Ernährungsrats. Ansprechperson ist hier Karin Petersen. [Hier](https://www.lueneburg2030.de/ernaehrungsrat/) findet ihr mehr zum Ernährungsrat in Lüneburg.  
