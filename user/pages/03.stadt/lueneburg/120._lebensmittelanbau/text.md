---
title: Lebensmittelanbau
menu: lebensmittelanbau
published: true
image_align: right
---

## Lebensmittelanbau

In Lüneburg gibt es die solidarische Landwirtschaft [WirGarten](https://lueneburg.wirgarten.com/), die als Genossenschaft ca. 500 Mitglieder hat. Auf 8,23 ha Anbaufläche werden dort ökologisches, saisonales und regionales Gemüse mit dem Konzept regenerativer Landwirtschaft produziert. Mehr Infos zum WirGarten findest du [auf deren Website](https://lueneburg.wirgarten.com/) und in der [Lünepedia](https://www.luenepedia.de/wiki/WirGarten). 
Am Rande des Stadtgebiets befinden sich zudem viele landwirtschaftliche Betriebe, die unter anderem Kartoffeln anbauen. Seit dem Spätsommer 2020 gab es durch foodsharing organisierte Nachernte-Aktionen. Konkrete Infos findest du [auf fodsharing.de](https://foodsharing.de/store/3945). Die Kooperation soll möglichst auf weitere Betriebe und Feldfrüchte ausgeweitet werden. 
2022 hörten wir von 20.000 Fenchelknollen im Umkreis, die aufgrund optischer Mängel nicht vom Handel abgenommen wurden. Auch hier konnten wir einiges direkt vom Feld retten und die Tafel dabei mitversorgen.


