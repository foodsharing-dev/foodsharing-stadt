---
title: Info Stände
menu: info_staende
published: true
image_align: right
---

## Info Stände
Wir sind regelmäßig mit Infoständen bei Veranstaltungen dabei, um mit Interessierten übers Lebensmittelretten zu sprechen. Z. B. auf dem Markt der Möglichkeiten des Klimacamps im Kurpark, beim Stadtteilfest in Kaltenmoor oder beim Klimastreik auf dem Marktplatz. 
Außerdem bemühen wir uns, durch Publikationen und Internet-Präsenz auch für Menschen zugänglich zu sein, die nicht auf der foodsharing-Plattform registriert sind. Man kann uns z. B. auch über eine facebook-Gruppe oder Social Media erreichen. Außerdem findet man mehr Infos bei [Lünepedia](https://www.luenepedia.de/wiki/Foodsharing) und in der [Artikelsammlung zu foodsharing](https://www.luenepedia.de/wiki/Foodsharing#Berichterstattung). 

