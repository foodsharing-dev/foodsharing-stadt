---
title: Ressourcen der öffentlichen Hand
menu: ressourcen
image_align: right
published: true
---

## Ressourcen der öffentlichen Hand

Die Hansestadt Lüneburg stellt eine Garage zur Verfügung. Diese bietet sowohl Platz für Lastenrad und Fahrradreparatur, als auch Materialien für Infostände und Abholungen. 