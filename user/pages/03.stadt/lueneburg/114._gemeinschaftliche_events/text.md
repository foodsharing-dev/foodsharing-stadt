---
title: Gemeinschaftliche Events
menu: gemeinschaftliche_events
published: false
image_align: left
---

## Gemeinschaftliche Events

Im Sommer 2023 gabe es ein foodsharing-Sommerfest im Kurpark mit Mitbring-Buffet und Siebdruck. Zu dem Fest wurden auch Vertreter:innen den Kooperationsorte der Fairteiler eingeladen, um sich mit Essenskörben bei ihnen zu bedanken. 