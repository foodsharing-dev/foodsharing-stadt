---
title: Fairteiler
menu: Fairteiler
published: true
image_align: right
---

## Fairteiler
Es gibt sieben Lebensmittel- und einen Pflanzenfairteiler in Lüneburg. Den Fairteiler in Ritterstraße gibt es schon seit 2015 und den Bethlehem-Fairteiler seit November 2017. 2020 folgten Paul-Gerhardt-Fairteiler und Genezareth-Fairteiler und im Februar 2021 ist der Bockelsberg-Fairteiler dazu gekommen. 2022 kam der Fairteiler in Adendorf dazu, 2023 der in Bardowick und 2024 der Pflanzenfairteiler am KGV Moldenweg.Aktuelles zu den Fairteilern findet ihr auch immer in der <a href="https://www.luenepedia.de/wiki/Foodsharing#Fairteiler">Lünepedia</a>.

• Ritterstraßen-Fairteiler" in der Ritterstraße 3: Hauseingang mit einem großen Regal, mit Kühlschrank. <a href="https://foodsharing.de/?page=fairteiler&sub=ft&bid=158&id=730">Zum Fairteiler</a>

• "Bethlehem-Fairteiler" in der Friedenstraße 8: Holzhütte mit Kühlschrank. <a href="https://foodsharing.de/?page=fairteiler&sub=ft&bid=158&id=985">Zum Fairteiler</a>

• "Paul-Gerhardt-Fairteiler" in der Bunsenstraße 82: Holzhäuschen mit Kühlschrank. <a href="https://foodsharing.de/?page=fairteiler&sub=ft&bid=158&id=1750">Zum Fairteiler</a>

• "Genezareth-Fairteiler", Am Springintgut 6a: Großer Schrank, in der warmen Jahreszeit mit Kühlschrank. <a href="https://foodsharing.de/?page=fairteiler&sub=ft&bid=158&id=1815">Zum Fairteiler</a>

• "Bockelsberg-Fairteiler" in der Wichernstraße 32: Holzhütte mit Kühlschrank. <a href="https://foodsharing.de/?page=fairteiler&sub=ft&bid=158&id=1942">Zum Fairteiler</a>

• "Adendorf-Fairteiler" im Kirchweg in Adendorf an der Emmaus-Kirche: Holzhäuschen mit Kühlschrank. <a href="https://foodsharing.de/?page=fairteiler&bid=158&sub=ft&id=2065">Zum Fairteiler</a> 

• "Domschatz-Fairteiler Bardowick" am Dom: Regale mit Kühlschrank. <a href="https://foodsharing.de/?page=fairteiler&sub=ft&id=2746">Zum Fairteiler</a> 

• "Pflanzenfairteiler KGV Moldenweg" neben dem Vereinsheim des Kleingartenvereins, direkt am Weg: Überdachtes Regal. 
