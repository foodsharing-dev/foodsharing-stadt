---
title: Politische Bündnissen und Demos
menu: politisches
image_align: right
published: true
---

## Politische Bündnissen und Demos

Die foodsharing Gruppe beteiligt sich aktiv an politischen Bündnissen und Demos wie z.B. am Globalen Klimastreik, dem Ernährungsrat sowie der Demo für Demokratie und Menschlichkeit.