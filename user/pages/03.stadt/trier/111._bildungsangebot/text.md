---
title: Bildungsangebot
menu: bildungsangebot
published: true
image_align: right
---

## Bildungsangebot

Immer wieder werden wir zur Mitgestaltung einzelner Unterrichtseinheiten zum Thema „Verantwortlicher Umgang mit Lebensmitteln“ und „Aktiv gegen Lebensmittelverschwendung“ angefragt. Selbst für die jüngsten in den Kindergärten besteht Interesse, sie mit unserer Unterstützung an einen wertschätzenden Umgang mit Lebensmitteln heranzuführen.