---
title: Trier
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 49.75960
        lng: 6.64415
    status: pending

content:
    items: @self.modular
---
