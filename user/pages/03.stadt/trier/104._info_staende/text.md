---
title: Info Stände
menu: info_staende
published: true
image_align: right
---

## Info Stände

**Pflanzentauschbörse**
Regelmäßig am 1. Mai veranstaltet die Initiative Trier im Wandel-Transition Trier e.V. in Kooperation mit dem Grünflächenamt in der Tufa, einem Kultur- und Kommunikationszentrum, eine Pflanzentauschbörse, bei der foodsharing Trier für die Anwesenden ein Angebot aus geretteten Lebensmitteln darbietet - meist Smoothies und getoastetes Brot mit veganem Brotaufstrich. Außerdem werden den ganzen Tag Lebensmittel aus Abholungen der vorangegangenen Tage verschenkt

**Flying grass carpet**
Zehn Tage lang haben zahlreiche Trierer Akteur\*innen im Rahmen des flying grass carpet, der größten mobilen Parklandschaft der Welt, direkt in Trier auf dem Viehmarktplatz das schnöde Betonquader mit einem bunten, unterhaltsamen Kunst-, Kultur- und Sportprogramm in eine grüne Insel des künstlerischen, kreativen und visionären Denkens verwandelt. Auch Foodsharing Trier war dabei.

**20-jähriges Jubiläum der LA 21 Trier**
In diesem Jahr waren wir von der Lokalen Agenda 21 eingeladen, deren 20-jähriges Jubiläum auf dem Viehmarktplatz mitzufeiern. Im Rahmen des Trierer Nachtmarkts haben wir Lebensmittel aus vorangegangenen Rettungen verschenkt, die interessierten Menschen über Foodsharing informiert und unsere Wünsche an die Politik formuliert.

**Weltbürgerfrühstück**
Die Lokale Agenda 21 und das Netzwerk Weltkirche des Dekanats Trier verfolgen mit dem alljährlichen WeltBürgerFrühstück, das seit 2007 stattfindet, das Ziel, die Stadt Trier zur „Hauptstadt des fairen Handels“ zu machen, das Bewusstsein der Bürger\*innen für fairen Handel zu stärken und das weltweit partnerschaftliche Engagement zu fördern. Wir finden, das passt gut zu Foodsharing Trier.