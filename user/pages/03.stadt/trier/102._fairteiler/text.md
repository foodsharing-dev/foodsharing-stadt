---
title: Fairteiler
menu: Fairteiler
published: false
image_align: left
---

## Fairteiler

**Fairteiler am Viehmarkt** 
Der Fairteiler ein Graffiti-verzierter Schrank, der seit dem 22.11. 2014 vorm Simplicissimus am Viehmarktplatz steht. Er ist zu den Öffnungszeiten des „Simpl“ zugänglich (derzeit Montags bis Samstags ab 10 und Sonntags und an Feiertagen ab 13 Uhr).  

**Fairteiler in Trier-Süd** 
Der Fairteiler ist ein hübscher, kleiner Schrank, der unter einem Carport steht. Er befindet sich auf einem Privatgrundstück und ist täglich von 8-22 Uhr frei zugänglich.

- <a href="https://foodsharing.de/?page=fairteiler&bid=157">Alle Fairteiler in Trier</a>
