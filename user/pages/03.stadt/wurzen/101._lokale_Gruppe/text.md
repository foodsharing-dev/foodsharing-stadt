---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
background: 
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver: 29
- AbholerInnen mind. einmal im Monat: ca. 10
- Engagierte über Abholungen hinaus: ca. 10
- Kontakt: <muldentalkreis@foodsharing.network>