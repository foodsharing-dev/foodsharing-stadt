---
title: Fairteiler
menu: Fairteiler
image_align: left
---

## Fairteiler

Wir haben einen Fairteiler, der im zweiten Eingang des Kanthauses zu finden ist. Geöffnet ist er jeden Dienstag zwischen 16 und 18 Uhr unter Betreuung zusammen mit einem Verschenkeladen und einem Makerspace. Es gibt ein Kistenregal und bei Bedarf Kühlboxen, aber keinen Kühlschrank.
