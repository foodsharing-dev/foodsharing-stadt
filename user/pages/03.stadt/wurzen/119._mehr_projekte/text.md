---
title: Weitere lokale Initiativen
menu: mehr_projekte
image_align: left
---

## Weitere lokale Initiativen

Das Kanthaus ist ein Projekthaus in Wurzen. Im Kanthaus passiert sehr viel überregionale Arbeit für foodsharing: Hackweeks zur Verbesserung der Plattform, Vorstandstreffen, Treffen der Festival-Koordinations-AG, Planung des foodsharing Festivals, Materiallager, Internationalisierung und mehr.
Infos zum Kanthaus unter: [https://kanthaus.online](https://kanthaus.online).

