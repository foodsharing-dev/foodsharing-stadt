---
title: Mettmann
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 51.25150
        lng: 6.97671
    status: pending

content:
    items: @self.modular
---
