---
title: Nutzung von Open-Source Software
menu: opensource
image_align: right
published: true
---

## Nutzung von Open-Source Software

Uns wurde durch ein lokales Unternehmen ein entsprechender Server zur Verfügung gestellt auf dem unsere [Homepage](https://foodsharing-lkofw.de/) und damit unser öffentlicher Auftritt läuft.

Mit unseren Vereinsmitgliedern kommunizieren wir über die Plattform von [foodsharing](http://foodsharing.de). Dort nutzen wir das Videokonferenzsystem zum Beispiel für Plenum oder interne Schulung unserer Foodsaver\*innen und Betriebsverantwortlichen.