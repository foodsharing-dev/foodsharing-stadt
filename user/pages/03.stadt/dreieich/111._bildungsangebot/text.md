---
title: Bildungsangebot
menu: bildungsangebot
image_align: right
published: true
---

## Bildungsangebot
Wir werden im August eine Veranstaltung in der Winkelsmühle haben. Diese richtet sich im Rahmen der Nachhaltigkeit an die Ehrenamtlichen der Diakonie. Im Anschluss sind hier weitere Veranstaltungen zum Thema Lebensmittelverarbeitung und -wertschätzung vorgesehen.
Weiterhin haben wir eine aktive Kooperation mit der VHS Dreieich. Hier fanden Ende 2022 und Anfang 2023 die ersten kostenlosen Infoveranstaltungen statt. Diese werden regelmäßig alle halbe Jahr in den Räumlichkeiten der VHS Dreieich stattfinden.

Zusätzlich gibt es eine AG für Bildungsarbeit. Diese bereiten verschiedene Veranstaltungen, Infostände, Workshops und noch vieles mehr vor.

Kontakt: Bildung.LK.Offenbach-West@foodsharing.network
