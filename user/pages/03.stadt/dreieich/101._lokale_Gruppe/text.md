---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
image_align: right
published: true
---

## Lokale foodsharing Gruppe

Es gibt... 
14 aktive Kooperationen in Dreieich
15 aktive Foodsaver\*innen in Dreieich
Außerdem engagieren sich 10 Foodsaver\*innen in Dreiech über das Retten hinaus zum Beispiel bei Veranstaltungen, AGs oder der Betreuung von Fairteilern.
