---
title: Dreiech
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/approved)
foodsharing:
   coordinates:
        lat: 50.0306
        lng: 8.69153
   status: pending

content:
    items: @self.modular

---