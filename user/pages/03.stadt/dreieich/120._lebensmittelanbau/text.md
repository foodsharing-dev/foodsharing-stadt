---
title: Lebensmittelanbau
menu: lebensmittelanbau
image_align: left
published: true
---

## Lebensmittelanbau

Dreieich hat einen [Lehr- und Kräutergarten](https://www.kraeutergarten-dreieich.de/) und betreibt Streuobstwiesen zu Schulungs- und Lehrzwecken.

Ein Weiteres Projekt ist die Kinder- und Jugendfarm [Dreieichhörnchen](https://www.dreieichhoernchen.de/). Diese hat einen eigenen Farmgarten und bringt dort Kindern und Jugendlichen den wertschätzenden Umgang mit Pflanzen und auch Tieren bei.

