---
title: Gemeinschaftlicher Fairteiler
menu: gemeinschaftlicher_fairteiler
image_align: left
published: true
---

## Gemeinschaftlicher Fairteiler
Wir haben derzeit 2 Fairteiler mit öffentlicher Beteiligung.

Fairteiler Stadtteilzentrum Sprendlingen:
Unser ältester Fairteiler befindet sich im Stadtteilzentrum (städtisch) in Sprendlingen. Ein Freiwilliger hat uns hier einen großen Unterstand mit Regalen gebaut. Derzeit befinden wir uns noch in Klärung, ob hier ggf. noch ein Kühlschrank eingebaut wird. Die Reinigung und Pflege übernehmen die Foodsaver.

Der Zweite Fairteiler steht an der Winkelsmühle wird durch die Diakonie Offenbach mit betreut. Das heißt sie haben uns in Abstimmung mit der Stadt und hier insbesondere dem Denkmalamt den Platz und den Schrank zur Verfügung. Auch hier übernimmt foodsharing die regelmäßige Reinigung und Pflege.


Alle Fairteiler stellen sich auf unserer Homepage und auf foodsharing.de vor.


