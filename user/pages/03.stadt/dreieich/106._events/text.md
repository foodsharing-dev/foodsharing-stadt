---
title: Events
menu: events
image_align: left
published: false
---

## Events
Wir haben Anfang 2023 in Zusammenarbeit mit der Volkshochschule (VHS) Dreieich eine Veranstaltung zum Alltag eines Lebensmittelretters inkl. Vortrag über Lebensmittelverschwendung durchgeführt. Insgesamt waren gut 20 Interessierte anwesend, die mit uns Kisten sortiert und über Lebensmittelverschwendung diskutiert haben. Das Ganze fand in den Räumlichkeiten der VHS Dreieich statt. Es ist geplant, dass wir regelmäßig einmal im Halbjahr eine Veranstaltung mit der VHS durchführen.

