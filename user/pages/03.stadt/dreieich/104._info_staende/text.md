---
title: Info Stände
menu: info_staende
image_align: right
published: true
---

## Info Stände
Wir haben 2022 unsere Zusammenarbeit mit der VHS Dreieich gestartet. Hier fanden bereits folgende Veranstaltungen statt:

- Tag der offenen Tür im Haus des lebenslangen Lernens
- Foodsharing stellt sich vor (Veranstaltung der VHS)  
- Nachhaltigkeitstag der VHS

Weiterhin konnten wir uns bei den Grünen Dreieich im Rahmen ihres Parteitages vorstellen und durften anschließend bei der Bürgersprechstunde Rede und Antwort stehen. Im April 2023 hatten wir zudem einen großen Infostand im Rahmen der Eröffnung des Lehr- und Kräutergartens Dreieich.

