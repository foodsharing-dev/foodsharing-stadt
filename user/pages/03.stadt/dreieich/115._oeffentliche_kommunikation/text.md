---
title: Öffentlichkeitsarbeit der Stadt
menu: oeffentliche_kommunikation
image_align: left
published: true
---

## Öffentlichkeitsarbeit der Stadt
Die Stadt Dreieich hat einen eigenen [Lehr- und Kräutergarten](https://www.kraeutergarten-dreieich.de/), welcher durch einen Verein betreut wird. Hier werden Interessierte rund um Flora und Fauna und die Nutzung von Kräutern informiert. Im Rahmen der Eröffnung im April 2023 konnten wir uns als Verein Präsentieren und auf Lebensmittelverschwendung aufmerksam machen (<a href="https://www.op-online.de/region/dreieich/viel-neues-im-sprendlinger-lehr-und-kraeutergarten-92232944.html">Link zum Zeitungsartikel</a>). Für die Zukunft ist geplant, dass wir hier in unregelmäßigen Abständen zusammen arbeiten.

Lehr- und Kräutergarten › Kräutergarten Dreieich 



Viel Neues im Sprendlinger Lehr- und Kräutergarten (op-online.de)

