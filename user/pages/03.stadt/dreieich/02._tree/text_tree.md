---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Dreieich

Dreieich mit seinen knapp 42.000 Einwohner\*innen gehört zum foodsharing Bezirk Landkreis Offenbach West, welcher sich im September 2021 gegründet hat. Dabei untergliedert sich die Stadt in die Stadtteile Sprendlingen, Buchschlag, Dreieichenhain, Götzenhain und Offenthal und ist daher sehr weitläufig.

Anzahl der Kooperationen: 
59 im gesamten Bezirk Offenbach West
Anzahl der foodsaver*innen: 88
Anzahl der aktiven Foodsaver*innen über das Retten hinaus: ca. 80%
Besonderes: Gemeinnütziger Verein (foodsharing Landkreis Offenbach West e.V.)
 


(Stand: Juni 2023)
