---
title: Fairteiler
menu: fairteiler
image_align: left
published: true
---

## Fairteiler
Wir haben derzeit zwei Fairteiler in Dreiech und ein weiterer befindet sich derzeit im Aufbau. 
Zudem hängt am Fairteiler ein QR-Code für die dazugehörige WhatsApp Gruppe. Außerdem haben wir eine foodsharing Gruppe auf facebook, welche es allen ermöglicht seine übrigen Lebensmittel zu fairteilen.

Diese Links führen euch zur Seite der Fairteiler bzw. der facebook Gruppe:
- <a href="https://foodsharing.de/?page=fairteiler&bid=3869&sub=ft&id=942">Fairteiler Stadtteilbüro Dreieich Sprendlingen</a>

- <a href="https://foodsharing.de/?page=fairteiler&bid=3869&sub=ft&id=2229">Fairteiler in der Winkelsmühle Dreieichenhain</a>

- <a href="https://foodsharing.de/?page=fairteiler&bid=3869&sub=ft&id=2663">Fairteiler im Haus des lebenlangen Lernens Sprendlingen</a>

- <a href="https://www.facebook.com/groups/FoodsharingLangenEgelsbachDreieich">Facebook Gruppe von Langen Egelsbach Dreieich</a>
