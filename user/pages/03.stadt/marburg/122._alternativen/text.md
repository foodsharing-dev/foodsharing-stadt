---
title: Förderung von Alternativen
menu: alternativen
image_align: left
published: true
---

## Förderung von Alternativen

Im Rahmen des Altstadtfonds wurde die Urban Gardening Gruppe in der Oberstadt bei der Grundstücksuche unterstützt. Die GartenWerkStadt haben wir 2012 mit dem Ziel gegründet, in Marburg Räume für eine Auseinandersetzung mit landwirtschaftlichen Themen und gesunder Ernährung ins Leben zu rufen – und dabei praktisch die Grundlagen des ökologischen Anbaus zu vermitteln und eine Diskussionsplattform für agrarpolitische Themen
zu schaffen. Die GartenWerkStadt wächst Stück für Stück und es kommen immer wieder neue Bausteine hinzu. Mehr Infos: [https://www.gartenwerkstadt.de](https://www.gartenwerkstadt.de)