---
title: Öffentlichkeitsarbeit Team
menu: oeffentlichkeit_team
image_align: right
published: true
---

## Öffentlichkeitsarbeit Team

Wir sind eine Gruppe von ca. 20 aktiven Foodsaver\*Innen die sich im Bereich Öffentlichkeitsarbeit engagieren und hier bei Märkten und Messen mit Infoständen vertreten sind. Auch in der EngagierDich Messe der Uni sind wir vertreten. 2022 haben wir in der Marburger Altstadt
eine ganze Ladenfläche ein Monat lang zum Thema Lebensmittelverschwendung und foodsharing gestaltet
Kontakt: [oeffentlichkeitsarbeit.marburg@foodsharing.network](mailto:
oeffentlichkeitsarbeit.marburg@foodsharing.network)