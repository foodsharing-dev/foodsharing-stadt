---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
image_align: right
published: true
---

## Lokale foodsharing Gruppe

- Anzahl der Foodsaver\*innen: 571
- Kontakt:   [marburg@foodsharing.network](mailto:marburg@foodsharing.network)