---
title: Weitere lokale Initiativen
menu: mehr_projekte
image_align: right
published: true
---

## Weitere lokale Initiativen

Neben der lokalen foodsharing Gruppe gibt es weitere Initiativen in Marburg die sich für eine Nachhaltige Lebensform engagieren:
Solidarburg ist ein Initiative, wo Werkzeuge, Campingausrüstung ausgeliehen werden kann und in einem Repaircafè gemeinsam Dinge
repariert werden können. [https://solidarburg.de](https://solidarburg.de)

AkTIERismus setzt sich für die moralische Berücksichtigung und die Rechte sämtlicher empfindungsfähiger Lebewesen ein. [https://aktierismus.de](https://aktierismus.de)


