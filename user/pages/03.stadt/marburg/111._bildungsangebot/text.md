---
title: Bildungsangebot
menu: bildungsangebot
image_align: right
published: true
---

## Bildungsangebot

Mit der Bildungs AG sind wir gerade noch am Wachsen, haben aber bereits einige Schulbesuche umsetzen können und hier den
Schüler\*Innen das Thema Lebensmittelverschwendung näher bringen