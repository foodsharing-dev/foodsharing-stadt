---
title: Lebensmittelanbau
menu: lebensmittelanbau
image_align: left
published: true
---

## Lebensmittelanbau

Die junge Solawi Petersilie fördert die Regionale Versorgung mit Gemüse und betreibt aktiv regenerative Landwirtschaft und
Biodiversitätsförderung. Mehr Infos: [https://gaertnereipetersilie.de/glashuepfer-e-v/](https://gaertnereipetersilie.de/glashuepfer-e-v/)
