---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
image_align: right
published: true
---

## Öffentliche Lebensmittelrettung

Memolife Messe Marburg haben wir nach einem erfolgreichen Wochenende das übrig gebliebene Essen gerettet. Herausfordernd ist bei solchen
Aktionen immer, recht kurzfristig und außerhalb der regulären Abholungen Menschen zu finden, die bereits zubereitete Speisen abholen und
hierfür auch genügend Dosen haben. Im Rahmen der Feier Marburg 800 im Jahre 2022 wurden bei der Aktion „Tischlein deck dich“ Backwaren gerettet. 

