---
title: Marburg
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 50.80884
        lng: 8.77059
    status: pending

content:
    items: @self.modular
---
