---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Marburg

- foodsharing Bezirk seit 2014
- Anzahl Kooperationen: 35
- Anzahl der Foodsaver\*innen: 571
- Besonderes: Marburg ist eine klassische Universitätsstadt. Bei uns sind jedoch nicht ausschließlich nur Studierende engagiert.

(Stand: August 2024)
