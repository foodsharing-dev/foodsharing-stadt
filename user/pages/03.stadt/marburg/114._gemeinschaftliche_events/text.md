---
title: Gemeinschaftliche Events
menu: gemeinschaftliche_events
image_align: left
published: true
---

## Gemeinschaftliche Events

Im Sommer 2022 wurde unsere Aktion Brotausstellung durch die Stadt unterstützt. Wir haben einen Ladenfläche zu Verfügung gestellt
bekommen, mit der wir im Zentrum Marburgs die breite Gesellschaft erreichen konnten. Die Brotausstellung zeigte, die Backwaren, welche
allein foodsharing an einem Wochenende in Marburg vor der Tonne gerettet hat. Und sensibilisierten somit für das Thema Lebensmittelverschwendung.