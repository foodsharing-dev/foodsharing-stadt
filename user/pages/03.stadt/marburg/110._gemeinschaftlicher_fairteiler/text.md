---
title: Gemeinschaftlicher Fairteiler
menu: gemeinschaftlicher_fairteiler
image_align: left
published: true
---

## Gemeinschaftlicher Fairteiler

Mit Eröffnung des Volkshochschulen Fairteilers im Dezember 2022 konnten wir ein erstmals gut erreichbaren Fairteiler in den Händen der Stadt
Marburg zugänglich machen.
