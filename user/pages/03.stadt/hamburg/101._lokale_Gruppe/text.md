---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver: 3469  
- AbholerInnen mind. einmal im Monat: ca. 700 
- Engagierte über Abholungen hinaus: Schwierig zu sagen, schätzungsweise sind es aber leider nur eine Hand voll von ca. 30 Foodsaver\*innen, die darüber hinaus in Arbeitsgruppen aktiv sind, Fairteiler pflegen, Messestände betreuen etc. Es gibt aktuell 41 Botschafter\*innen in Hamburg.  
- Kontakt: <hamburg@foodsharing.network>   