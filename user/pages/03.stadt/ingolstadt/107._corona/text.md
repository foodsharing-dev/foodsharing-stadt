---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: right
---

## foodsharing in Zeiten von Corona

Die Slots wurden teilweise in Sortierer\*in und Abholer\*in aufgeteilt. Die Weitergabe erfolgte kontaktlos. Aufgrund einer hohen Home Office-Quote wurden die Slots schneller als
vor Corona besetzt. Alle Gruppentreffen und AG-Treffen wurden online abgehalten, wodurch für
manche Foodsaver\*innen eine Teilnahme erleichtert wurde.

Der Fairteiler wurde zeitweise geschlossen und die öffentlichen
Fairteilungen ausgesetzt. Einzelne sozialbedürftige Haushalte, die
oft zu den Fairteilungen kamen, wurden ersatzweise durch kontaktlose
Übergabe direkt beliefert. 

Soziale Organisationen wurden auch während des Lockdowns weiter mit
Lebensmitteln versorgt. Dank eines entsprechenden Hygienekonzeptes
konnte eine öffentliche Fairteilung mittlerweile wieder aufgenommen
werden.

Mehr Infos dazu findet ihr in diesem [Zeitungsartikel](https://www.donaukurier.de/lokales/ingolstadt/Auf-Rettungsmission-im-Supermarkt;art599,4749204?fbclid=IwAR0Xmobg7XLgNbDYOmEG1btUjM1MbLF97Cd_4RmtwCIvaLU0qv9jr99oyhs). 
