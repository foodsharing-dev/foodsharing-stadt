---
title: Lebensmittelanbau
menu: lebensmittelanbau
published: true
image_align: right
---

## Lebensmittelanbau

Im Kräutergarten am Neuen
Schloss können die Bürger\*innen frische Kräuter ernten. Es ist ein
Gemeinschaftsprojekt vom Gartenamt und dem Bayerischem Armeemuseum. Dieser [Link](https://www.ingolstadt.de/Rathaus/Aktuelles/Meldungs-Archiv/Der-Kr%C3%A4utergarten-am-Neuen-Schloss.php?object=tx,2789.5.1&ModID=7&FID=2789.5156.1&NavID=2789.737&La=1) führt euch zur Webseite des Kräutergartens.


