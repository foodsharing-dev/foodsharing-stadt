---
title: Fairteiler
menu: Fairteiler
published: true
image_align: left
---

## Fairteiler
In Ingolstadt gibt es aktuell
einen Fairteiler in Vronis Ratschhaus. Der Fairteiler wird von
Privatpersonen beliefert und von den Mitarbeiter\*innen vor Ort betreut.
Über diesen <a href="https://foodsharing.de/?page=fairteiler&bid=271&sub=ft&id=546">Link</a> kommt ihr zur Fairteilerwebseite.
