---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: right
---

## Öffentlichkeitsarbeit Team
Unsere AG Öffentlichkeitsarbeit unternimmt regelmäßig Auftritte, um über
Lebensmittelverschwendung aufzuklären. Dazu zählen Infostände mit
geretteten Lebensmitteln und Vorträge, z.B. an den Tagen der
Nachhaltigkeit oder dem Parking Day (siehe Foto).
Kontaktiert uns gerne über diese Mail: events.ingolstadt@foodsharing.network
