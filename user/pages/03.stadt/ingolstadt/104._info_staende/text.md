---
title: Info Stände
menu: info_staende
published: true
image_align: left
---

## Info Stände

Auf der Landesgartenschau 2021 in Ingolstadt bespielten wir an mehreren Tagen einen Open Space
Container, bei dem wir an die Besucher gerettete Lebensmittel
verschenkten und u.a. mit einem Quiz über Lebensmittelwertschätzung
aufklärten.
