---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver\*innen: 288
- Abholer\*innen mind. einmal im Monat: ca. 100
- Botschafter\*innen: 4
- Engagierte über Abholungen hinaus: ca. 30  
