---
title: Ingolstadt
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 48.7648
        lng: 11.4306
    status: pending


content:
    items: @self.modular
---
