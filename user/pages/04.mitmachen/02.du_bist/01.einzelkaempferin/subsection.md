---
title: 'Einzelkämpfer*in'
hide_page_title: true
---

### Einzelkämpfer\*in

#### Du bist der\*die erste, der\*die sich in deinem Umfeld für das Thema interessiert

Wundervoll, dass du dich für mehr Lebensmittelwertschätzung engagieren möchtest! Zunächst ist es sinnvoll, sich dafür Mitstreitende zu suchen. Sehr gerne kannst du dich über die Plattform foodsharing.de vernetzen. Und dann kann es eigentlich auch direkt losgehen!

<p class="prev-next">

<a class="btn btn-primary" href=/de/mitmachen/du_bist>
                            <i class="fa fa-chevron-left"></i>
                            zurück zur Ausgangssituation</a>

</p>
