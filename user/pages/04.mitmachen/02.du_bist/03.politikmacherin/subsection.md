---
title: 'Politikmacher*in'
hide_page_title: true
---

### Politikmacher\*in
#### Du arbeitest für die öffentliche Hand

Super, es braucht mehr Menschen wie dich, die Teil von politischen Strukturen sind und sich für mehr Lebensmittelwertschätzung einsetzen! Verschaff dir am besten zunächst einen Überblick über die Aktivitäten in deiner Stadt. Gibt es bereits eine lokale foodsharing Gruppe? Falls ja: Kontaktiere sie und tauscht euch über eine Zusammenarbeit im Rahmen der Bewegung aus. Falls nein: Dann kannst du sie ins Leben rufen! Natürlich sind Mitstreitende aus deinem direkten Berufsumfeld auch sehr wertvoll. Sprecht darüber, wie ihr Teil der Bewegung werden könnt und nehmt Kontakt zum Organisationsteam der Bewegung foodsharing-Städte auf.

<p class="prev-next">

<a class="btn btn-primary" href=/de/mitmachen/du_bist>
                            <i class="fa fa-chevron-left"></i>
                            zurück zur Ausgangssituation</a>

</p>